This project aims at designing, testing and analysis of a RT object detection system, based on HOG features[1].
The design is a combination of hardware and software, so it is deployed on an FPGA. 
The hardware is written in VHDL and is synthesized for the FPGAs on the DE1-SoC and ML605 development boards,
while software handles minor things like initialization and troubleshooting, executed on an ARM or Microblaze.
In the future, more boards will be included, such as the DE0-Nano-SoC or the Zybo.
So far, it works in theory and simulation, but it still has problems on a real-working example. Additionally, the hardware
resource usage is still a bit high.

TODO:
1. Organize this project a bit better.
2. Get the system to work, by utilizing a OV7670 camera module. 
3. Improve resource utilization
4. Extend the implementation to multiple-scale processing
5. Improve handling of high-res images



[1]Dalal, Navneet, and Bill Triggs. "Histograms of oriented gradients for human detection." Computer Vision and Pattern Recognition, 2005. CVPR 2005. IEEE Computer Society Conference on. Vol. 1. IEEE, 2005.