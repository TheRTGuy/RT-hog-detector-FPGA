library IEEE;

use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use IEEE.MATH_REAL.ALL;
use STD.textio.all;

entity tb_ip4 is
end entity tb_ip4;

architecture arch of tb_ip4 is
	component lin_interp_div is
	port (
		val_in : in std_logic_vector(16 downto 0);
		val_out : out std_logic_vector(17 downto 0)
	);
	end component lin_interp_div;
	
	signal sqrt_out : real;
	signal clk : std_logic := '0';
	signal rst : std_logic := '0';
	
	signal val_in : std_logic_vector(16 downto 0) := (others => '0');
	signal val_out : std_logic_vector(17 downto 0);
begin
	sqrt_out <= real(to_integer(unsigned(val_out))) / 65536.0;

	sqrt0 : lin_interp_div
	port map
	(
	      val_in => val_in,
	      val_out => val_out
	);
	
	clk <= not clk after 100 ns;
	
	test : process
	begin
		wait for 50 ns;
		rst <= '1';
		wait for 50 ns;
		rst <= '0';
		
		wait until falling_edge(clk);
		
		for i in 0 to 511*64*4 loop
			val_in <= std_logic_vector(to_unsigned(i,17));
			wait until rising_edge(clk);
			wait until falling_edge(clk);
		end loop;
		
		wait until rising_edge(clk);
		wait until falling_edge(clk);
		
		wait until rising_edge(clk);
		wait until falling_edge(clk);
		assert false report "End of simulation" severity failure;
 
		wait;
	end process test;
end architecture arch;

