library IEEE;

use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity sccb_int is
generic(
	CLK_DIVIDER_PER : natural := 260
);
port(
	sio_c_t	: out std_logic;
	sio_c_i	: in std_logic := '0';
	sio_c_o	: out std_logic;
	
	sio_d_t : out std_logic;
	sio_d_i : in std_logic := '0';
	sio_d_o : out std_logic;
	
	id_in 	: in std_logic_vector(7 downto 0) := (others => '0');
	reg_in 	: in std_logic_vector(7 downto 0) := (others => '0');
	val_in 	: in std_logic_vector(7 downto 0) := (others => '0');
	val_out : out std_logic_vector(7 downto 0);
	
	rd_en_i : in std_logic := '0';
	wr_en_i : in std_logic := '0';
	busy_o 	: out std_logic;
	
	clk 	: in std_logic := '0';
	reset 	: in std_logic := '0'
);
end entity sccb_int;

architecture rtl of sccb_int is
	type state_t is (idle,start,phase1,phase2,phase3,stop);
	signal state : state_t := idle;
	
	constant CLK_DIVIDER_PER_QUART : natural := CLK_DIVIDER_PER / 4;
 
	signal clk_div : natural range 0 to CLK_DIVIDER_PER-1 := 0;
	signal clk_div_nxt : natural range 0 to CLK_DIVIDER_PER-1;
	signal clk_div_0 : std_logic;
	signal clk_div_1 : std_logic;
	signal clk_div_roll : std_logic;
	signal clk_ctr : natural range 0 to 8 := 0;
	signal clk_ctr_roll : std_logic;
	signal clk_0 : std_logic := '0';
	signal clk_z : std_logic := '0';
	
	signal data_z : std_logic := '0';
	signal data_d : std_logic := '0';
	
	signal rd_en : std_logic := '0';
	signal wr_en : std_logic := '0';
	
	signal busy : std_logic;
	signal start_i : std_logic;
	signal sample : std_logic;
	
	signal sreg : std_logic_vector(7 downto 0) := (others => '0');
begin
	busy <= rd_en or wr_en;
	busy_o <= busy;
	start_i <= wr_en_i or rd_en_i;
	clk_div_roll <= '1' when clk_div = CLK_DIVIDER_PER-1 else '0';
	clk_div_nxt <= 0 when clk_div = CLK_DIVIDER_PER-1 else clk_div + 1;
	clk_ctr_roll <= '1' when clk_ctr = 0 else '0';
	sample <= '1' when clk_div = CLK_DIVIDER_PER_QUART else '0';
	
	--sio_d <= 'Z' when data_z = '0' else data_d;
	sio_d_t <= not data_z;
	sio_d_o <= data_d;
	data_proc : process(clk, reset)
	begin
		if reset = '1' then
			data_z <= '0';
			data_d <= '0';
		elsif rising_edge(clk) then
			if busy = '1' then
				case state is
				when start =>
					data_d <= not clk_ctr_roll;
					data_z <= '1';
				when stop =>
					data_d <= clk_ctr_roll;
					data_z <= '1';
				when phase2 =>
					data_d <= sreg(7) or not wr_en;
					data_z <= clk_ctr_roll xor wr_en;
				when phase1|phase3 =>
					data_d <= sreg(7);
					data_z <= not clk_ctr_roll;
				when others =>
					data_d <= '0';
					data_z <= '0';
				end case;
			else
				data_d <= '0';
				data_z <= '0';
			end if;
		end if;
	end process data_proc;
	
	sio_c_t <= not clk_z;
	sio_c_o <= clk_0;
	
	clk_div_0 <= '0' when clk_div < CLK_DIVIDER_PER_QUART else '1';
	clk_div_1 <= '1' when clk_div < (CLK_DIVIDER_PER-CLK_DIVIDER_PER_QUART) else '0';
	clk_proc : process(clk,reset)
	begin
		if reset = '1' then
			clk_0 <= '0';
			clk_z <= '0';
		elsif rising_edge(clk) then
			clk_z <= busy;
			if busy = '1' then
				case state is
				when start => clk_0 <= (clk_div_1 or not clk_ctr_roll);
				when stop => clk_0 <= (clk_div_0 or clk_ctr_roll);
				when idle => clk_0 <= '0';
				when others => clk_0 <= (clk_div_0 and clk_div_1);
				end case;
			else
				clk_0 <= '0';
			end if;
		end if;
	end process clk_proc;
	
	
	-- State machines,counters,etc
	val_out <= sreg;
	state_proc : process(clk,reset)
	begin
		if reset = '1' then
			clk_div <= 0;
			clk_ctr <= 0;
			wr_en <= '0';
			state <= idle;
			rd_en <= '0';
			sreg <= (others => '0');
		elsif rising_edge(clk) then
			if busy = '1' then
				clk_div <= clk_div_nxt;
				
				if sample = '1' then
					if wr_en = '0' and state = phase2 and clk_ctr_roll = '0' then
						sreg <= sreg(6 downto 0) & sio_d_i;
					end if;
				elsif clk_div_roll = '1' then
					if clk_ctr_roll = '1' then
						case state is
						when start =>
							clk_ctr <= 8;
							state <= phase1;
							sreg <= id_in(7 downto 1) & (not wr_en);
						when phase1 =>
							clk_ctr <= 8;
							state <= phase2;
							sreg <= reg_in;
						when phase2 =>
							if rd_en = '1' then
								clk_ctr <= 1;
								state <= stop;
							else
								clk_ctr <= 8;
								state <= phase3;
								sreg <= val_in;
							end if;
						when phase3 =>
							clk_ctr <= 1;
							state <= stop;
						when stop =>
							if rd_en = '1' and wr_en = '1' then
								clk_ctr <= 1;
								state <= start;
							else
								rd_en <= '0';
								state <= idle;
							end if;
							wr_en <= '0';
						when idle => null;
						end case;
					else
						clk_ctr <= clk_ctr - 1;
						if wr_en = '1' or state = phase1 then
							sreg <= sreg(6 downto 0) & '0';
						end if;
					end if;
				end if;
			elsif start_i = '1' then
				state <= start;
				clk_ctr <= 1;
				clk_div <= 0;
				wr_en <= '1';
				rd_en <= rd_en_i;
			end if;
		end if;
	end process state_proc;
end architecture rtl;