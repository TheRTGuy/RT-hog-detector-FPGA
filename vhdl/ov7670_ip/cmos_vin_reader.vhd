library IEEE;

use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity cmos_vin_reader is
port
(
	d : in std_logic_vector(7 downto 0) := (others => '0');
	data_out : out std_logic_vector(23 downto 0);
	
	vsync : in std_logic := '0';
	href : in std_logic := '0';
	
	frame_format : in std_logic_vector(1 downto 0) := "00";
	capture : in std_logic := '0';
	frame_started_o : out std_logic;
	valid_o : out std_logic;
	
	pclk : in std_logic := '0';
	reset : std_logic := '0'
);
end entity cmos_vin_reader;

architecture rtl of cmos_vin_reader is
	signal frame_valid: std_logic_vector(1 downto 0) := "00";
	constant RGB444 : std_logic_vector(1 downto 0) := "00";
	constant RGB555 : std_logic_vector(1 downto 0) := "01";
	constant RGB565 : std_logic_vector(1 downto 0) := "10";
	constant RGB888 : std_logic_vector(1 downto 0) := "11";	
	
	signal data : std_logic_vector(23 downto 0) := (others => '0');
	signal byte_num : std_logic_vector(1 downto 0) := "00";
	signal frame_start : std_logic_vector(1 downto 0) := "00";
	signal frm_fmt : std_logic_vector(1 downto 0) := RGB444;
	
	signal pix_complete : std_logic := '0';
	signal frame_started : std_logic;
	signal capturing : std_logic_vector(1 downto 0) := "00";
begin
	frame_started_o <= frame_started;
	frame_started <= frame_start(0) and frame_start(1);
	valid_o <= frame_started and pix_complete;
	data_out <= data;
	seq : process(pclk,reset)
	begin
		if reset = '1' then
			capturing <= "00";
			frm_fmt <= RGB444;
			frame_start <= "00";
			byte_num <= "00";
			data <= (others => '0');
			pix_complete <= '0';
		elsif rising_edge(pclk) then
			-- Sync the capture signal to the 'pclk' domain
			capturing(1) <= capturing(0);
			capturing(0) <= capture;
			-- Wait for a fresh frame start
			case frame_start is
			when "00" =>
				-- Make sure we want to capture
				if capturing(1) = '1' and capturing(1) = capturing(0) then
					frm_fmt <= frame_format; -- Clock the format in, once capturing process starts
					frame_start <= "01";
				else
					frame_start <= "00";
				end if;
			when "01" =>
				if vsync = '1' then
					frame_start <= "10";
				else
					frame_start <= "01";
				end if;
			when "10" =>
				if vsync = '0' then
					frame_start <= "11";
				else
					frame_start <= "10";
				end if;
			when "11" =>
				if vsync = '1' then
					frame_start <= "00";
				else
					frame_start <= "11";
				end if;
			when others => 
				frame_start <= "00";
			end case;
			
			if frame_started = '1' and href = '1' then
				-- 23     16 15      8 7       0
				-- RRRR 0000 GGGG 0000 BBBB 0000 - RGB444
				-- RRRR R000 GGGG G000 BBBB B000 - RGB555
				-- RRRR R000 GGGG GG00 BBBB B000 - RGB565
				-- RRRR RRRR GGGG GGGG BBBB BBBB - RGB888
				case byte_num is
				when "00" =>
					pix_complete <= '0';
					case frm_fmt is
					when RGB444 => data(23 downto 16) <=  d(3 downto 0) & "0000"; -- R
					when RGB555 => data(23 downto 14) <= d(6 downto 2) & "000" & d(1 downto 0); -- RG
					when RGB565 => data(23 downto 13) <= d(7 downto 3) & "000" & d(2 downto 0); -- RG
					when others => data(23 downto 16) <= d;
					end case;
					byte_num <= "01";
				when "01" =>
					case frm_fmt is
					when RGB444 => data(15 downto 0) <=  d(7 downto 4) & "0000" & d(3 downto 0) & "0000"; -- GB
					when RGB555 => data(13 downto 0) <= d(7 downto 5) & "000" & d(4 downto 0) & "000"; -- GB
					when RGB565 => data(12 downto 0) <= d(7 downto 5) & "00" & d(4 downto 0) & "000"; -- GB
					when others => data(15 downto 8) <= d;
					end case;
					
					if frm_fmt /= RGB888 then
						pix_complete <= '1';
						byte_num <= "00";
					else
						pix_complete <= '0';
						byte_num <= "10";
					end if;
				when "10" =>
					pix_complete <= '1';
					data(7 downto 0) <= d;
					byte_num <= "00";
				when others =>
					data <= (others => '0');
					byte_num <= "00";
					pix_complete <= '0';
				end case;
			else
				pix_complete <= '0';
				byte_num <= "00";
			end if;
		end if;
	end process seq;
end architecture rtl;