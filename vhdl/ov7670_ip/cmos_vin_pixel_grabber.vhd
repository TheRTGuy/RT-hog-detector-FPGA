----------------------------------------------------------------------------------
-- cmos_vin_pixel_grabber.vhd
-- 
-- Description: Simple module which detects the beginning of a fresh frame, by
-- 		using the VSYNC signal provided by most CMOS image sensors
-- Author: Viktorio el Hakim
----------------------------------------------------------------------------------

library IEEE;

use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity cmos_vin_pixel_grabber is
port
(
	din : in std_logic_vector(7 downto 0) := (others => '0');
	data_out : out std_logic_vector(23 downto 0);
	
	rd_en : in std_logic := '0';
	valid_o : out std_logic;
	frm_fmt_in : in std_logic_vector(2 downto 0) := "000";
	
	clk : in std_logic := '0';
	reset : std_logic := '0'
);
end entity cmos_vin_pixel_grabber;

architecture rtl of cmos_vin_pixel_grabber is
	constant RGB444 : std_logic_vector(2 downto 0) := "000";
	constant RGB555 : std_logic_vector(2 downto 0) := "001";
	constant RGB565 : std_logic_vector(2 downto 0) := "010";
	constant RGB888 : std_logic_vector(2 downto 0) := "011";	
	constant BGR444 : std_logic_vector(2 downto 0) := "100";
	constant BGR555 : std_logic_vector(2 downto 0) := "101";
	constant BGR565 : std_logic_vector(2 downto 0) := "110";
	constant BGR888 : std_logic_vector(2 downto 0) := "111";	
	
	signal data : std_logic_vector(23 downto 0) := (others => '0');
	signal byte_num : std_logic_vector(1 downto 0) := "00";
	signal valid : std_logic := '0';
begin
	valid_o <= valid;
	data_out <= data;
	seq : process(clk,reset)
	begin
		if reset = '1' then
			byte_num <= "00";
			data <= (others => '0');
			valid <= '0';
		elsif rising_edge(clk) then
			if rd_en = '1' then
				-- 23     16 15      8 7       0
				-- RRRR 0000 GGGG 0000 BBBB 0000 - RGB444
				-- RRRR R000 GGGG G000 BBBB B000 - RGB555
				-- RRRR R000 GGGG GG00 BBBB B000 - RGB565
				-- RRRR RRRR GGGG GGGG BBBB BBBB - RGB888
				
				-- BBBB 0000 GGGG 0000 RRRR 0000 - BGR444
				-- BBBB B000 GGGG G000 RRRR R000 - BGR555
				-- BBBB B000 GGGG GG00 RRRR R000 - BGR565
				-- BBBB BBBB GGGG GGGG RRRR RRRR - BGR888
				case byte_num is
				when "00" =>
					valid <= '0';
					case frm_fmt_in is
					when RGB444 => data(23 downto 16) <= din(3 downto 0) & "0000"; -- R
					when RGB555 => data(23 downto 14) <= din(6 downto 2) & "000" & din(1 downto 0); -- RG
					when RGB565 => data(23 downto 13) <= din(7 downto 3) & "000" & din(2 downto 0); -- RG
					when RGB888 => data(23 downto 16) <= din;
					
					when BGR444 => data(7 downto 0) <= din(3 downto 0) & "0000"; -- R
					when BGR555 => data(15 downto 14) <= din(1 downto 0);
						       data(7 downto 0) <= din(6 downto 2) & "000";  -- RG
					when BGR565 => data(15 downto 13) <= din(2 downto 0);
						       data(7 downto 0) <= din(7 downto 3) & "000"; -- RG
					when BGR888 => data(7 downto 0) <= din;
					when others => data(23 downto 16) <= din;
					end case;
					byte_num <= "01";
				when "01" =>
					case frm_fmt_in is
					when RGB444 => data(15 downto 0) <=  din(7 downto 4) & "0000" & din(3 downto 0) & "0000"; -- GB
					when RGB555 => data(13 downto 0) <= din(7 downto 5) & "000" & din(4 downto 0) & "000"; -- GB
					when RGB565 => data(12 downto 0) <= din(7 downto 5) & "00" & din(4 downto 0) & "000"; -- GB
					
					when BGR444 => data(23 downto 8) <= din(3 downto 0) & "0000" & din(7 downto 4) & "0000"; -- GB
					when BGR555 => data(23 downto 16) <= din(4 downto 0) & "000"; -- GB
						       data(13 downto 8) <= din(7 downto 5) & "000";
					when BGR565 => data(23 downto 16) <= din(4 downto 0) & "000"; -- GB
						       data(12 downto 8) <= din(7 downto 5) & "00";
						       
					when others => data(15 downto 8) <= din;
					end case;
					
					if frm_fmt_in /= RGB888 and frm_fmt_in /= BGR888 then
						valid <= '1';
						byte_num <= "00";
					else
						valid <= '0';
						byte_num <= "10";
					end if;
				when "10" =>
					valid <= '1';
					
					if frm_fmt_in = RGB888 then
						data(7 downto 0) <= din;
					else
						data(23 downto 16) <= din;
					end if;
					byte_num <= "00";
				when others =>
					data <= (others => '0');
					byte_num <= "00";
					valid <= '0';
				end case;
			else
				valid <= '0';
				byte_num <= "00";
			end if;
		end if;
	end process seq;
end architecture rtl;