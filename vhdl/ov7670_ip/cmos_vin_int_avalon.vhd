library IEEE;

use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity cmos_vin_int_avalon is
generic
(
	SCCB_CLK_DIVIDER_PER : natural := 260;
	USE_EXT_FIFO_INTERFACE : boolean := true;
	PIXEL_CTR_MAX : natural := 307200
);
port( 
	-- Avalon register slave interface
	avs_s0_address		:	in	std_logic_vector(1 downto 0)  := (others => '0'); --      s0.address
	avs_s0_read		:	in	std_logic := '0';             --        .read
	avs_s0_readdata		:	out	std_logic_vector(31 downto 0);                    --        .readdata
	avs_s0_write		:	in	std_logic := '0';             --        .write
	avs_s0_writedata	:	in	std_logic_vector(31 downto 0) := (others => '0'); --        .writedata
	
	
	-- Camera data interface
	-- Standard interface
	cmos_vin_d_in : in std_logic_vector(7 downto 0) := (others => '0');
	cmos_vin_vsync_in : in std_logic := '0';
	cmos_vin_href_in : in std_logic := '0';
	cmos_vin_pclk_in : in std_logic := '0';
	
	-- External FIFO interface
	cmos_vin_wrst_out : out std_logic;
	cmos_vin_rrst_out : out std_logic;
	cmos_vin_wen_out : out std_logic;
	cmos_vin_rclk_out : out std_logic;
		
	-- Camera SCCB (IIC like) interface
	SCCB_sioc_io	: inout std_logic;
	SCCB_siod_io	: inout std_logic;
		
	-- Data stream output interface
	data_out : out std_logic_vector(31 downto 0);
	data_valid : out std_logic;
	
	-- Standard interface clock
	clk			:	in    	std_logic := '0';             --   clock.clk
	reset			:	in    	std_logic := '0'             --   reset.reset
);
end entity cmos_vin_int_avalon;

architecture rtl of cmos_vin_int_avalon is
	component cmos_vin_int_ul is
	generic
	(
		SCCB_CLK_DIVIDER_PER : natural := 260;
		USE_EXT_FIFO_INTERFACE : boolean := true;
		PIXEL_CTR_MAX : natural := 307200
	);
	port
	(
		-- CMOS camera SCCB interface
		SCCB_sioc_t : out std_logic;
		SCCB_sioc_i : in std_logic;
		SCCB_sioc_o : out std_logic;
		SCCB_siod_t : out std_logic;
		SCCB_siod_i : in std_logic;
		SCCB_siod_o : out std_logic;
		
		-- CMOS camera data interface
		-- Common
		d : in std_logic_vector(7 downto 0) := (others => '0');
		vsync : in std_logic := '0';
		pclk : in std_logic := '0';
		-- Without FIFO
		href : in std_logic := '0';
		
		-- With FIFO
		fifo_rclk : out std_logic;
		fifo_wen : out std_logic;
		fifo_wrst : out std_logic;
		fifo_rrst : out std_logic;
		
		-- Control interface
		reg_we_in : in std_logic := '0';
		reg_re_in : in std_logic := '0';
		reg_addr_in : in std_logic_vector(1 downto 0);
		reg_data_in : in std_logic_vector(31 downto 0);
		reg_data_out : out std_logic_vector(31 downto 0);
		reg_valid : out std_logic;
		
		-- Data stream output interface
		data_out : out std_logic_vector(31 downto 0);
		data_valid : out std_logic;
		
		clk : in std_logic := '0';
		reset : in std_logic := '0'
	);
	end component cmos_vin_int_ul;
	
	signal sccb_sioc_t : std_logic;
	signal sccb_sioc_o : std_logic;
	signal sccb_siod_t : std_logic;
	signal sccb_siod_o : std_logic;
begin
	
	SCCB_sioc_io <= 'Z' when sccb_sioc_t = '1' else sccb_sioc_o;
	SCCB_siod_io <= 'Z' when sccb_siod_t = '1' else sccb_siod_o;
			
	int0 : cmos_vin_int_ul
	generic map
	(
		SCCB_CLK_DIVIDER_PER => SCCB_CLK_DIVIDER_PER,
		USE_EXT_FIFO_INTERFACE => USE_EXT_FIFO_INTERFACE,
		PIXEL_CTR_MAX => PIXEL_CTR_MAX
	)
	port map
	(
		SCCB_sioc_t => sccb_sioc_t,
		SCCB_sioc_i => SCCB_sioc_io,
		SCCB_sioc_o => sccb_sioc_o,
		SCCB_siod_t => sccb_siod_t,
		SCCB_siod_i => SCCB_siod_io,
		SCCB_siod_o => sccb_siod_o,
		d => cmos_vin_d_in,
		vsync => cmos_vin_vsync_in,
		pclk => cmos_vin_pclk_in,
		href => cmos_vin_href_in,
		fifo_rclk => cmos_vin_rclk_out,
		fifo_wen => cmos_vin_wen_out,
		fifo_wrst => cmos_vin_wrst_out,
		fifo_rrst => cmos_vin_rrst_out,
		reg_we_in => avs_s0_write,
		reg_re_in => avs_s0_read,
		reg_addr_in => avs_s0_address,
		reg_data_in => avs_s0_writedata,
		reg_data_out => avs_s0_readdata,
		data_out => data_out,
		data_valid => data_valid,
		clk => clk,
		reset => reset
	);
end architecture rtl;