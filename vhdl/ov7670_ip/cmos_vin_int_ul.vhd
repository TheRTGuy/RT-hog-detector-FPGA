library IEEE;

use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity cmos_vin_int_ul is
generic
(
	SCCB_CLK_DIVIDER_PER : natural := 260;
	USE_EXT_FIFO_INTERFACE : boolean := true;
	PIXEL_CTR_MAX : natural := 307200
);
port
(
	-- CMOS camera SCCB interface
	SCCB_sioc_t : out std_logic;
	SCCB_sioc_i : in std_logic;
	SCCB_sioc_o : out std_logic;
	SCCB_siod_t : out std_logic;
	SCCB_siod_i : in std_logic;
	SCCB_siod_o : out std_logic;
	
	-- CMOS camera data interface
	-- Common
	d : in std_logic_vector(7 downto 0) := (others => '0');
	vsync : in std_logic := '0';
	pclk : in std_logic := '0';
	-- Without FIFO
	href : in std_logic := '0';
	
	-- With FIFO
	fifo_rclk : out std_logic;
	fifo_wen : out std_logic;
	fifo_wrst : out std_logic;
	fifo_rrst : out std_logic;
	
	-- Control interface
	reg_we_in : in std_logic := '0';
	reg_re_in : in std_logic := '0';
	reg_addr_in : in std_logic_vector(1 downto 0);
	reg_data_in : in std_logic_vector(31 downto 0);
	reg_data_out : out std_logic_vector(31 downto 0);
	reg_valid : out std_logic;
	
	-- Data stream output interface
	data_out : out std_logic_vector(31 downto 0);
	data_valid : out std_logic;
	
	clk : in std_logic := '0';
	reset : in std_logic := '0'
);
end entity cmos_vin_int_ul;

architecture rtl of cmos_vin_int_ul is
	component cmos_vin_frame_detector is
	generic
	(
		SYNC_CAPTURE_SIGNAL : boolean := true;
		SYNC_VSYNC_SIGNAL : boolean := false
	);
	port
	(
		vsync_in : in std_logic := '0';
		capture : in std_logic := '0';
		frame_started_o : out std_logic;
		
		clk : in std_logic := '0';
		reset : std_logic := '0'
	);
	end component cmos_vin_frame_detector;
	component cmos_vin_pixel_grabber is
	port
	(
		din : in std_logic_vector(7 downto 0) := (others => '0');
		data_out : out std_logic_vector(23 downto 0);
		
		rd_en : in std_logic := '0';
		valid_o : out std_logic;
		frm_fmt_in : in std_logic_vector(2 downto 0) := "000";
		
		clk : in std_logic := '0';
		reset : std_logic := '0'
	);
	end component cmos_vin_pixel_grabber;
	component sccb_int is
	generic(
		CLK_DIVIDER_PER : natural := 260
	);
	port(
		sio_c_t	: out std_logic;
		sio_c_i	: in std_logic := '0';
		sio_c_o	: out std_logic;
		
		sio_d_t : out std_logic;
		sio_d_i : in std_logic := '0';
		sio_d_o : out std_logic;
		
		id_in 	: in std_logic_vector(7 downto 0) := (others => '0');
		reg_in 	: in std_logic_vector(7 downto 0) := (others => '0');
		val_in 	: in std_logic_vector(7 downto 0) := (others => '0');
		val_out : out std_logic_vector(7 downto 0);
		
		rd_en_i : in std_logic := '0';
		wr_en_i : in std_logic := '0';
		busy_o 	: out std_logic;
		
		clk 	: in std_logic := '0';
		reset 	: in std_logic := '0'
	);
	end component sccb_int;
	
	constant RGB444 : std_logic_vector(2 downto 0) := "000";
	constant RGB555 : std_logic_vector(2 downto 0) := "001";
	constant RGB565 : std_logic_vector(2 downto 0) := "010";
	constant RGB888 : std_logic_vector(2 downto 0) := "011";	
	constant BGR444 : std_logic_vector(2 downto 0) := "100";
	constant BGR555 : std_logic_vector(2 downto 0) := "101";
	constant BGR565 : std_logic_vector(2 downto 0) := "110";
	constant BGR888 : std_logic_vector(2 downto 0) := "111";
	
	signal sccb_id : std_logic_vector(7 downto 0) := (others => '0');
	signal sccb_reg : std_logic_vector(7 downto 0) := (others => '0');
	signal sccb_val : std_logic_vector(7 downto 0) := (others => '0');
	signal sccb_val_out : std_logic_vector(7 downto 0) := (others => '0');
	signal sccb_busy : std_logic := '0';
	signal sccb_wr_en : std_logic := '0';
	signal sccb_rd_en : std_logic := '0';
	signal sccb_start : std_logic := '0';
	
	signal cmos_vin_capture : std_logic := '0';
	signal cmos_vin_frame_started : std_logic;
	signal cmos_vin_frame_start_sync : std_logic_vector(1 downto 0) := "00";
	signal cmos_vin_frame_start : std_logic;
	signal cmos_vin_data_clk : std_logic;
	signal cmos_vin_data_out : std_logic_vector(23 downto 0);
	signal cmos_vin_pixel_valid : std_logic;
	signal cmos_vin_frm_fmt : std_logic_vector(2 downto 0) := RGB444;
	signal cmos_pg_rd_en : std_logic;
	signal cmos_vin_stream_start : std_logic := '0';
	signal cmos_vin_stream_stop : std_logic := '0';
	
	signal cmos_vin_fifo_wrst : std_logic := '0';
	signal cmos_vin_fifo_rrst : std_logic := '0';
	
	signal pixel_ctr : natural range 0 to PIXEL_CTR_MAX-1 := 0;
	signal pixel_capture : std_logic := '0';
	signal pixel_ctr_nxt : natural range 0 to PIXEL_CTR_MAX-1 := 0;
	signal pixel_span : natural range 0 to PIXEL_CTR_MAX-1 := 0;
	signal pixel_ack : std_logic := '0';
begin
	fd0 : cmos_vin_frame_detector
	generic map
	(
		SYNC_CAPTURE_SIGNAL => true,
		SYNC_VSYNC_SIGNAL => USE_EXT_FIFO_INTERFACE
	)
	port map
	(
		vsync_in 	=> vsync,
		capture 	=> cmos_vin_capture,
		frame_started_o => cmos_vin_frame_started,
		clk		=> pclk,
		reset		=> reset
	);
	
	pg0 : cmos_vin_pixel_grabber
	port map
	(
		din => d,
		data_out => cmos_vin_data_out,
		
		rd_en => cmos_pg_rd_en,
		valid_o => cmos_vin_pixel_valid,
		frm_fmt_in => cmos_vin_frm_fmt,
		
		clk => pclk,
		reset => reset
	);
	
	fifo_rrst <= cmos_vin_fifo_rrst;
	fifo_wrst <= cmos_vin_fifo_wrst;
	fifo_rclk <= pclk;
	fifo_wen <= cmos_vin_frame_started;
	ext_fifo : if USE_EXT_FIFO_INTERFACE = true generate
		cmos_pg_rd_en <= pixel_capture;
		
		pixel_ctr_nxt <= 0 when pixel_ctr = pixel_span or pixel_ctr = PIXEL_CTR_MAX-1 else 
				  pixel_ctr + 1;
		vram_proc : process(pclk, reset)
		begin
			if reset = '1' then
				pixel_capture <= '0';
				pixel_ctr <= 0;
				pixel_ack <= '0';
			elsif rising_edge(pclk) then
				pixel_ack <= cmos_vin_stream_start or cmos_vin_stream_stop;
				
				if pixel_capture = '1' then
					if cmos_vin_stream_stop = '1' or pixel_ctr = pixel_span or pixel_ctr = PIXEL_CTR_MAX-1 then
						pixel_capture <= '0';
						pixel_ctr <= 0;
					elsif cmos_vin_pixel_valid = '1' then
						pixel_ctr <= pixel_ctr_nxt;
					end if;
				else
					if cmos_vin_stream_start = '1' then
						pixel_capture <= '1';
					end if;
					pixel_ctr <= 0;
				end if;
			end if;
		end process vram_proc;
	
	end generate ext_fifo;
	
	int_fifo : if USE_EXT_FIFO_INTERFACE = false generate
		cmos_pg_rd_en <= href and cmos_vin_frame_started;
	end generate int_fifo;
	
	cmos_vin_frame_start <= cmos_vin_frame_start_sync(1);-- and not (cmos_vin_frame_start_sync(1) xor cmos_vin_frame_start_sync(0));
	sync_proc : process(clk, reset)
	begin
		if reset = '1' then
			-- Sync to local clock
			cmos_vin_frame_start_sync <= "00";
		elsif rising_edge(clk) then
			-- Sync to local clock
			cmos_vin_frame_start_sync(1) <= cmos_vin_frame_start_sync(0);
			cmos_vin_frame_start_sync(0) <= cmos_vin_frame_started;
		end if;
	end process sync_proc;


	sccb0 : sccb_int
	generic map
	(
		CLK_DIVIDER_PER => SCCB_CLK_DIVIDER_PER
	)
	port map
	(
		sio_c_t => SCCB_sioc_t,
		sio_c_i => SCCB_sioc_i,
		sio_c_o => SCCB_sioc_o,
		sio_d_t => SCCB_siod_t,
		sio_d_i => SCCB_siod_i,
		sio_d_o => SCCB_siod_o,
		id_in	=> sccb_id,
		reg_in	=> sccb_reg,
		val_in	=> sccb_val,
		val_out => sccb_val_out,
		rd_en_i => sccb_rd_en,
		wr_en_i => sccb_wr_en,
		busy_o	=> sccb_busy,
		clk 	=> clk,
		reset	=> reset
	);
			
	reg_proc : process(clk,reset)
	begin
		if reset = '1' then
			sccb_id <= (others => '0');
			sccb_reg <= (others => '0');
			sccb_val <= (others => '0');
			sccb_start <= '0';
			sccb_wr_en <= '0';
			sccb_rd_en <= '0';
			
			cmos_vin_capture <= '0';
			cmos_vin_frm_fmt <= RGB444;
			cmos_vin_stream_stop <= '0';
			cmos_vin_stream_start <= '0';
			cmos_vin_fifo_rrst <= '0';
			cmos_vin_fifo_wrst <= '0';
			
			pixel_span <= 0;
		elsif rising_edge(clk) then
			-- Automatically reset the rd and wr enables
			if sccb_rd_en = '1' then
				sccb_rd_en <= '0';
			end if;
			if sccb_wr_en = '1' then
				sccb_wr_en <= '0';
			end if;
			
			
			-- At the end of a transaction, put the output value in our value register and reset
			if sccb_start = '1' and sccb_busy = '0' and sccb_rd_en = '0' and sccb_wr_en = '0' then
				sccb_start <= '0';
				sccb_val <= sccb_val_out;
			end if;
			
			if pixel_ack = '1' and USE_EXT_FIFO_INTERFACE then
				cmos_vin_stream_start <= '0';
				cmos_vin_stream_stop <= '0';
			end if;
			
			if reg_we_in = '1' then
				case reg_addr_in is
				when "00" =>
					if sccb_start = '0' then
						sccb_rd_en <= reg_data_in(25);
						sccb_wr_en <= reg_data_in(24);
						sccb_start <= reg_data_in(25) or reg_data_in(24);
						sccb_id <= reg_data_in(23 downto 16);
						sccb_reg <= reg_data_in(15 downto 8);
						sccb_val <= reg_data_in(7 downto 0);
					end if;
				when "01" =>
					cmos_vin_fifo_rrst <= reg_data_in(15);
					cmos_vin_fifo_wrst <= reg_data_in(14);
					cmos_vin_stream_stop <= reg_data_in(5);
					cmos_vin_stream_start <= reg_data_in(4);
					cmos_vin_frm_fmt <= reg_data_in(3 downto 1);
					cmos_vin_capture <= reg_data_in(0);
				when "10" =>
					pixel_span <= to_integer(unsigned(reg_data_in));
				when others => null;
				end case;
			end if;
		end if;
	end process reg_proc;
	
	reg_valid <= '1';
	with reg_addr_in & reg_re_in select reg_data_out <=
		(31 downto 25 => '0') & sccb_start & sccb_id & sccb_reg & sccb_val
			when "00" & '1',
		(31 downto 6 => '0') & pixel_capture
				     & cmos_vin_frame_start
				     & cmos_vin_frm_fmt
				     & cmos_vin_capture
			when "01" & '1',
		std_logic_vector(to_unsigned(pixel_ctr,32))
			when "10" & '1',
		(others => '0')
			when others;
		
	
	data_valid <= cmos_vin_pixel_valid;
	data_out <= (31 downto 24 => '0') & cmos_vin_data_out;
end architecture rtl;
