----------------------------------------------------------------------------------
-- cmos_vin_frame_detector.vhd
-- 
-- Description: Simple module which detects the beginning of a fresh frame, by
-- 		using the VSYNC signal provided by most CMOS image sensors
-- Author: Viktorio el Hakim
----------------------------------------------------------------------------------

library IEEE;

use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity cmos_vin_frame_detector is
generic
(
	SYNC_CAPTURE_SIGNAL : boolean := true;
	SYNC_VSYNC_SIGNAL : boolean := false
);
port
(
	vsync_in : in std_logic := '0';
	capture : in std_logic := '0';
	frame_started_o : out std_logic;
	
	clk : in std_logic := '0';
	reset : std_logic := '0'
);
end entity cmos_vin_frame_detector;

architecture rtl of cmos_vin_frame_detector is
	signal frame_start : std_logic_vector(1 downto 0) := "00";
	
	signal frame_started : std_logic;
	
	signal capturing : std_logic;
	signal capturing_sync : std_logic_vector(1 downto 0) := "00";
	
	signal vsync_sinc : std_logic_vector(1 downto 0) := "00";
	signal vsync : std_logic;
begin
	CAP_SYNC : if SYNC_CAPTURE_SIGNAL = true generate
		cap_sync_proc : process(clk,reset)
		begin
			if reset = '1' then
				capturing_sync <= "00";
			elsif rising_edge(clk) then
				capturing_sync(1) <= capturing_sync(0);
				capturing_sync(0) <= capture;
			end if;
		end process cap_sync_proc;
		capturing <= capturing_sync(1) and (capturing_sync(1) xnor capturing_sync(0));
	end generate CAP_SYNC;
	CAP_NSYNC : if SYNC_CAPTURE_SIGNAL = false generate
		capturing <= capture;
	end generate CAP_NSYNC;
	
	VSYNC_SYNC : if SYNC_VSYNC_SIGNAL = true generate
		vsync_sync_proc : process(clk,reset)
		begin
			if reset = '1' then
				vsync_sinc <= "00";
			elsif rising_edge(clk) then
				vsync_sinc(1) <= vsync_sinc(0);
				vsync_sinc(0) <= vsync_in;
			end if;
		end process vsync_sync_proc;
		vsync <= vsync_sinc(1) and (vsync_sinc(1) xnor vsync_sinc(0));
	end generate VSYNC_SYNC;
	VSYNC_NSYNC : if SYNC_VSYNC_SIGNAL = false generate
		vsync <= vsync_in;
	end generate VSYNC_NSYNC;

	frame_started_o <= frame_started;
	frame_started <= frame_start(0) and frame_start(1);
	seq : process(clk,reset)
	begin
		if reset = '1' then
			frame_start <= "00";
		elsif rising_edge(clk) then
			-- Wait for a fresh frame start
			case frame_start is
			when "00" =>
				-- Make sure we want to capture
				if capturing = '1' then
					frame_start <= "01";
				else
					frame_start <= "00";
				end if;
			when "01" =>
				if vsync = '1' then
					frame_start <= "10";
				else
					frame_start <= "01";
				end if;
			when "10" =>
				if vsync = '0' then
					frame_start <= "11";
				else
					frame_start <= "10";
				end if;
			when "11" =>
				if vsync = '1' then
					frame_start <= "00";
				else
					frame_start <= "11";
				end if;
			when others => 
				frame_start <= "00";
			end case;
		end if;
	end process seq;
end architecture rtl;