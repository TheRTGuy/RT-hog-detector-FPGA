library IEEE;

use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use STD.textio.all;

entity tb_ip3 is
generic(
	IMG_WIDTH_PIXELS : natural := 185;
	IMG_HEIGHT_PIXELS : natural := 138;
	WIND_WIDTH_PIXELS : natural := 64;
	WIND_HEIGHT_PIXELS : natural := 128;
	DP_AMOUNT : natural := 3
);
end entity tb_ip3;

architecture arch of tb_ip3 is
	function calc_wnd_amt_quantized( constant wind_amt_tot : natural;
					  constant img_len_blocks : natural;
					  constant wind_len_blocks : natural;
					  constant dp_index : natural;
					  constant dp_amt : natural)
					  return natural is
		variable b : natural := ((wind_amt_tot-1)/8) mod dp_amt;
		variable c : natural := (wind_amt_tot-1) mod 8;
		variable amt : natural := ((img_len_blocks-wind_len_blocks + (dp_amt-1) - dp_index)/dp_amt)*8;
	begin
		if b = dp_index then
			return amt+c+1;
		else
			return amt;
		end if;
	end function;
	constant WIND_AMT_COL : natural := IMG_WIDTH_PIXELS - WIND_WIDTH_PIXELS + 1;
	constant WIND_AMT_ROW : natural := IMG_HEIGHT_PIXELS - WIND_HEIGHT_PIXELS + 1;
	constant WIND_AMT_TOT : natural := WIND_AMT_ROW*WIND_AMT_COL;
	constant IMG_WIDTH_BLOCKS : natural := (IMG_WIDTH_PIXELS/8) - 1;
	constant IMG_HEIGHT_BLOCKS : natural := (IMG_HEIGHT_PIXELS/8) - 1;
	constant WIND_WIDTH_BLOCKS : natural := (WIND_WIDTH_PIXELS/8) - 1;
	constant WIND_HEIGHT_BLOCKS : natural := (WIND_HEIGHT_PIXELS/8) - 1;
	
	
	component addr_distr is
	generic
	(
		IMG_WIDTH_PIXELS : natural := 320;
		IMG_HEIGHT_PIXELS : natural := 240;
		WIND_WIDTH_PIXELS : natural := 64;
		WIND_HEIGHT_PIXELS : natural := 128;
		-- m
		DP_AMOUNT_WIDTH : natural := 3;
		DP_AMOUNT_HEIGHT : natural := 1;
		-- p
		DP_INDEX_WIDTH : natural := 0;
		DP_INDEX_HEIGHT : natural := 0
	);
	port
	(
		en : in std_logic := '0';
		sync_in : in std_logic;
		mem_we : out std_logic;
		ready : out std_logic;
		addr_out : out std_logic_vector(31 downto 0);
		block_modulo_width : out std_logic_vector(31 downto 0);
		block_modulo_height : out std_logic_vector(31 downto 0);
		coef_addr_out : out std_logic_vector(31 downto 0);
		
		clk : in std_logic := '0';
		reset : in std_logic := '0'
	);
	end component addr_distr;
	
	type wind_len_quantized_t is array(natural range <>) of natural;
	
	function init_wind_width_q_arr(constant wind_len_blocks : natural;
					constant dp_amt : natural;
					constant dp_idx : natural)
		return wind_len_quantized_t is
		
		variable wind_len_arr : wind_len_quantized_t(dp_amt-1 downto 0);
	begin
		for i in 0 to dp_amt-1 loop
		        if i >= dp_idx then
				wind_len_arr(i) := (wind_len_blocks + dp_amt - 1 - i + dp_idx)/dp_amt;
			else
				wind_len_arr(i) := (wind_len_blocks - 1 + dp_idx - i)/dp_amt;
			end if;
		end loop;
		return wind_len_arr;
	end function;
	
	constant WIND_WIDTH_BLOCKS_QUANTIZED : wind_len_quantized_t(DP_AMOUNT-1 downto 0) :=
		init_wind_width_q_arr(WIND_WIDTH_BLOCKS,DP_AMOUNT,0);
		
	
	type wind_block_ctr_t is array(natural range <>) of natural range 0 to WIND_WIDTH_BLOCKS*WIND_HEIGHT_BLOCKS-1;
	
	signal wind_block_ctr0 : wind_block_ctr_t(0 to calc_wnd_amt_quantized(WIND_AMT_COL,IMG_WIDTH_BLOCKS,WIND_WIDTH_BLOCKS,0,DP_AMOUNT)*WIND_AMT_ROW-1);
	signal wind_block_ctr1 : wind_block_ctr_t(0 to calc_wnd_amt_quantized(WIND_AMT_COL,IMG_WIDTH_BLOCKS,WIND_WIDTH_BLOCKS,1,DP_AMOUNT)*WIND_AMT_ROW-1);
	signal wind_block_ctr2 : wind_block_ctr_t(0 to calc_wnd_amt_quantized(WIND_AMT_COL,IMG_WIDTH_BLOCKS,WIND_WIDTH_BLOCKS,2,DP_AMOUNT)*WIND_AMT_ROW-1);

	signal cur_ctr0 : natural range 0 to WIND_WIDTH_BLOCKS*WIND_HEIGHT_BLOCKS-1 := 0;
	signal cur_ctr1 : natural range 0 to WIND_WIDTH_BLOCKS*WIND_HEIGHT_BLOCKS-1 := 0;
	signal cur_ctr2 : natural range 0 to WIND_WIDTH_BLOCKS*WIND_HEIGHT_BLOCKS-1 := 0;
	
	type coef_addr_table_t is array(DP_AMOUNT-1 downto 0) of wind_block_ctr_t(0 to WIND_WIDTH_BLOCKS*WIND_HEIGHT_BLOCKS-1);
	
	function coef_table_init(constant wind_width : natural;
				  constant wind_height : natural;
				  constant dp_amt : natural;
				  constant wind_widths_q : wind_len_quantized_t(DP_AMOUNT-1 downto 0))
		return coef_addr_table_t is
		
		variable table : coef_addr_table_t := (others => (others => 0));
	begin
		for i in 0 to wind_height - 1 loop
			for j in 0 to wind_width - 1 loop
				table(j mod dp_amt)(j/dp_amt + wind_widths_q(j mod dp_amt)*i) := j + wind_width*i;
			end loop;
		end loop;
		return table;
	end function;
	constant coef_addr_table : coef_addr_table_t := coef_table_init(WIND_WIDTH_BLOCKS,
									WIND_HEIGHT_BLOCKS,
									DP_AMOUNT,
									WIND_WIDTH_BLOCKS_QUANTIZED);
	
	signal coef_addr0 : natural range 0 to WIND_WIDTH_BLOCKS*WIND_HEIGHT_BLOCKS-1 := 0;
	signal coef_addr1 : natural range 0 to WIND_WIDTH_BLOCKS*WIND_HEIGHT_BLOCKS-1 := 0;
	signal coef_addr2 : natural range 0 to WIND_WIDTH_BLOCKS*WIND_HEIGHT_BLOCKS-1 := 0;
	
	signal wind_rdy0 : boolean;
	signal wind_rdy1 : boolean;
	signal wind_rdy2 : boolean;
	signal overlap : boolean;
	
	signal mem_addr0 : natural;
	signal mem_addr1 : natural;
	signal mem_addr2 : natural;
	
	signal ctr_diff0 :  integer range -WIND_WIDTH_BLOCKS*WIND_HEIGHT_BLOCKS-1 to WIND_WIDTH_BLOCKS*WIND_HEIGHT_BLOCKS-1;
	signal ctr_diff1 :  integer range -WIND_WIDTH_BLOCKS*WIND_HEIGHT_BLOCKS-1 to WIND_WIDTH_BLOCKS*WIND_HEIGHT_BLOCKS-1;
	signal ctr_diff2 :  integer range -WIND_WIDTH_BLOCKS*WIND_HEIGHT_BLOCKS-1 to WIND_WIDTH_BLOCKS*WIND_HEIGHT_BLOCKS-1;
	
	signal sync : std_logic_vector(DP_AMOUNT-1 downto 0);
	signal mem_we : std_logic_vector(DP_AMOUNT-1 downto 0);
	signal sync_sig : std_logic;
	signal clk : std_logic := '0';
	signal rst : std_logic := '0';
	
	type addr_arr is array(DP_AMOUNT-1 downto 0) of std_logic_vector(31 downto 0);
	signal addr_out : addr_arr;
	signal coef_out : addr_arr;
	signal block_modulo_width : addr_arr;
begin

	mem_addr0 <= to_integer(unsigned(addr_out(0)));
	mem_addr1 <= to_integer(unsigned(addr_out(1)));
	mem_addr2 <= to_integer(unsigned(addr_out(2)));
	
	ctr_diff0 <= cur_ctr0 - coef_addr0;
	ctr_diff1 <= cur_ctr1 - coef_addr1;
	ctr_diff2 <= cur_ctr2 - coef_addr2;
	
	wind_rdy0 <= to_integer(unsigned(coef_out(0))) = WIND_WIDTH_BLOCKS_QUANTIZED((WIND_WIDTH_BLOCKS-1) mod DP_AMOUNT)*WIND_HEIGHT_BLOCKS - 1
	           and to_integer(unsigned(block_modulo_width(0))) = ((WIND_WIDTH_BLOCKS-1) mod DP_AMOUNT) and mem_we(0) = '1';
	wind_rdy1 <= to_integer(unsigned(coef_out(1))) = WIND_WIDTH_BLOCKS_QUANTIZED((WIND_WIDTH_BLOCKS-1) mod DP_AMOUNT)*WIND_HEIGHT_BLOCKS - 1
	           and to_integer(unsigned(block_modulo_width(1))) = ((WIND_WIDTH_BLOCKS-1) mod DP_AMOUNT) and mem_we(1) = '1';
	wind_rdy2 <= to_integer(unsigned(coef_out(2))) = WIND_WIDTH_BLOCKS_QUANTIZED((WIND_WIDTH_BLOCKS-1) mod DP_AMOUNT)*WIND_HEIGHT_BLOCKS - 1
	           and to_integer(unsigned(block_modulo_width(2))) = ((WIND_WIDTH_BLOCKS-1) mod DP_AMOUNT) and mem_we(2) = '1';
	overlap <= (wind_rdy0 and wind_rdy1) or (wind_rdy0 and wind_rdy2) or (wind_rdy1 and wind_rdy2);
	-- Here we check if our memory controller is consistant with the behavior of our model
	cnt_proc : process(clk,rst)
	begin
		if rst = '1' then
			wind_block_ctr0 <= (others => 0);
			wind_block_ctr1 <=  (others => 0);
			wind_block_ctr2 <= (others => 0);
			cur_ctr0 <= 0;
			cur_ctr1 <= 0;
			cur_ctr2 <= 0;
			
			coef_addr0 <= 0;
			coef_addr1 <= 0;
			coef_addr2 <= 0;
		elsif rising_edge(clk) then
	
			if mem_we(0) = '1'  then
				if wind_block_ctr0(mem_addr0) = WIND_WIDTH_BLOCKS*WIND_HEIGHT_BLOCKS-1 then
					wind_block_ctr0(mem_addr0) <= 0;
				else
					wind_block_ctr0(mem_addr0) <= wind_block_ctr0(mem_addr0) + 1;
				end if;
				cur_ctr0 <= wind_block_ctr0(mem_addr0);
				coef_addr0 <= coef_addr_table(to_integer(unsigned(block_modulo_width(0))))(to_integer(unsigned(coef_out(0))));
			end if;
				
			if mem_we(1) = '1'  then
				if wind_block_ctr1(mem_addr1) = WIND_WIDTH_BLOCKS*WIND_HEIGHT_BLOCKS-1 then
					wind_block_ctr1(mem_addr1) <= 0;
				else
					wind_block_ctr1(mem_addr1) <= wind_block_ctr1(mem_addr1) + 1;
				end if;
				cur_ctr1 <= wind_block_ctr1(mem_addr1);
				coef_addr1 <= coef_addr_table(to_integer(unsigned(block_modulo_width(1))))(to_integer(unsigned(coef_out(1))));
			end if;
				
			if mem_we(2) = '1'  then
				if wind_block_ctr2(mem_addr2) = WIND_WIDTH_BLOCKS*WIND_HEIGHT_BLOCKS-1 then
					wind_block_ctr2(mem_addr2) <= 0;
				else
					wind_block_ctr2(mem_addr2) <= wind_block_ctr2(mem_addr2) + 1;
				end if;
				cur_ctr2 <= wind_block_ctr2(mem_addr2);
				coef_addr2 <= coef_addr_table(to_integer(unsigned(block_modulo_width(2))))(to_integer(unsigned(coef_out(2))));
			end if;
		end if;
	end process cnt_proc;
	
		
	sync_sig <= sync(0) and sync(1) and sync(2);
	distr_gen : for i in 0 to DP_AMOUNT-1 generate
		addr_distr0 : addr_distr
		generic map
		(
			IMG_WIDTH_PIXELS => IMG_WIDTH_PIXELS,
			IMG_HEIGHT_PIXELS => IMG_HEIGHT_PIXELS,
			WIND_WIDTH_PIXELS => WIND_WIDTH_PIXELS,
			WIND_HEIGHT_PIXELS => WIND_HEIGHT_PIXELS,
			-- m
			DP_AMOUNT_WIDTH => DP_AMOUNT,
			DP_AMOUNT_HEIGHT => 1,
			-- p
			DP_INDEX_WIDTH => i,
			DP_INDEX_HEIGHT => 0
		)
		port map
		(
			addr_out => addr_out(i),
			coef_addr_out => coef_out(i),
			block_modulo_width => block_modulo_width(i),
			ready => sync(i),
			mem_we => mem_we(i),
			en => '1',
			sync_in => sync_sig,
			clk => clk,
			reset => rst
		);
	end generate;
	
	clk <= not clk after 100 ns;
	
	test : process
		file test_out : text;
		variable addrline : line;
		variable coefline : line;
	begin
-- 		file_open(test_out,"test_addr",WRITE_MODE);
		wait for 50 ns;
		rst <= '1';
		wait for 50 ns;
		rst <= '0';
		
		for i in 0 to (IMG_HEIGHT_PIXELS-15)*(IMG_WIDTH_PIXELS-15)-1 loop
-- 			write(addrline,string'("{ "));
-- 			write(coefline,string'("{ "));
			wait until falling_edge(clk);
			while sync_sig = '0' loop
-- 				write(addrline, to_integer(unsigned(addr_out)));
-- 				write(addrline,string'(" "));
-- 				write(coefline, to_integer(unsigned(coef_out)));
-- 				write(coefline,string'(" "));
				wait until falling_edge(clk);
			end loop;
-- 			write(addrline, to_integer(unsigned(addr_out)));
-- 			write(addrline,string'(" "));
-- 			write(addrline,string'("}"));
-- 			write(coefline, to_integer(unsigned(coef_out)));
-- 			write(coefline,string'(" "));
-- 			write(coefline,string'("}"));
-- 			writeline(test_out,addrline);
-- 			writeline(test_out,coefline);
		end loop;
-- 		file_close(test_out);
		assert false report "End of simulation" severity failure;
 
		wait;
	end process test;
end architecture arch;

