library IEEE;

use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity svm_accumulator is
generic(
	DP_AMOUNT_WIDTH : natural := 2;
	USE_XIL_BRAM : boolean := FALSE;
	USE_INFERRED_BRAM : boolean := FALSE;
	IMG_WIDTH_PIXELS : natural := 320;
	IMG_HEIGHT_PIXELS : natural := 240;
	WIND_WIDTH_PIXELS : natural := 64;
	WIND_HEIGHT_PIXELS : natural := 128
);
port(
	reg_data_in : in std_logic_vector(31 downto 0);
	reg_data_out : out std_logic_vector(31 downto 0);
	reg_addr : in std_logic_vector(1 downto 0);
	reg_we : in std_logic;
	reg_re : in std_logic;
	
	block_vec_c0 : in std_logic_vector(14 downto 0);
	block_vec_c1 : in std_logic_vector(14 downto 0);
	block_vec_c2 : in std_logic_vector(14 downto 0);
	block_vec_c3 : in std_logic_vector(14 downto 0);
	block_vec_c4 : in std_logic_vector(14 downto 0);
	block_vec_c5 : in std_logic_vector(14 downto 0);
	block_vec_c6 : in std_logic_vector(14 downto 0);
	block_vec_c7 : in std_logic_vector(14 downto 0);
	block_vec_c8 : in std_logic_vector(14 downto 0);
	block_vec_c9 : in std_logic_vector(14 downto 0);
	block_vec_c10 : in std_logic_vector(14 downto 0);
	block_vec_c11 : in std_logic_vector(14 downto 0);
	block_vec_c12 : in std_logic_vector(14 downto 0);
	block_vec_c13 : in std_logic_vector(14 downto 0);
	block_vec_c14 : in std_logic_vector(14 downto 0);
	block_vec_c15 : in std_logic_vector(14 downto 0);
	block_vec_c16 : in std_logic_vector(14 downto 0);
	block_vec_c17 : in std_logic_vector(14 downto 0);
	block_vec_c18 : in std_logic_vector(14 downto 0);
	block_vec_c19 : in std_logic_vector(14 downto 0);
	block_vec_c20 : in std_logic_vector(14 downto 0);
	block_vec_c21 : in std_logic_vector(14 downto 0);
	block_vec_c22 : in std_logic_vector(14 downto 0);
	block_vec_c23 : in std_logic_vector(14 downto 0);
	block_vec_c24 : in std_logic_vector(14 downto 0);
	block_vec_c25 : in std_logic_vector(14 downto 0);
	block_vec_c26 : in std_logic_vector(14 downto 0);
	block_vec_c27 : in std_logic_vector(14 downto 0);
	block_vec_c28 : in std_logic_vector(14 downto 0);
	block_vec_c29 : in std_logic_vector(14 downto 0);
	block_vec_c30 : in std_logic_vector(14 downto 0);
	block_vec_c31 : in std_logic_vector(14 downto 0);
	block_vec_norm_factor : in std_logic_vector(17 downto 0);
	
	block_vec_available : in std_logic;
	block_vec_nxt : out std_logic;
	
	detected : out std_logic;
	valid_out : out std_logic;
	
	pline_clk : in std_logic := '0';
	pline_rst : in std_logic := '0';
	
	clk : in std_logic;
	reset : in std_logic
);
end entity svm_accumulator;

library IEEE;

use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

architecture rtl of svm_accumulator is
	component accum_bram is
	generic(
		DWIDTH : natural := 57;
		MEM_LEN : natural := 512
	);
	port(
		we : in STD_LOGIC;
		addr0 : in natural range 0 to MEM_LEN-1;
		din0 : in STD_LOGIC_VECTOR(DWIDTH-1 downto 0);
		clk0 : in STD_LOGIC;
		
		addr1 : in natural range 0 to MEM_LEN-1;
		dout1 : out STD_LOGIC_VECTOR(DWIDTH-1 downto 0);
		clk1 : in STD_LOGIC
	);
	end component accum_bram;

	component dot_prod is
	port
	(	
		valid_in : in std_logic := '0';

		x_c0 : in std_logic_vector(14 downto 0);
		x_c1 : in std_logic_vector(14 downto 0);
		x_c2 : in std_logic_vector(14 downto 0);
		x_c3 : in std_logic_vector(14 downto 0);
		x_c4 : in std_logic_vector(14 downto 0);
		x_c5 : in std_logic_vector(14 downto 0);
		x_c6 : in std_logic_vector(14 downto 0);
		x_c7 : in std_logic_vector(14 downto 0);
		x_c8 : in std_logic_vector(14 downto 0);
		x_c9 : in std_logic_vector(14 downto 0);
		x_c10 : in std_logic_vector(14 downto 0);
		x_c11 : in std_logic_vector(14 downto 0);
		x_c12 : in std_logic_vector(14 downto 0);
		x_c13 : in std_logic_vector(14 downto 0);
		x_c14 : in std_logic_vector(14 downto 0);
		x_c15 : in std_logic_vector(14 downto 0);
		x_c16 : in std_logic_vector(14 downto 0);
		x_c17 : in std_logic_vector(14 downto 0);
		x_c18 : in std_logic_vector(14 downto 0);
		x_c19 : in std_logic_vector(14 downto 0);
		x_c20 : in std_logic_vector(14 downto 0);
		x_c21 : in std_logic_vector(14 downto 0);
		x_c22 : in std_logic_vector(14 downto 0);
		x_c23 : in std_logic_vector(14 downto 0);
		x_c24 : in std_logic_vector(14 downto 0);
		x_c25 : in std_logic_vector(14 downto 0);
		x_c26 : in std_logic_vector(14 downto 0);
		x_c27 : in std_logic_vector(14 downto 0);
		x_c28 : in std_logic_vector(14 downto 0);
		x_c29 : in std_logic_vector(14 downto 0);
		x_c30 : in std_logic_vector(14 downto 0);
		x_c31 : in std_logic_vector(14 downto 0);
		
		w_c0 : in std_logic_vector(17 downto 0);
		w_c1 : in std_logic_vector(17 downto 0);
		w_c2 : in std_logic_vector(17 downto 0);
		w_c3 : in std_logic_vector(17 downto 0);
		w_c4 : in std_logic_vector(17 downto 0);
		w_c5 : in std_logic_vector(17 downto 0);
		w_c6 : in std_logic_vector(17 downto 0);
		w_c7 : in std_logic_vector(17 downto 0);
		w_c8 : in std_logic_vector(17 downto 0);
		w_c9 : in std_logic_vector(17 downto 0);
		w_c10 : in std_logic_vector(17 downto 0);
		w_c11 : in std_logic_vector(17 downto 0);
		w_c12 : in std_logic_vector(17 downto 0);
		w_c13 : in std_logic_vector(17 downto 0);
		w_c14 : in std_logic_vector(17 downto 0);
		w_c15 : in std_logic_vector(17 downto 0);
		w_c16 : in std_logic_vector(17 downto 0);
		w_c17 : in std_logic_vector(17 downto 0);
		w_c18 : in std_logic_vector(17 downto 0);
		w_c19 : in std_logic_vector(17 downto 0);
		w_c20 : in std_logic_vector(17 downto 0);
		w_c21 : in std_logic_vector(17 downto 0);
		w_c22 : in std_logic_vector(17 downto 0);
		w_c23 : in std_logic_vector(17 downto 0);
		w_c24 : in std_logic_vector(17 downto 0);
		w_c25 : in std_logic_vector(17 downto 0);
		w_c26 : in std_logic_vector(17 downto 0);
		w_c27 : in std_logic_vector(17 downto 0);
		w_c28 : in std_logic_vector(17 downto 0);
		w_c29 : in std_logic_vector(17 downto 0);
		w_c30 : in std_logic_vector(17 downto 0);
		w_c31 : in std_logic_vector(17 downto 0);
		
		val_out : out std_logic_vector(38 downto 0);
		
		valid_out : out std_logic;
		
		clk : in std_logic;
		reset : in std_logic
	);
	end component dot_prod;

	component addr_distr is
	generic
	(
		IMG_WIDTH_PIXELS : natural := 320;
		IMG_HEIGHT_PIXELS : natural := 240;
		WIND_WIDTH_PIXELS : natural := 64;
		WIND_HEIGHT_PIXELS : natural := 128;
		-- m
		DP_AMOUNT_WIDTH : natural := 3;
		DP_AMOUNT_HEIGHT : natural := 1;
		-- p
		DP_INDEX_WIDTH : natural := 0;
		DP_INDEX_HEIGHT : natural := 0
	);
	port
	(
		en : in std_logic := '0';
		sync_in : in std_logic;
		mem_we : out std_logic;
		ready : out std_logic;
		addr_out : out std_logic_vector(31 downto 0);
		block_modulo_width : out std_logic_vector(31 downto 0);
		block_modulo_height : out std_logic_vector(31 downto 0);
		coef_addr_out : out std_logic_vector(31 downto 0);
		
		clk : in std_logic := '0';
		reset : in std_logic := '0'
	);
	end component addr_distr;
	
	function amt_bit_len(constant amt : natural) return natural is
		variable i : natural := 1;
		variable k : natural;
	begin
		if amt = 0 then
			return 0;
		end if;
		k := amt/2;
		while k > 0 loop
			k := k / 2;
			i := i + 1;
		end loop;
		return i;
	end function;
	
	function calc_wnd_amt_quantized( constant wind_amt_tot : natural;
					  constant img_len_blocks : natural;
					  constant wind_len_blocks : natural;
					  constant dp_index : natural;
					  constant dp_amt : natural)
					  return natural is
		variable b : natural := ((wind_amt_tot-1)/8) mod dp_amt;
		variable c : natural := (wind_amt_tot-1) mod 8;
		variable amt : natural := ((img_len_blocks-wind_len_blocks + (dp_amt-1) - dp_index)/dp_amt)*8;
	begin
		if b = dp_index then
			return amt+c+1;
		else
			return amt;
		end if;
	end function;
	constant WIND_AMT_COL : natural := IMG_WIDTH_PIXELS - WIND_WIDTH_PIXELS + 1;
	constant WIND_AMT_ROW : natural := IMG_HEIGHT_PIXELS - WIND_HEIGHT_PIXELS + 1;
	constant WIND_AMT_TOT : natural := WIND_AMT_ROW*WIND_AMT_COL;
	constant IMG_WIDTH_BLOCKS : natural := (IMG_WIDTH_PIXELS/8) - 1;
	constant IMG_HEIGHT_BLOCKS : natural := (IMG_HEIGHT_PIXELS/8) - 1;
	constant WIND_WIDTH_BLOCKS : natural := (WIND_WIDTH_PIXELS/8) - 1;
	constant WIND_HEIGHT_BLOCKS : natural := (WIND_HEIGHT_PIXELS/8) - 1;

	-- SVM coef memory quantization
	type wind_len_quantized_t is array(natural range <>) of natural;
	
	function init_wind_width_q_arr(constant wind_len_blocks : natural;
					constant dp_amt : natural;
					constant dp_idx : natural)
		return wind_len_quantized_t is
		
		variable wind_len_arr : wind_len_quantized_t(dp_amt-1 downto 0);
	begin
		for i in 0 to dp_amt-1 loop
		        if i >= dp_idx then
				wind_len_arr(i) := (wind_len_blocks + dp_amt - 1 - i + dp_idx)/dp_amt;
			else
				wind_len_arr(i) := (wind_len_blocks - 1 + dp_idx - i)/dp_amt;
			end if;
		end loop;
		return wind_len_arr;
	end function;
	
	constant WIND_WIDTH_BLOCKS_QUANTIZED : wind_len_quantized_t(DP_AMOUNT_WIDTH-1 downto 0) :=
		init_wind_width_q_arr(WIND_WIDTH_BLOCKS,DP_AMOUNT_WIDTH,0);
		
	-- Block vector buffering
	signal buff0_0 : std_logic_vector(14 downto 0) := (others => '0');
	signal buff0_1 : std_logic_vector(14 downto 0) := (others => '0');
	signal buff0_2 : std_logic_vector(14 downto 0) := (others => '0');
	signal buff0_3 : std_logic_vector(14 downto 0) := (others => '0');
	signal buff0_4 : std_logic_vector(14 downto 0) := (others => '0');
	signal buff0_5 : std_logic_vector(14 downto 0) := (others => '0');
	signal buff0_6 : std_logic_vector(14 downto 0) := (others => '0');
	signal buff0_7 : std_logic_vector(14 downto 0) := (others => '0');
	signal buff0_8 : std_logic_vector(14 downto 0) := (others => '0');
	signal buff0_9 : std_logic_vector(14 downto 0) := (others => '0');
	signal buff0_10 : std_logic_vector(14 downto 0) := (others => '0');
	signal buff0_11 : std_logic_vector(14 downto 0) := (others => '0');
	signal buff0_12 : std_logic_vector(14 downto 0) := (others => '0');
	signal buff0_13 : std_logic_vector(14 downto 0) := (others => '0');
	signal buff0_14 : std_logic_vector(14 downto 0) := (others => '0');
	signal buff0_15 : std_logic_vector(14 downto 0) := (others => '0');
	signal buff0_16 : std_logic_vector(14 downto 0) := (others => '0');
	signal buff0_17 : std_logic_vector(14 downto 0) := (others => '0');
	signal buff0_18 : std_logic_vector(14 downto 0) := (others => '0');
	signal buff0_19 : std_logic_vector(14 downto 0) := (others => '0');
	signal buff0_20 : std_logic_vector(14 downto 0) := (others => '0');
	signal buff0_21 : std_logic_vector(14 downto 0) := (others => '0');
	signal buff0_22 : std_logic_vector(14 downto 0) := (others => '0');
	signal buff0_23 : std_logic_vector(14 downto 0) := (others => '0');
	signal buff0_24 : std_logic_vector(14 downto 0) := (others => '0');
	signal buff0_25 : std_logic_vector(14 downto 0) := (others => '0');
	signal buff0_26 : std_logic_vector(14 downto 0) := (others => '0');
	signal buff0_27 : std_logic_vector(14 downto 0) := (others => '0');
	signal buff0_28 : std_logic_vector(14 downto 0) := (others => '0');
	signal buff0_29 : std_logic_vector(14 downto 0) := (others => '0');
	signal buff0_30 : std_logic_vector(14 downto 0) := (others => '0');
	signal buff0_31 : std_logic_vector(14 downto 0) := (others => '0');
	
	-- SVM bias
	signal svm_bias : std_logic_vector(56 downto 0) := (others => '0');
	signal svm_bias_b : std_logic_vector(56 downto 0) := (others => '0');

	type coef_reg_t is array(3 downto 0) of std_logic_vector(17 downto 0);
	type coef_mem_we_t is array(DP_AMOUNT_WIDTH-1 downto 0) of std_logic_vector(7 downto 0);
	signal coef_mem_we : coef_mem_we_t;
	signal coef_mem_din : std_logic_vector(71 downto 0);
	signal coef_mem_reg : coef_reg_t := (others => (others => '0'));
	type coef_mem_port_t is array(DP_AMOUNT_WIDTH-1 downto 0) of std_logic_vector(18*32-1 downto 0);
	signal coef_mem_rdport : coef_mem_port_t;
	signal coef_mem_rdport_sel : coef_mem_port_t;
	
	-- Window accumulator memories
	type mem_port_t is array(DP_AMOUNT_WIDTH-1 downto 0) of std_logic_vector(56 downto 0);
	signal mem_port0_din : mem_port_t;
	signal mem_port0_din0 : mem_port_t := (others => (others => '0'));
	signal mem_port0_din_add_sel : mem_port_t;
	signal mem_port1_dout : mem_port_t;
	
	type mem_addr_t is array(DP_AMOUNT_WIDTH-1 downto 0) of natural range 0 to WIND_AMT_COL*WIND_AMT_ROW;
	signal mem_addr : mem_addr_t;
	signal mem_addr0 : mem_addr_t := (others => 0);
	signal mem_addr1 : mem_addr_t := (others => 0);
	signal mem_addr2 : mem_addr_t := (others => 0);
	signal mem_addr3 : mem_addr_t := (others => 0);
	signal mem_addr4 : mem_addr_t := (others => 0);
	signal mem_addr5 : mem_addr_t := (others => 0);
	signal mem_addr6 : mem_addr_t := (others => 0);
	signal mem_addr7 : mem_addr_t := (others => 0);
	signal mem_addr8 : mem_addr_t := (others => 0);
	
	signal wind_rdy : std_logic_vector(DP_AMOUNT_WIDTH-1 downto 0);
	signal wind_rdy0 : std_logic_vector(DP_AMOUNT_WIDTH-1 downto 0) := (others => '0');
	signal wind_rdy1 : std_logic_vector(DP_AMOUNT_WIDTH-1 downto 0) := (others => '0');
	signal wind_rdy2 : std_logic_vector(DP_AMOUNT_WIDTH-1 downto 0) := (others => '0');
	signal wind_rdy3 : std_logic_vector(DP_AMOUNT_WIDTH-1 downto 0) := (others => '0');
	signal wind_rdy4 : std_logic_vector(DP_AMOUNT_WIDTH-1 downto 0) := (others => '0');
	signal wind_rdy5 : std_logic_vector(DP_AMOUNT_WIDTH-1 downto 0) := (others => '0');
	signal wind_rdy6 : std_logic_vector(DP_AMOUNT_WIDTH-1 downto 0) := (others => '0');
	signal wind_rdy7 : std_logic_vector(DP_AMOUNT_WIDTH-1 downto 0) := (others => '0');
	signal wind_rdy8 : std_logic_vector(DP_AMOUNT_WIDTH-1 downto 0) := (others => '0');
	
	-- Signals coming from the memory controllers
	signal sync : std_logic_vector(DP_AMOUNT_WIDTH-1 downto 0);
	signal sync_sig : std_logic;
	signal mem_we : std_logic_vector(DP_AMOUNT_WIDTH-1 downto 0);
	signal mem_we0 : std_logic_vector(DP_AMOUNT_WIDTH-1 downto 0) := (others => '0');
	signal mem_we1 : std_logic_vector(DP_AMOUNT_WIDTH-1 downto 0) := (others => '0');
	signal mem_we2 : std_logic_vector(DP_AMOUNT_WIDTH-1 downto 0) := (others => '0');
	signal mem_we3 : std_logic_vector(DP_AMOUNT_WIDTH-1 downto 0) := (others => '0');
	signal mem_we4 : std_logic_vector(DP_AMOUNT_WIDTH-1 downto 0) := (others => '0');
	signal mem_we5 : std_logic_vector(DP_AMOUNT_WIDTH-1 downto 0) := (others => '0');
	signal mem_we6 : std_logic_vector(DP_AMOUNT_WIDTH-1 downto 0) := (others => '0');
	signal mem_we7 : std_logic_vector(DP_AMOUNT_WIDTH-1 downto 0) := (others => '0');
	signal mem_we8 : std_logic_vector(DP_AMOUNT_WIDTH-1 downto 0) := (others => '0');
	type addr_arr is array(DP_AMOUNT_WIDTH-1 downto 0) of std_logic_vector(31 downto 0);
	signal addr_out : addr_arr;
	signal coef_out : addr_arr;
	signal block_modulo_width : addr_arr;
	type block_mod_width_arr_t is array(0 to DP_AMOUNT_WIDTH-1) of natural range 0 to DP_AMOUNT_WIDTH-1;
	signal block_mod_width : block_mod_width_arr_t;
	type wind_block_addr_t is array(DP_AMOUNT_WIDTH-1 downto 0) of natural range 0 to WIND_WIDTH_BLOCKS*WIND_HEIGHT_BLOCKS - 1;
	signal wind_block_addr : wind_block_addr_t;
	signal window_init : std_logic_vector(DP_AMOUNT_WIDTH-1 downto 0);
	signal window_init0 : std_logic_vector(DP_AMOUNT_WIDTH-1 downto 0) := (others => '0');
	signal window_init1 : std_logic_vector(DP_AMOUNT_WIDTH-1 downto 0) := (others => '0');
	signal window_init2 : std_logic_vector(DP_AMOUNT_WIDTH-1 downto 0) := (others => '0');
	signal window_init3 : std_logic_vector(DP_AMOUNT_WIDTH-1 downto 0) := (others => '0');
	signal window_init4 : std_logic_vector(DP_AMOUNT_WIDTH-1 downto 0) := (others => '0');
	signal window_init5 : std_logic_vector(DP_AMOUNT_WIDTH-1 downto 0) := (others => '0');
	signal window_init6 : std_logic_vector(DP_AMOUNT_WIDTH-1 downto 0) := (others => '0');
	signal window_init7 : std_logic_vector(DP_AMOUNT_WIDTH-1 downto 0) := (others => '0');
	signal coef_mem_rd_addr : wind_block_addr_t;
	signal coef_mem_wr_addr : wind_block_addr_t;
	
	-- Dot-product signals
	type dp_res_t is array(DP_AMOUNT_WIDTH-1 downto 0) of std_logic_vector(38 downto 0);
	signal dp_res : dp_res_t;
	signal dp_valid : std_logic_vector(DP_AMOUNT_WIDTH-1 downto 0);
	
	signal norm0 : signed(17 downto 0) := (others => '0');
	signal norm1 : signed(17 downto 0) := (others => '0');
	signal norm2 : signed(17 downto 0) := (others => '0');
	signal norm3 : signed(17 downto 0) := (others => '0');
	signal norm4 : signed(17 downto 0) := (others => '0');
	signal norm5 : signed(17 downto 0) := (others => '0');
	signal norm6 : signed(17 downto 0) := (others => '0');
	type dp_res_norm_t is array(DP_AMOUNT_WIDTH-1 downto 0) of signed(56 downto 0);
	signal dp_res_norm : dp_res_norm_t;
	signal dp_res_norm0 : dp_res_norm_t := (others => (others => '0'));
	
	signal dp_rst : std_logic_vector(DP_AMOUNT_WIDTH-1 downto 0) := (others => '0');
	signal addr_distr_rst : std_logic_vector(DP_AMOUNT_WIDTH-1 downto 0) := (others => '0');
	
	alias bank_idx_v : std_logic_vector(2 downto 0) is reg_data_in(2 downto 0);
	alias dp_idx_v : std_logic_vector(amt_bit_len(DP_AMOUNT_WIDTH-1)+2 downto 3) is reg_data_in(amt_bit_len(DP_AMOUNT_WIDTH-1)+2 downto 3);
	alias cmem_addr : std_logic_vector(31 downto amt_bit_len(DP_AMOUNT_WIDTH-1)+3) is reg_data_in(31 downto amt_bit_len(DP_AMOUNT_WIDTH-1)+3);
begin
	pline_proc : process(pline_clk)
		type barr is array(DP_AMOUNT_WIDTH-1 downto 0) of boolean;
		variable tmp : std_logic;
		variable tmp2 : barr;
		variable tmp3 : boolean;
	begin
		if rising_edge(pline_clk) then
			if pline_rst = '1' then
				mem_we0 <= (others => '0');
				mem_we1 <= (others => '0');
				mem_we2 <= (others => '0');
				mem_we3 <= (others => '0');
				mem_we4 <= (others => '0');
				mem_we5 <= (others => '0');
				mem_we6 <= (others => '0');
				mem_we7 <= (others => '0');
				mem_we8 <= (others => '0');
				wind_rdy0 <= (others => '0');
				wind_rdy1 <= (others => '0');
				wind_rdy2 <= (others => '0');
				wind_rdy3 <= (others => '0');
				wind_rdy4 <= (others => '0');
				wind_rdy5 <= (others => '0');
				wind_rdy6 <= (others => '0');
				wind_rdy7 <= (others => '0');
				wind_rdy8 <= (others => '0');
				window_init0 <= (others => '0');
				window_init1 <= (others => '0');
				window_init2 <= (others => '0');
				window_init3 <= (others => '0');
				window_init4 <= (others => '0');
				window_init5 <= (others => '0');
				window_init6 <= (others => '0');
				window_init7 <= (others => '0');
				svm_bias_b <= (others => '0');
				
				detected <= '0';
				valid_out <= '0';
			else
				for i in 0 to DP_AMOUNT_WIDTH-1 loop
					tmp2(i) := (wind_rdy8(i) = '1') and (signed(mem_port0_din0(i)) > to_signed(0,57));
				end loop;
				
				tmp := wind_rdy8(0);
				tmp3 := tmp2(0);
				for i in 1 to DP_AMOUNT_WIDTH-1 loop
					tmp := tmp or wind_rdy8(i);
					tmp3 := tmp3 or tmp2(i);
				end loop;
				--valid_out <= dp_valid(0) or dp_valid(1) or dp_valid(2);
				valid_out <= tmp;
				if tmp3 then
					detected <= '1';
				else 
					detected <= '0';
				end if;
				
				
				-- Synchronize with DP pipe-line
				mem_port0_din0 <= mem_port0_din;
				mem_we0 <= mem_we;
				mem_we1 <= mem_we0;
				mem_we2 <= mem_we1;
				mem_we3 <= mem_we2;
				mem_we4 <= mem_we3;
				mem_we5 <= mem_we4;
				mem_we6 <= mem_we5;
				mem_we7 <= mem_we6;
				mem_we8 <= mem_we7;
				wind_rdy0 <= wind_rdy;
				wind_rdy1 <= wind_rdy0;
				wind_rdy2 <= wind_rdy1;
				wind_rdy3 <= wind_rdy2;
				wind_rdy4 <= wind_rdy3;
				wind_rdy5 <= wind_rdy4;
				wind_rdy6 <= wind_rdy5;
				wind_rdy7 <= wind_rdy6;
				wind_rdy8 <= wind_rdy7;
				window_init0 <= window_init;
				window_init1 <= window_init0;
				window_init2 <= window_init1;
				window_init3 <= window_init2;
				window_init4 <= window_init3;
				window_init5 <= window_init4;
				window_init6 <= window_init5;
				window_init7 <= window_init6;
				svm_bias_b <= svm_bias;
			end if;
			
			dp_rst <= (others => pline_rst);
			addr_distr_rst <= (others => pline_rst);
			
			mem_addr0 <= mem_addr;
			mem_addr1 <= mem_addr0;
			mem_addr2 <= mem_addr1;
			mem_addr3 <= mem_addr2;
			mem_addr4 <= mem_addr3;
			mem_addr5 <= mem_addr4;
			mem_addr6 <= mem_addr5;
			mem_addr7 <= mem_addr6;
			mem_addr8 <= mem_addr7;
				
			buff0_0 <= block_vec_c0;
			buff0_1 <= block_vec_c1;
			buff0_2 <= block_vec_c2;
			buff0_3 <= block_vec_c3;
			buff0_4 <= block_vec_c4;
			buff0_5 <= block_vec_c5;
			buff0_6 <= block_vec_c6;
			buff0_7 <= block_vec_c7;
			buff0_8 <= block_vec_c8;
			buff0_9 <= block_vec_c9;
			buff0_10 <= block_vec_c10;
			buff0_11 <= block_vec_c11;
			buff0_12 <= block_vec_c12;
			buff0_13 <= block_vec_c13;
			buff0_14 <= block_vec_c14;
			buff0_15 <= block_vec_c15;
			buff0_16 <= block_vec_c16;
			buff0_17 <= block_vec_c17;
			buff0_18 <= block_vec_c18;
			buff0_19 <= block_vec_c19;
			buff0_20 <= block_vec_c20;
			buff0_21 <= block_vec_c21;
			buff0_22 <= block_vec_c22;
			buff0_23 <= block_vec_c23;
			buff0_24 <= block_vec_c24;
			buff0_25 <= block_vec_c25;
			buff0_26 <= block_vec_c26;
			buff0_27 <= block_vec_c27;
			buff0_28 <= block_vec_c28;
			buff0_29 <= block_vec_c29;
			buff0_30 <= block_vec_c30;
			buff0_31 <= block_vec_c31;
			
			norm0 <= signed(block_vec_norm_factor);
			norm1 <= norm0;
			norm2 <= norm1;
			norm3 <= norm2;
			norm4 <= norm3;
			norm5 <= norm4;
			norm6 <= norm5;
			
			dp_res_norm0 <= dp_res_norm;
		end if;
	end process pline_proc;
	
	-- Register write interface (to load SVM weights and bias)
	reg_wr_proc : process(clk,reset,reg_data_in,reg_addr,reg_we)
		variable decode : std_logic_vector(3 downto 0);
		variable reg_idx : natural range 0 to 3;
		variable coef_mem_reg_en : std_logic_vector(3 downto 0);
	begin
		reg_idx := to_integer(unsigned(reg_data_in(19 downto 18)));
		decode := (others => '0');
		decode(reg_idx) := '1';
		if reg_we = '1' and reg_addr = "00" then
			coef_mem_reg_en := decode;
		else
			coef_mem_reg_en := (others => '0');
		end if;
			
		if reset = '1' then
			svm_bias <= (others => '0');
			coef_mem_reg <= (others => (others => '0'));
		elsif rising_edge(clk) then
			if reg_we = '1' then
				case reg_addr is
				when "10" =>
					svm_bias(31 downto 0) <= reg_data_in;
				when "11" =>
					svm_bias(56 downto 32) <= reg_data_in(24 downto 0);
				when others => null;
				end case;
			end if;
			
			for i in 0 to 3 loop
				if coef_mem_reg_en(i) = '1' then
					coef_mem_reg(i) <= reg_data_in(17 downto 0);
				end if;
			end loop;
		end if;
	end process reg_wr_proc;
	

	-- If all memory controllers are ready, signal next block read
	block_vec_nxt <= sync_sig;

	coef_mem_din <= coef_mem_reg(3)&coef_mem_reg(2)&coef_mem_reg(1)&coef_mem_reg(0);
	coef_mem_addrwe_proc : process(reg_data_in,reg_we,reg_addr,wind_block_addr,block_mod_width)
		variable bank_idx : natural range 0 to 7;
		variable bank_decode : std_logic_vector(7 downto 0);
	begin
		bank_idx := to_integer(unsigned(bank_idx_v));
		bank_decode := (others => '0');
		bank_decode(bank_idx) := '1';
		
		
		coef_mem_we <= (others => (others => '0'));
		coef_mem_rd_addr <= wind_block_addr;
		
		for i in 0 to DP_AMOUNT_WIDTH-1 loop
			for j in 0 to DP_AMOUNT_WIDTH-1 loop
				if block_mod_width(i) = j then
					coef_mem_rd_addr(j) <= wind_block_addr(i);
				end if;
			end loop;
		end loop;
		
		coef_mem_wr_addr <= (others => 0);
		if reg_we = '1' and reg_addr = "01" then
-- 			for i in 0 to DP_AMOUNT_WIDTH-1 loop
-- 				if reg_data_in(amt_bit_len(DP_AMOUNT_WIDTH-1)+2 downto 3) = std_logic_vector(to_unsigned(i,amt_bit_len(DP_AMOUNT_WIDTH-1))) then
-- 				if reg_data_in(3 downto 3) = std_logic_vector(to_unsigned(i,1)) then
-- 					coef_mem_wr_addr(i) <= to_integer(unsigned(reg_data_in(31 downto amt_bit_len(DP_AMOUNT_WIDTH-1)+2)));
-- 					coef_mem_we(i) <= bank_decode;
-- 				end if;
-- 			end loop;
			coef_mem_we(to_integer(unsigned(dp_idx_v))) <= bank_decode;
			coef_mem_wr_addr(to_integer(unsigned(dp_idx_v))) <= to_integer(unsigned(cmem_addr));
		end if;
	end process coef_mem_addrwe_proc;
	
	sync_proc : process(sync)
		variable tmp : std_logic;
	begin
		tmp := sync(0);
		for i in 1 to DP_AMOUNT_WIDTH-1 loop
			tmp := tmp and sync(i);
		end loop;
		sync_sig <= tmp;
	end process sync_proc;
	dp_gen : for i in 0 to DP_AMOUNT_WIDTH-1 generate
		cbram_gen : for j in 0 to 7 generate
			cbram : accum_bram
			generic map
			(
				DWIDTH => 72,
				MEM_LEN => (WIND_WIDTH_BLOCKS_QUANTIZED(i)*WIND_HEIGHT_BLOCKS)+1
			)
			port map
			(
				we => coef_mem_we(i)(j),
				addr0 => coef_mem_wr_addr(i),
				din0 => coef_mem_din,
				clk0 => clk,
				
				addr1 => coef_mem_rd_addr(i),
				dout1 => coef_mem_rdport(i)( ((j+1)*72 - 1) downto (j*72) ),
				clk1 => pline_clk
			);
		end generate cbram_gen;
	
	
		mem_addr(i) <= to_integer(unsigned(addr_out(i)));
		mem_port0_din(i) <= std_logic_vector(dp_res_norm0(i) + signed(mem_port0_din_add_sel(i)));
		mem_port0_din_add_sel(i) <= svm_bias_b when window_init7(i) = '1' else mem_port1_dout(i);
		acc_mem : accum_bram
		generic map
		(
			DWIDTH => 57,
			MEM_LEN => (calc_wnd_amt_quantized(WIND_AMT_COL,IMG_WIDTH_BLOCKS,WIND_WIDTH_BLOCKS,i,DP_AMOUNT_WIDTH)*WIND_AMT_ROW)
		)
		port map
		(
			we => mem_we8(i),
			addr0 => mem_addr8(i),
			din0 => mem_port0_din0(i),
			clk0 => pline_clk,
			
			addr1 => mem_addr6(i),
			dout1 => mem_port1_dout(i),
			clk1 => pline_clk
		);
		
		wind_rdy(i) <= '1' when wind_block_addr(i) = (WIND_WIDTH_BLOCKS_QUANTIZED((WIND_WIDTH_BLOCKS-1) mod DP_AMOUNT_WIDTH)*WIND_HEIGHT_BLOCKS - 1)
		                           and block_mod_width(i) = ((WIND_WIDTH_BLOCKS-1) mod DP_AMOUNT_WIDTH) 
		                            and mem_we(i) = '1' else '0';
	           
		wind_block_addr(i) <= 0 when (to_integer(unsigned(coef_out(i))) > (WIND_WIDTH_BLOCKS_QUANTIZED(block_mod_width(i))*WIND_HEIGHT_BLOCKS - 1)) else to_integer(unsigned(coef_out(i)));
		block_mod_width(i) <= to_integer(unsigned(block_modulo_width(i)));
		window_init(i) <= '1' when (wind_block_addr(i) = 0 and block_mod_width(i) = 0) else '0';
		addr_distr0 : addr_distr
		generic map
		(
			IMG_WIDTH_PIXELS => IMG_WIDTH_PIXELS,
			IMG_HEIGHT_PIXELS => IMG_HEIGHT_PIXELS,
			WIND_WIDTH_PIXELS => WIND_WIDTH_PIXELS,
			WIND_HEIGHT_PIXELS => WIND_HEIGHT_PIXELS,
			-- m
			DP_AMOUNT_WIDTH => DP_AMOUNT_WIDTH,
			DP_AMOUNT_HEIGHT => 1,
			-- p
			DP_INDEX_WIDTH => i,
			DP_INDEX_HEIGHT => 0
		)
		port map
		(
			addr_out => addr_out(i),
			coef_addr_out => coef_out(i),
			block_modulo_width => block_modulo_width(i),
			ready => sync(i),
			mem_we => mem_we(i),
			en => block_vec_available,
			sync_in => sync_sig,
			clk => pline_clk,
			reset => addr_distr_rst(i)
		);
		
		coef_mem_rdport_sel_proc : process(block_mod_width,coef_mem_rdport)
		begin
			coef_mem_rdport_sel(i) <= coef_mem_rdport(0);
			for j in 1 to DP_AMOUNT_WIDTH-1 loop
				if block_mod_width(i) = j then
					coef_mem_rdport_sel(i) <= coef_mem_rdport(j);
				end if;
			end loop;
			
		end process coef_mem_rdport_sel_proc;
		
		dp_res_norm(i) <= signed(dp_res(i)) * norm6;
		dp : dot_prod
		port map(
			--valid_in => block_vec_available,

			x_c0 => buff0_0,
			x_c1 => buff0_1,
			x_c2 => buff0_2,
			x_c3 => buff0_3,
			x_c4 => buff0_4,
			x_c5 => buff0_5,
			x_c6 => buff0_6,
			x_c7 => buff0_7,
			x_c8 => buff0_8,
			x_c9 => buff0_9,
			x_c10 => buff0_10,
			x_c11 => buff0_11,
			x_c12 => buff0_12,
			x_c13 => buff0_13,
			x_c14 => buff0_14,
			x_c15 => buff0_15,
			x_c16 => buff0_16,
			x_c17 => buff0_17,
			x_c18 => buff0_18,
			x_c19 => buff0_19,
			x_c20 => buff0_20,
			x_c21 => buff0_21,
			x_c22 => buff0_22,
			x_c23 => buff0_23,
			x_c24 => buff0_24,
			x_c25 => buff0_25,
			x_c26 => buff0_26,
			x_c27 => buff0_27,
			x_c28 => buff0_28,
			x_c29 => buff0_29,
			x_c30 => buff0_30,
			x_c31 => buff0_31,
			
			w_c0 => coef_mem_rdport_sel(i)(17 downto 0),
			w_c1 => coef_mem_rdport_sel(i)(18*2-1 downto 18),
			w_c2 => coef_mem_rdport_sel(i)(18*3-1 downto 18*2),
			w_c3 => coef_mem_rdport_sel(i)(18*4-1 downto 18*3),
			w_c4 => coef_mem_rdport_sel(i)(18*5-1 downto 18*4),
			w_c5 => coef_mem_rdport_sel(i)(18*6-1 downto 18*5),
			w_c6 => coef_mem_rdport_sel(i)(18*7-1 downto 18*6),
			w_c7 => coef_mem_rdport_sel(i)(18*8-1 downto 18*7),
			w_c8 => coef_mem_rdport_sel(i)(18*9-1 downto 18*8),
			w_c9 => coef_mem_rdport_sel(i)(18*10-1 downto 18*9),
			w_c10 => coef_mem_rdport_sel(i)(18*11-1 downto 18*10),
			w_c11 => coef_mem_rdport_sel(i)(18*12-1 downto 18*11),
			w_c12 => coef_mem_rdport_sel(i)(18*13-1 downto 18*12),
			w_c13 => coef_mem_rdport_sel(i)(18*14-1 downto 18*13),
			w_c14 => coef_mem_rdport_sel(i)(18*15-1 downto 18*14),
			w_c15 => coef_mem_rdport_sel(i)(18*16-1 downto 18*15),
			w_c16 => coef_mem_rdport_sel(i)(18*17-1 downto 18*16),
			w_c17 => coef_mem_rdport_sel(i)(18*18-1 downto 18*17),
			w_c18 => coef_mem_rdport_sel(i)(18*19-1 downto 18*18),
			w_c19 => coef_mem_rdport_sel(i)(18*20-1 downto 18*19),
			w_c20 => coef_mem_rdport_sel(i)(18*21-1 downto 18*20),
			w_c21 => coef_mem_rdport_sel(i)(18*22-1 downto 18*21),
			w_c22 => coef_mem_rdport_sel(i)(18*23-1 downto 18*22),
			w_c23 => coef_mem_rdport_sel(i)(18*24-1 downto 18*23),
			w_c24 => coef_mem_rdport_sel(i)(18*25-1 downto 18*24),
			w_c25 => coef_mem_rdport_sel(i)(18*26-1 downto 18*25),
			w_c26 => coef_mem_rdport_sel(i)(18*27-1 downto 18*26),
			w_c27 => coef_mem_rdport_sel(i)(18*28-1 downto 18*27),
			w_c28 => coef_mem_rdport_sel(i)(18*29-1 downto 18*28),
			w_c29 => coef_mem_rdport_sel(i)(18*30-1 downto 18*29),
			w_c30 => coef_mem_rdport_sel(i)(18*31-1 downto 18*30),
			w_c31 => coef_mem_rdport_sel(i)(18*32-1 downto 18*31),
			
			val_out => dp_res(i),
			--valid_out => dp_valid(i),
			
			clk => pline_clk,
			reset => dp_rst(i)
		);
	end generate dp_gen;
end architecture rtl;