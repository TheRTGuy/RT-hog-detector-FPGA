library IEEE;

use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity accum_bram is
generic(
	DWIDTH : natural := 57;
	MEM_LEN : natural := 512
);
port(
	we : in STD_LOGIC;
	addr0 : in natural range 0 to MEM_LEN-1;
	din0 : in STD_LOGIC_VECTOR(DWIDTH-1 downto 0);
	clk0 : in STD_LOGIC;
	
	addr1 : in natural range 0 to MEM_LEN-1;
	dout1 : out STD_LOGIC_VECTOR(DWIDTH-1 downto 0);
	clk1 : in STD_LOGIC
);
end entity accum_bram;

architecture rtl of accum_bram is
	type mem_t is array(0 to MEM_LEN-1) of STD_LOGIC_VECTOR(DWIDTH-1 downto 0);
	signal mem : mem_t := (others => (others => '0'));
	
	attribute ram_style : string;
	attribute ram_style of mem : signal is "block";
begin
	bram_port0 : process(clk0)
	begin
		if rising_edge(clk0) then
			if we = '1' then
				mem(addr0) <= din0;
			end if;
		end if;
	end process bram_port0;
	
	bram_port1 : process(clk1)
	begin
		if rising_edge(clk1) then
			dout1 <= mem(addr1);
		end if;
	end process bram_port1;
end architecture rtl;