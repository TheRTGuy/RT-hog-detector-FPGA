library IEEE;

use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity block_extract_pline is
	generic
	(
		IMG_WIDTH : natural := 320;
		IMG_HEIGHT : natural := 240
	);
	port
	(
		data_in : in std_logic_vector(7 downto 0);
		valid_in : in std_logic;
		
		block_vec_nxt : in std_logic;
		
		feat_out0 : out std_logic_vector(14 downto 0);
		feat_out1 : out std_logic_vector(14 downto 0);
		feat_out2 : out std_logic_vector(14 downto 0);
		feat_out3 : out std_logic_vector(14 downto 0);
		feat_out4 : out std_logic_vector(14 downto 0);
		feat_out5 : out std_logic_vector(14 downto 0);
		feat_out6 : out std_logic_vector(14 downto 0);
		feat_out7 : out std_logic_vector(14 downto 0);
		feat_out8 : out std_logic_vector(14 downto 0);
		feat_out9 : out std_logic_vector(14 downto 0);
		feat_out10 : out std_logic_vector(14 downto 0);
		feat_out11 : out std_logic_vector(14 downto 0);
		feat_out12 : out std_logic_vector(14 downto 0);
		feat_out13 : out std_logic_vector(14 downto 0);
		feat_out14 : out std_logic_vector(14 downto 0);
		feat_out15 : out std_logic_vector(14 downto 0);
		feat_out16 : out std_logic_vector(14 downto 0);
		feat_out17 : out std_logic_vector(14 downto 0);
		feat_out18 : out std_logic_vector(14 downto 0);
		feat_out19 : out std_logic_vector(14 downto 0);
		feat_out20 : out std_logic_vector(14 downto 0);
		feat_out21 : out std_logic_vector(14 downto 0);
		feat_out22 : out std_logic_vector(14 downto 0);
		feat_out23 : out std_logic_vector(14 downto 0);
		feat_out24 : out std_logic_vector(14 downto 0);
		feat_out25 : out std_logic_vector(14 downto 0);
		feat_out26 : out std_logic_vector(14 downto 0);
		feat_out27 : out std_logic_vector(14 downto 0);
		feat_out28 : out std_logic_vector(14 downto 0);
		feat_out29 : out std_logic_vector(14 downto 0);
		feat_out30 : out std_logic_vector(14 downto 0);
		feat_out31 : out std_logic_vector(14 downto 0);
		norm_out : out std_logic_vector(17 downto 0);
		
		valid_out : out std_logic;
	
		
		clk : in std_logic := '0';
		reset : in std_logic := '0'
	);
end entity block_extract_pline;

architecture rtl of block_extract_pline is
	component hor_edge_detect is
	generic(
		DATA_IN_WIDTH : natural := 8
		);
	port( 
		data_in : in std_logic_vector(DATA_IN_WIDTH-1 downto 0);
		data_valid : in std_logic;
		
		en : in std_logic;
		
		data_out : out std_logic_vector(DATA_IN_WIDTH downto 0);
		data_valid_o : out std_logic;
		
		clk			:	in    	std_logic := '0';
		reset			:	in    	std_logic := '0'
	);
	end component hor_edge_detect;
	component ver_edge_detect is
	generic
	(
		DATA_IN_WIDTH : natural := 8;
		IMG_WIDTH : natural := 320
	);
	port( 
		data_in : in std_logic_vector(DATA_IN_WIDTH-1 downto 0);
		data_valid : in std_logic;
		
		en : in std_logic;
		
		data_out : out std_logic_vector(DATA_IN_WIDTH downto 0);
		data_valid_o : out std_logic;
		
		clk			:	in    	std_logic := '0';
		reset			:	in    	std_logic := '0'
	);
	end component ver_edge_detect;
	component edge_mag_orient is
	generic(
		CORDIC_LENGTH : natural := 7
	);
	port( 
		data_in : in std_logic_vector(31 downto 0);
		data_valid : in std_logic;
		
		en : in std_logic;
		
		data_out : out std_logic_vector(31 downto 0);
		data_valid_o : out std_logic;
		
		clk			:	in    	std_logic := '0';
		reset			:	in    	std_logic := '0'
	);
	end component edge_mag_orient;
	component sliding_hog is
	generic
	(
		IMG_WIDTH : natural := 320;
		IMG_HEIGHT : natural := 240
	);
	port
	(
		mag_in : in std_logic_vector(8 downto 0);
		orient_in : in std_logic_vector(15 downto 0);
		
		valid_in : in std_logic;
		
		en : in std_logic;
		
		cell0_bin0_out : out std_logic_vector(14 downto 0); 
		cell0_bin1_out : out std_logic_vector(14 downto 0);
		cell0_bin2_out : out std_logic_vector(14 downto 0);
		cell0_bin3_out : out std_logic_vector(14 downto 0);
		cell0_bin4_out : out std_logic_vector(14 downto 0);
		cell0_bin5_out : out std_logic_vector(14 downto 0);
		cell0_bin6_out : out std_logic_vector(14 downto 0);
		cell0_bin7_out : out std_logic_vector(14 downto 0);
		
		cell1_bin0_out : out std_logic_vector(14 downto 0); 
		cell1_bin1_out : out std_logic_vector(14 downto 0);
		cell1_bin2_out : out std_logic_vector(14 downto 0);
		cell1_bin3_out : out std_logic_vector(14 downto 0);
		cell1_bin4_out : out std_logic_vector(14 downto 0);
		cell1_bin5_out : out std_logic_vector(14 downto 0);
		cell1_bin6_out : out std_logic_vector(14 downto 0);
		cell1_bin7_out : out std_logic_vector(14 downto 0);
		
		cell2_bin0_out : out std_logic_vector(14 downto 0); 
		cell2_bin1_out : out std_logic_vector(14 downto 0);
		cell2_bin2_out : out std_logic_vector(14 downto 0);
		cell2_bin3_out : out std_logic_vector(14 downto 0);
		cell2_bin4_out : out std_logic_vector(14 downto 0);
		cell2_bin5_out : out std_logic_vector(14 downto 0);
		cell2_bin6_out : out std_logic_vector(14 downto 0);
		cell2_bin7_out : out std_logic_vector(14 downto 0);
		
		cell3_bin0_out : out std_logic_vector(14 downto 0); 
		cell3_bin1_out : out std_logic_vector(14 downto 0);
		cell3_bin2_out : out std_logic_vector(14 downto 0);
		cell3_bin3_out : out std_logic_vector(14 downto 0);
		cell3_bin4_out : out std_logic_vector(14 downto 0);
		cell3_bin5_out : out std_logic_vector(14 downto 0);
		cell3_bin6_out : out std_logic_vector(14 downto 0);
		cell3_bin7_out : out std_logic_vector(14 downto 0);
		
		valid_out : out std_logic;
		
		clk : in std_logic;
		reset : in std_logic
	);
	end component sliding_hog;
	component norm_block is
	port
	(
		valid_in : in std_logic := '0';
		en : in std_logic := '0';

		feat_in0 : in std_logic_vector(14 downto 0);
		feat_in1 : in std_logic_vector(14 downto 0);
		feat_in2 : in std_logic_vector(14 downto 0);
		feat_in3 : in std_logic_vector(14 downto 0);
		feat_in4 : in std_logic_vector(14 downto 0);
		feat_in5 : in std_logic_vector(14 downto 0);
		feat_in6 : in std_logic_vector(14 downto 0);
		feat_in7 : in std_logic_vector(14 downto 0);
		feat_in8 : in std_logic_vector(14 downto 0);
		feat_in9 : in std_logic_vector(14 downto 0);
		feat_in10 : in std_logic_vector(14 downto 0);
		feat_in11 : in std_logic_vector(14 downto 0);
		feat_in12 : in std_logic_vector(14 downto 0);
		feat_in13 : in std_logic_vector(14 downto 0);
		feat_in14 : in std_logic_vector(14 downto 0);
		feat_in15 : in std_logic_vector(14 downto 0);
		feat_in16 : in std_logic_vector(14 downto 0);
		feat_in17 : in std_logic_vector(14 downto 0);
		feat_in18 : in std_logic_vector(14 downto 0);
		feat_in19 : in std_logic_vector(14 downto 0);
		feat_in20 : in std_logic_vector(14 downto 0);
		feat_in21 : in std_logic_vector(14 downto 0);
		feat_in22 : in std_logic_vector(14 downto 0);
		feat_in23 : in std_logic_vector(14 downto 0);
		feat_in24 : in std_logic_vector(14 downto 0);
		feat_in25 : in std_logic_vector(14 downto 0);
		feat_in26 : in std_logic_vector(14 downto 0);
		feat_in27 : in std_logic_vector(14 downto 0);
		feat_in28 : in std_logic_vector(14 downto 0);
		feat_in29 : in std_logic_vector(14 downto 0);
		feat_in30 : in std_logic_vector(14 downto 0);
		feat_in31 : in std_logic_vector(14 downto 0);
		
		feat_out0 : out std_logic_vector(14 downto 0);
		feat_out1 : out std_logic_vector(14 downto 0);
		feat_out2 : out std_logic_vector(14 downto 0);
		feat_out3 : out std_logic_vector(14 downto 0);
		feat_out4 : out std_logic_vector(14 downto 0);
		feat_out5 : out std_logic_vector(14 downto 0);
		feat_out6 : out std_logic_vector(14 downto 0);
		feat_out7 : out std_logic_vector(14 downto 0);
		feat_out8 : out std_logic_vector(14 downto 0);
		feat_out9 : out std_logic_vector(14 downto 0);
		feat_out10 : out std_logic_vector(14 downto 0);
		feat_out11 : out std_logic_vector(14 downto 0);
		feat_out12 : out std_logic_vector(14 downto 0);
		feat_out13 : out std_logic_vector(14 downto 0);
		feat_out14 : out std_logic_vector(14 downto 0);
		feat_out15 : out std_logic_vector(14 downto 0);
		feat_out16 : out std_logic_vector(14 downto 0);
		feat_out17 : out std_logic_vector(14 downto 0);
		feat_out18 : out std_logic_vector(14 downto 0);
		feat_out19 : out std_logic_vector(14 downto 0);
		feat_out20 : out std_logic_vector(14 downto 0);
		feat_out21 : out std_logic_vector(14 downto 0);
		feat_out22 : out std_logic_vector(14 downto 0);
		feat_out23 : out std_logic_vector(14 downto 0);
		feat_out24 : out std_logic_vector(14 downto 0);
		feat_out25 : out std_logic_vector(14 downto 0);
		feat_out26 : out std_logic_vector(14 downto 0);
		feat_out27 : out std_logic_vector(14 downto 0);
		feat_out28 : out std_logic_vector(14 downto 0);
		feat_out29 : out std_logic_vector(14 downto 0);
		feat_out30 : out std_logic_vector(14 downto 0);
		feat_out31 : out std_logic_vector(14 downto 0);
		norm_out : out std_logic_vector(17 downto 0);
		
		valid_out : out std_logic;
		
		clk : in std_logic;
		reset : in std_logic
	);
	end component norm_block;
	
	signal hed_rst : std_logic := '0';
	signal ved_rst : std_logic := '0';
	signal mag_or_rst : std_logic := '0';
	signal shod_rst : std_logic := '0';
	signal norm_rst : std_logic := '0';
	
	signal hed_out : std_logic_vector(8 downto 0);
	signal ved_out : std_logic_vector(8 downto 0);
	signal grad_yx : std_logic_vector(31 downto 0);
	signal ed_valid : std_logic;
	signal mag_or_out : std_logic_vector(31 downto 0);
	signal mag_or_valid : std_logic;
	signal mag : std_logic_vector(8 downto 0);
	signal orient : std_logic_vector(15 downto 0);
	signal shod_valid : std_logic;
	
	
	signal block_valid : std_logic;
	
	signal feat_vec_e0 : std_logic_vector(14 downto 0);
	signal feat_vec_e1 : std_logic_vector(14 downto 0);
	signal feat_vec_e2 : std_logic_vector(14 downto 0);
	signal feat_vec_e3 : std_logic_vector(14 downto 0);
	signal feat_vec_e4 : std_logic_vector(14 downto 0);
	signal feat_vec_e5 : std_logic_vector(14 downto 0);
	signal feat_vec_e6 : std_logic_vector(14 downto 0);
	signal feat_vec_e7 : std_logic_vector(14 downto 0);
	signal feat_vec_e8 : std_logic_vector(14 downto 0);
	signal feat_vec_e9 : std_logic_vector(14 downto 0);
	signal feat_vec_e10 : std_logic_vector(14 downto 0);
	signal feat_vec_e11 : std_logic_vector(14 downto 0);
	signal feat_vec_e12 : std_logic_vector(14 downto 0);
	signal feat_vec_e13 : std_logic_vector(14 downto 0);
	signal feat_vec_e14 : std_logic_vector(14 downto 0);
	signal feat_vec_e15 : std_logic_vector(14 downto 0);
	signal feat_vec_e16 : std_logic_vector(14 downto 0);
	signal feat_vec_e17 : std_logic_vector(14 downto 0);
	signal feat_vec_e18 : std_logic_vector(14 downto 0);
	signal feat_vec_e19 : std_logic_vector(14 downto 0);
	signal feat_vec_e20 : std_logic_vector(14 downto 0);
	signal feat_vec_e21 : std_logic_vector(14 downto 0);
	signal feat_vec_e22 : std_logic_vector(14 downto 0);
	signal feat_vec_e23 : std_logic_vector(14 downto 0);
	signal feat_vec_e24 : std_logic_vector(14 downto 0);
	signal feat_vec_e25 : std_logic_vector(14 downto 0);
	signal feat_vec_e26 : std_logic_vector(14 downto 0);
	signal feat_vec_e27 : std_logic_vector(14 downto 0);
	signal feat_vec_e28 : std_logic_vector(14 downto 0);
	signal feat_vec_e29 : std_logic_vector(14 downto 0);
	signal feat_vec_e30 : std_logic_vector(14 downto 0);
	signal feat_vec_e31 : std_logic_vector(14 downto 0);
begin
	rst_distr_proc : process(clk)
	begin
		if rising_edge(clk) then
			hed_rst <= reset;
			ved_rst <= reset;
			mag_or_rst <= reset;
			shod_rst <= reset;
			norm_rst <= reset;
		end if;
	end process rst_distr_proc;
	
	hed : hor_edge_detect
	port map(
		data_in => data_in,
		data_valid => valid_in,
		en => block_vec_nxt,
		data_out => hed_out,
		data_valid_o => ed_valid,
		clk => clk,
		reset => hed_rst
	);
	ved : ver_edge_detect
	generic map(
		IMG_WIDTH => IMG_WIDTH
	)
	port map(
		data_in => data_in,
		data_valid => valid_in,
		en => block_vec_nxt,
		data_out => ved_out,
		clk => clk,
		reset => ved_rst
	);
	
	grad_yx <= (15 downto 9 => ved_out(8)) & ved_out & (15 downto 9 => hed_out(8)) & hed_out;
	mag_or : edge_mag_orient
	generic map(
		CORDIC_LENGTH => 9
	)
	port map(
		data_in => grad_yx,
		data_valid => ed_valid,
		en => block_vec_nxt,
		data_out => mag_or_out,
		data_valid_o => mag_or_valid,
		clk => clk,
		reset => mag_or_rst
	);
	
	mag <= mag_or_out(8 downto 0);
	orient <= mag_or_out(31 downto 16);
	
	shod : sliding_hog
	generic map(
		IMG_WIDTH => IMG_WIDTH,
		IMG_HEIGHT => IMG_HEIGHT
	)
	port map(
		mag_in => mag,
		orient_in => orient,
		
		valid_in => mag_or_valid,
		en => block_vec_nxt,
		
		cell0_bin0_out => feat_vec_e0,
		cell0_bin1_out => feat_vec_e1,
		cell0_bin2_out => feat_vec_e2,
		cell0_bin3_out => feat_vec_e3,
		cell0_bin4_out => feat_vec_e4,
		cell0_bin5_out => feat_vec_e5,
		cell0_bin6_out => feat_vec_e6,
		cell0_bin7_out => feat_vec_e7,
		cell1_bin0_out => feat_vec_e8,
		cell1_bin1_out => feat_vec_e9,
		cell1_bin2_out => feat_vec_e10,
		cell1_bin3_out => feat_vec_e11,
		cell1_bin4_out => feat_vec_e12,
		cell1_bin5_out => feat_vec_e13,
		cell1_bin6_out => feat_vec_e14,
		cell1_bin7_out => feat_vec_e15,
		cell2_bin0_out => feat_vec_e16,
		cell2_bin1_out => feat_vec_e17,
		cell2_bin2_out => feat_vec_e18,
		cell2_bin3_out => feat_vec_e19,
		cell2_bin4_out => feat_vec_e20,
		cell2_bin5_out => feat_vec_e21,
		cell2_bin6_out => feat_vec_e22,
		cell2_bin7_out => feat_vec_e23,
		cell3_bin0_out => feat_vec_e24,
		cell3_bin1_out => feat_vec_e25,
		cell3_bin2_out => feat_vec_e26,
		cell3_bin3_out => feat_vec_e27,
		cell3_bin4_out => feat_vec_e28,
		cell3_bin5_out => feat_vec_e29,
		cell3_bin6_out => feat_vec_e30,
		cell3_bin7_out => feat_vec_e31,
		
		valid_out => shod_valid,
		
		clk => clk,
		reset => shod_rst
	);
	
	norm : norm_block
	port map
	(
		feat_in0 => feat_vec_e0,
		feat_in1 => feat_vec_e1,
		feat_in2 => feat_vec_e2,
		feat_in3 => feat_vec_e3,
		feat_in4 => feat_vec_e4,
		feat_in5 => feat_vec_e5,
		feat_in6 => feat_vec_e6,
		feat_in7 => feat_vec_e7,
		feat_in8 => feat_vec_e8,
		feat_in9 => feat_vec_e9,
		feat_in10 => feat_vec_e10,
		feat_in11 => feat_vec_e11,
		feat_in12 => feat_vec_e12,
		feat_in13 => feat_vec_e13,
		feat_in14 => feat_vec_e14,
		feat_in15 => feat_vec_e15,
		feat_in16 => feat_vec_e16,
		feat_in17 => feat_vec_e17,
		feat_in18 => feat_vec_e18,
		feat_in19 => feat_vec_e19,
		feat_in20 => feat_vec_e20,
		feat_in21 => feat_vec_e21,
		feat_in22 => feat_vec_e22,
		feat_in23 => feat_vec_e23,
		feat_in24 => feat_vec_e24,
		feat_in25 => feat_vec_e25,
		feat_in26 => feat_vec_e26,
		feat_in27 => feat_vec_e27,
		feat_in28 => feat_vec_e28,
		feat_in29 => feat_vec_e29,
		feat_in30 => feat_vec_e30,
		feat_in31 => feat_vec_e31,
		
		valid_in => shod_valid,
		en => block_vec_nxt,
		
		feat_out0 => feat_out0,
		feat_out1 => feat_out1,
		feat_out2 => feat_out2,
		feat_out3 => feat_out3,
		feat_out4 => feat_out4,
		feat_out5 => feat_out5,
		feat_out6 => feat_out6,
		feat_out7 => feat_out7,
		feat_out8 => feat_out8,
		feat_out9 => feat_out9,
		feat_out10 => feat_out10,
		feat_out11 => feat_out11,
		feat_out12 => feat_out12,
		feat_out13 => feat_out13,
		feat_out14 => feat_out14,
		feat_out15 => feat_out15,
		feat_out16 => feat_out16,
		feat_out17 => feat_out17,
		feat_out18 => feat_out18,
		feat_out19 => feat_out19,
		feat_out20 => feat_out20,
		feat_out21 => feat_out21,
		feat_out22 => feat_out22,
		feat_out23 => feat_out23,
		feat_out24 => feat_out24,
		feat_out25 => feat_out25,
		feat_out26 => feat_out26,
		feat_out27 => feat_out27,
		feat_out28 => feat_out28,
		feat_out29 => feat_out29,
		feat_out30 => feat_out30,
		feat_out31 => feat_out31,
		norm_out => norm_out,
		valid_out => valid_out,
		
		clk => clk,
		reset => norm_rst
	);
		
end architecture rtl;