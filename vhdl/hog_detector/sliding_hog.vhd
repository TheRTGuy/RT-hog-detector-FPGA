 
library IEEE;

use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity sliding_hog is
generic
(
	IMG_WIDTH : natural := 320;
	IMG_HEIGHT : natural := 240
);
port
(
	mag_in : in std_logic_vector(8 downto 0);
	orient_in : in std_logic_vector(15 downto 0);
	
	valid_in : in std_logic;
	
	en : in std_logic;
	
	cell0_bin0_out : out std_logic_vector(14 downto 0); 
	cell0_bin1_out : out std_logic_vector(14 downto 0);
	cell0_bin2_out : out std_logic_vector(14 downto 0);
	cell0_bin3_out : out std_logic_vector(14 downto 0);
	cell0_bin4_out : out std_logic_vector(14 downto 0);
	cell0_bin5_out : out std_logic_vector(14 downto 0);
	cell0_bin6_out : out std_logic_vector(14 downto 0);
	cell0_bin7_out : out std_logic_vector(14 downto 0);
	
	cell1_bin0_out : out std_logic_vector(14 downto 0); 
	cell1_bin1_out : out std_logic_vector(14 downto 0);
	cell1_bin2_out : out std_logic_vector(14 downto 0);
	cell1_bin3_out : out std_logic_vector(14 downto 0);
	cell1_bin4_out : out std_logic_vector(14 downto 0);
	cell1_bin5_out : out std_logic_vector(14 downto 0);
	cell1_bin6_out : out std_logic_vector(14 downto 0);
	cell1_bin7_out : out std_logic_vector(14 downto 0);
	
	cell2_bin0_out : out std_logic_vector(14 downto 0); 
	cell2_bin1_out : out std_logic_vector(14 downto 0);
	cell2_bin2_out : out std_logic_vector(14 downto 0);
	cell2_bin3_out : out std_logic_vector(14 downto 0);
	cell2_bin4_out : out std_logic_vector(14 downto 0);
	cell2_bin5_out : out std_logic_vector(14 downto 0);
	cell2_bin6_out : out std_logic_vector(14 downto 0);
	cell2_bin7_out : out std_logic_vector(14 downto 0);
	
	cell3_bin0_out : out std_logic_vector(14 downto 0); 
	cell3_bin1_out : out std_logic_vector(14 downto 0);
	cell3_bin2_out : out std_logic_vector(14 downto 0);
	cell3_bin3_out : out std_logic_vector(14 downto 0);
	cell3_bin4_out : out std_logic_vector(14 downto 0);
	cell3_bin5_out : out std_logic_vector(14 downto 0);
	cell3_bin6_out : out std_logic_vector(14 downto 0);
	cell3_bin7_out : out std_logic_vector(14 downto 0);
	
	valid_out : out std_logic;
	
	clk : in std_logic;
	reset : in std_logic
);
end entity sliding_hog;

architecture rtl of sliding_hog is
	component sh_orient_quantizer is 
	port
	(
		orient_in : in std_logic_vector(15 downto 0);
		idx_out : out std_logic_vector(2 downto 0)
	);
	end component sh_orient_quantizer;
	
	type u9_arr_t is array(7 downto 0) of unsigned(8 downto 0);
	type u9_arr_arr_t is array(7 downto 0) of u9_arr_t;
	type u10_arr_t is array(7 downto 0) of unsigned(9 downto 0);
	type u11_arr_t is array(7 downto 0) of unsigned(10 downto 0);
	type u12_arr_t is array(7 downto 0) of unsigned(11 downto 0);
	
	signal fc0switch : u9_arr_arr_t;
	signal lc0switch : u9_arr_arr_t;
	signal fc1switch : u9_arr_arr_t;
	signal lc1switch : u9_arr_arr_t;
	signal fc2switch : u9_arr_arr_t;
	signal lc2switch : u9_arr_arr_t;
	signal fc3switch : u9_arr_arr_t;
	signal lc3switch : u9_arr_arr_t;
	
	signal fc0add0 : u10_arr_t;
	signal fc0add1 : u10_arr_t;
	signal fc0add2 : u10_arr_t;
	signal fc0add3 : u10_arr_t;
	signal fc0add4 : u11_arr_t;
	signal fc0add5 : u11_arr_t;
	signal fc0add6 : u12_arr_t;
	
	signal lc0add0 : u10_arr_t;
	signal lc0add1 : u10_arr_t;
	signal lc0add2 : u10_arr_t;
	signal lc0add3 : u10_arr_t;
	signal lc0add4 : u11_arr_t;
	signal lc0add5 : u11_arr_t;
	signal lc0add6 : u12_arr_t;
	
	signal fc1add0 : u10_arr_t;
	signal fc1add1 : u10_arr_t;
	signal fc1add2 : u10_arr_t;
	signal fc1add3 : u10_arr_t;
	signal fc1add4 : u11_arr_t;
	signal fc1add5 : u11_arr_t;
	signal fc1add6 : u12_arr_t;
	
	signal lc1add0 : u10_arr_t;
	signal lc1add1 : u10_arr_t;
	signal lc1add2 : u10_arr_t;
	signal lc1add3 : u10_arr_t;
	signal lc1add4 : u11_arr_t;
	signal lc1add5 : u11_arr_t;
	signal lc1add6 : u12_arr_t;
	
	signal fc2add0 : u10_arr_t;
	signal fc2add1 : u10_arr_t;
	signal fc2add2 : u10_arr_t;
	signal fc2add3 : u10_arr_t;
	signal fc2add4 : u11_arr_t;
	signal fc2add5 : u11_arr_t;
	signal fc2add6 : u12_arr_t;
	
	signal lc2add0 : u10_arr_t;
	signal lc2add1 : u10_arr_t;
	signal lc2add2 : u10_arr_t;
	signal lc2add3 : u10_arr_t;
	signal lc2add4 : u11_arr_t;
	signal lc2add5 : u11_arr_t;
	signal lc2add6 : u12_arr_t;
	
	signal fc3add0 : u10_arr_t;
	signal fc3add1 : u10_arr_t;
	signal fc3add2 : u10_arr_t;
	signal fc3add3 : u10_arr_t;
	signal fc3add4 : u11_arr_t;
	signal fc3add5 : u11_arr_t;
	signal fc3add6 : u12_arr_t;
	
	signal lc3add0 : u10_arr_t;
	signal lc3add1 : u10_arr_t;
	signal lc3add2 : u10_arr_t;
	signal lc3add3 : u10_arr_t;
	signal lc3add4 : u11_arr_t;
	signal lc3add5 : u11_arr_t;
	signal lc3add6 : u12_arr_t;
	
	type stdv8_arr_t is array(7 downto 0) of std_logic_vector(7 downto 0);
	signal fc0_en : stdv8_arr_t;
	signal lc0_en : stdv8_arr_t;
	signal fc1_en : stdv8_arr_t;
	signal lc1_en : stdv8_arr_t;
	signal fc2_en : stdv8_arr_t;
	signal lc2_en : stdv8_arr_t;
	signal fc3_en : stdv8_arr_t;
	signal lc3_en : stdv8_arr_t;
	
	type bin_arr_t is array(7 downto 0) of unsigned(14 downto 0);
	signal cell0_bins : bin_arr_t := (others => (others => '0'));
	signal cell1_bins : bin_arr_t := (others => (others => '0'));
	signal cell2_bins : bin_arr_t := (others => (others => '0'));
	signal cell3_bins : bin_arr_t := (others => (others => '0'));
	
	type mag_arr_t is array(15 downto 0) of unsigned(8 downto 0);
	type mag_arr_arr_t is array(15 downto 0) of mag_arr_t;
	signal mag_regs : mag_arr_arr_t := (others => (others => (others => '0')));
	type mag_buff_t is array(14 downto 0) of unsigned(8 downto 0);
	signal mag_buff : mag_buff_t := (others => (others => '0'));
	
	type idx_arr_t is array(15 downto 0) of std_logic_vector(2 downto 0);
	type idx_arr_arr_t is array(15 downto 0) of idx_arr_t;
	signal idx_regs : idx_arr_arr_t := (others => (others => (others => '0')));
	type idx_buff_t is array(14 downto 0) of std_logic_vector(2 downto 0);
	signal idx_buff : idx_buff_t := (others => (others => '0'));
	
	type mag_line_t is array(IMG_WIDTH-18 downto 0) of unsigned(8 downto 0);
	type idx_line_t is array(IMG_WIDTH-18 downto 0) of std_logic_vector(2 downto 0);
	type mag_line_arr_t is array(14 downto 0) of mag_line_t;
	type idx_line_arr_t is array(14 downto 0) of idx_line_t;
	
	signal mag_lines : mag_line_arr_t := (others => (others => (others => '0')));
	signal idx_lines : idx_line_arr_t := (others => (others => (others => '0')));
	signal buff_ctr : natural range 0 to IMG_WIDTH-18 := 0;
	
	signal fc0_idx : idx_arr_t;
	signal fc0 : mag_arr_t;
-- 	alias fc0_idx : idx_arr_t is idx_buff;
	alias lc0_idx : idx_arr_t is idx_regs(7);
-- 	alias fc0 : mag_arr_t is mag_buff;
	alias lc0 : mag_arr_t is mag_regs(7); 
	alias fc1_idx : idx_arr_t is idx_regs(7);
	alias lc1_idx : idx_arr_t is idx_regs(15);
	alias fc1 : mag_arr_t is mag_regs(7);
	alias lc1 : mag_arr_t is mag_regs(15); 
	
	signal orient_idx : std_logic_vector(2 downto 0);
	signal orient_idx0 : std_logic_vector(2 downto 0) := (others => '0');
	signal mag_in0 : std_logic_vector(8 downto 0) := (others => '0');
	signal valid_in0 : std_logic := '0';
	signal valid_in1 : std_logic := '0';
	signal valid_in2 : std_logic := '0';
	signal valid_in3 : std_logic := '0';
	signal block_valid : std_logic;
	signal col_ctr : natural range 0 to IMG_WIDTH-1 := 0;
	signal row_ctr : natural range 0 to IMG_HEIGHT-1 := 0;
begin
	fc0_idx(0) <= orient_idx0;
	fc0(0) <= unsigned(mag_in0);
	fc0_gen : for i in 1 to 15 generate
		fc0_idx(i) <= idx_buff(i-1);
		fc0(i) <= mag_buff(i-1);
	end generate fc0_gen;

	cell0_bin0_out <= std_logic_vector(cell0_bins(0));
	cell0_bin1_out <= std_logic_vector(cell0_bins(1));
	cell0_bin2_out <= std_logic_vector(cell0_bins(2));
	cell0_bin3_out <= std_logic_vector(cell0_bins(3));
	cell0_bin4_out <= std_logic_vector(cell0_bins(4));
	cell0_bin5_out <= std_logic_vector(cell0_bins(5));
	cell0_bin6_out <= std_logic_vector(cell0_bins(6));
	cell0_bin7_out <= std_logic_vector(cell0_bins(7));
	
	cell1_bin0_out <= std_logic_vector(cell1_bins(0));
	cell1_bin1_out <= std_logic_vector(cell1_bins(1));
	cell1_bin2_out <= std_logic_vector(cell1_bins(2));
	cell1_bin3_out <= std_logic_vector(cell1_bins(3));
	cell1_bin4_out <= std_logic_vector(cell1_bins(4));
	cell1_bin5_out <= std_logic_vector(cell1_bins(5));
	cell1_bin6_out <= std_logic_vector(cell1_bins(6));
	cell1_bin7_out <= std_logic_vector(cell1_bins(7));
	
	cell2_bin0_out <= std_logic_vector(cell2_bins(0));
	cell2_bin1_out <= std_logic_vector(cell2_bins(1));
	cell2_bin2_out <= std_logic_vector(cell2_bins(2));
	cell2_bin3_out <= std_logic_vector(cell2_bins(3));
	cell2_bin4_out <= std_logic_vector(cell2_bins(4));
	cell2_bin5_out <= std_logic_vector(cell2_bins(5));
	cell2_bin6_out <= std_logic_vector(cell2_bins(6));
	cell2_bin7_out <= std_logic_vector(cell2_bins(7));
	
	cell3_bin0_out <= std_logic_vector(cell3_bins(0)); 
	cell3_bin1_out <= std_logic_vector(cell3_bins(1));
	cell3_bin2_out <= std_logic_vector(cell3_bins(2));
	cell3_bin3_out <= std_logic_vector(cell3_bins(3));
	cell3_bin4_out <= std_logic_vector(cell3_bins(4));
	cell3_bin5_out <= std_logic_vector(cell3_bins(5));
	cell3_bin6_out <= std_logic_vector(cell3_bins(6));
	cell3_bin7_out <= std_logic_vector(cell3_bins(7));
	
	-- Here we generate decoding signals for the adder tree input switches
	decode_gen : for i in 0 to 7 generate
		fc0_decode : process(fc0_idx(i))
		begin
			fc0_en(i) <= (others => '0');
			fc0_en(i)(to_integer(unsigned(fc0_idx(i)))) <= '1';
		end process fc0_decode;
		fc1_decode : process(fc1_idx(i))
		begin
			fc1_en(i) <= (others => '0');
			fc1_en(i)(to_integer(unsigned(fc1_idx(i)))) <= '1';
		end process fc1_decode;
		fc2_decode : process(fc0_idx(i+8))
		begin
			fc2_en(i) <= (others => '0');
			fc2_en(i)(to_integer(unsigned(fc0_idx(i+8)))) <= '1';
		end process fc2_decode;
		fc3_decode : process(fc1_idx(i+8))
		begin
			fc3_en(i) <= (others => '0');
			fc3_en(i)(to_integer(unsigned(fc1_idx(i+8)))) <= '1';
		end process fc3_decode;
		
		lc0_decode : process(lc0_idx(i))
		begin
			lc0_en(i) <= (others => '0');
			lc0_en(i)(to_integer(unsigned(lc0_idx(i)))) <= '1';
		end process lc0_decode;
		lc1_decode : process(lc1_idx(i))
		begin
			lc1_en(i) <= (others => '0');
			lc1_en(i)(to_integer(unsigned(lc1_idx(i)))) <= '1';
		end process lc1_decode;
		lc2_decode : process(lc0_idx(i+8))
		begin
			lc2_en(i) <= (others => '0');
			lc2_en(i)(to_integer(unsigned(lc0_idx(i+8)))) <= '1';
		end process lc2_decode;
		lc3_decode : process(lc1_idx(i+8))
		begin
			lc3_en(i) <= (others => '0');
			lc3_en(i)(to_integer(unsigned(lc1_idx(i+8)))) <= '1';
		end process lc3_decode;
	end generate decode_gen;
	
	-- For each bin accumulator
	accum_gen : for i in 0 to 7 generate
		-- These switches provide a 0 or a value from the corresponding line in the kernel to the adder trees, depending on the orientation index
		switch_gen : for j in 0 to 7 generate
			fc0switch(j)(i) <= (others => '0') when (fc0_en(j)(i) = '0') else fc0(j);
			lc0switch(j)(i) <= (others => '0') when lc0_en(j)(i) = '0' else lc0(j);
			fc1switch(j)(i) <= (others => '0') when fc1_en(j)(i) = '0' else fc1(j);
			lc1switch(j)(i) <= (others => '0') when lc1_en(j)(i) = '0' else lc1(j);
			fc2switch(j)(i) <= (others => '0') when fc2_en(j)(i) = '0' else fc0(j+8);
			lc2switch(j)(i) <= (others => '0') when lc2_en(j)(i) = '0' else lc0(j+8);
			fc3switch(j)(i) <= (others => '0') when fc3_en(j)(i) = '0' else fc1(j+8);
			lc3switch(j)(i) <= (others => '0') when lc3_en(j)(i) = '0' else lc1(j+8);
		end generate switch_gen;
	
		-- Here we have an adder tree for each bin in a cell and for each cell in a block
		tree_proc : process(clk)
		begin
			if rising_edge(clk) then
				if en = '1' then
					fc0add0(i) <= ('0'&fc0switch(0)(i)) + ('0'&fc0switch(1)(i));
					fc0add1(i) <= ('0'&fc0switch(2)(i)) + ('0'&fc0switch(3)(i));
					fc0add2(i) <= ('0'&fc0switch(4)(i)) + ('0'&fc0switch(5)(i));
					fc0add3(i) <= ('0'&fc0switch(6)(i)) + ('0'&fc0switch(7)(i));
					fc0add4(i) <= ('0'&fc0add0(i)) + ('0'&fc0add1(i));
					fc0add5(i) <= ('0'&fc0add2(i)) + ('0'&fc0add3(i));
					fc0add6(i) <= ('0'&fc0add4(i)) + ('0'&fc0add5(i));
					
					lc0add0(i) <= ('0'&lc0switch(0)(i)) + ('0'&lc0switch(1)(i));
					lc0add1(i) <= ('0'&lc0switch(2)(i)) + ('0'&lc0switch(3)(i));
					lc0add2(i) <= ('0'&lc0switch(4)(i)) + ('0'&lc0switch(5)(i));
					lc0add3(i) <= ('0'&lc0switch(6)(i)) + ('0'&lc0switch(7)(i));
					lc0add4(i) <= ('0'&lc0add0(i)) + ('0'&lc0add1(i));
					lc0add5(i) <= ('0'&lc0add2(i)) + ('0'&lc0add3(i));
					lc0add6(i) <= ('0'&lc0add4(i)) + ('0'&lc0add5(i));
					
					fc1add0(i) <= ('0'&fc1switch(0)(i)) + ('0'&fc1switch(1)(i));
					fc1add1(i) <= ('0'&fc1switch(2)(i)) + ('0'&fc1switch(3)(i));
					fc1add2(i) <= ('0'&fc1switch(4)(i)) + ('0'&fc1switch(5)(i));
					fc1add3(i) <= ('0'&fc1switch(6)(i)) + ('0'&fc1switch(7)(i));
					fc1add4(i) <= ('0'&fc1add0(i)) + ('0'&fc1add1(i));
					fc1add5(i) <= ('0'&fc1add2(i)) + ('0'&fc1add3(i));
					fc1add6(i) <= ('0'&fc1add4(i)) + ('0'&fc1add5(i));
					
					lc1add0(i) <= ('0'&lc1switch(0)(i)) + ('0'&lc1switch(1)(i));
					lc1add1(i) <= ('0'&lc1switch(2)(i)) + ('0'&lc1switch(3)(i));
					lc1add2(i) <= ('0'&lc1switch(4)(i)) + ('0'&lc1switch(5)(i));
					lc1add3(i) <= ('0'&lc1switch(6)(i)) + ('0'&lc1switch(7)(i));
					lc1add4(i) <= ('0'&lc1add0(i)) + ('0'&lc1add1(i));
					lc1add5(i) <= ('0'&lc1add2(i)) + ('0'&lc1add3(i));
					lc1add6(i) <= ('0'&lc1add4(i)) + ('0'&lc1add5(i));
					
					fc2add0(i) <= ('0'&fc2switch(0)(i)) + ('0'&fc2switch(1)(i));
					fc2add1(i) <= ('0'&fc2switch(2)(i)) + ('0'&fc2switch(3)(i));
					fc2add2(i) <= ('0'&fc2switch(4)(i)) + ('0'&fc2switch(5)(i));
					fc2add3(i) <= ('0'&fc2switch(6)(i)) + ('0'&fc2switch(7)(i));
					fc2add4(i) <= ('0'&fc2add0(i)) + ('0'&fc2add1(i));
					fc2add5(i) <= ('0'&fc2add2(i)) + ('0'&fc2add3(i));
					fc2add6(i) <= ('0'&fc2add4(i)) + ('0'&fc2add5(i));
					
					lc2add0(i) <= ('0'&lc2switch(0)(i)) + ('0'&lc2switch(1)(i));
					lc2add1(i) <= ('0'&lc2switch(2)(i)) + ('0'&lc2switch(3)(i));
					lc2add2(i) <= ('0'&lc2switch(4)(i)) + ('0'&lc2switch(5)(i));
					lc2add3(i) <= ('0'&lc2switch(6)(i)) + ('0'&lc2switch(7)(i));
					lc2add4(i) <= ('0'&lc2add0(i)) + ('0'&lc2add1(i));
					lc2add5(i) <= ('0'&lc2add2(i)) + ('0'&lc2add3(i));
					lc2add6(i) <= ('0'&lc2add4(i)) + ('0'&lc2add5(i));
					
					fc3add0(i) <= ('0'&fc3switch(0)(i)) + ('0'&fc3switch(1)(i));
					fc3add1(i) <= ('0'&fc3switch(2)(i)) + ('0'&fc3switch(3)(i));
					fc3add2(i) <= ('0'&fc3switch(4)(i)) + ('0'&fc3switch(5)(i));
					fc3add3(i) <= ('0'&fc3switch(6)(i)) + ('0'&fc3switch(7)(i));
					fc3add4(i) <= ('0'&fc3add0(i)) + ('0'&fc3add1(i));
					fc3add5(i) <= ('0'&fc3add2(i)) + ('0'&fc3add3(i));
					fc3add6(i) <= ('0'&fc3add4(i)) + ('0'&fc3add5(i));
					
					lc3add0(i) <= ('0'&lc3switch(0)(i)) + ('0'&lc3switch(1)(i));
					lc3add1(i) <= ('0'&lc3switch(2)(i)) + ('0'&lc3switch(3)(i));
					lc3add2(i) <= ('0'&lc3switch(4)(i)) + ('0'&lc3switch(5)(i));
					lc3add3(i) <= ('0'&lc3switch(6)(i)) + ('0'&lc3switch(7)(i));
					lc3add4(i) <= ('0'&lc3add0(i)) + ('0'&lc3add1(i));
					lc3add5(i) <= ('0'&lc3add2(i)) + ('0'&lc3add3(i));
					lc3add6(i) <= ('0'&lc3add4(i)) + ('0'&lc3add5(i));
				end if;
			end if;
		end process tree_proc;
		accum_proc : process(clk)
		begin
			if rising_edge(clk) then
				if reset = '1' then
					cell0_bins(i) <= (others => '0');
					cell1_bins(i) <= (others => '0');
					cell2_bins(i) <= (others => '0');
					cell3_bins(i) <= (others => '0');
				elsif en = '1' then
					if valid_in3 = '1' then
						-- Add the sum of values from the first column of each cell to the corresponding bin and subtract the sum of values from the last column
						cell0_bins(i) <= (cell0_bins(i) + ("000" & fc0add6(i))) - ("000" & lc0add6(i));
						cell1_bins(i) <= (cell1_bins(i) + ("000" & fc1add6(i))) - ("000" & lc1add6(i));
						cell2_bins(i) <= (cell2_bins(i) + ("000" & fc2add6(i))) - ("000" & lc2add6(i));
						cell3_bins(i) <= (cell3_bins(i) + ("000" & fc3add6(i))) - ("000" & lc3add6(i));
					end if;
				end if;
			end if;
		end process accum_proc;
	end generate accum_gen;


	block_valid <= '1' when (row_ctr >= 15 and col_ctr >= 15) else '0';
	reg_proc : process(clk)
	begin
		if rising_edge(clk) then
			if reset = '1' then
				valid_out <= '0';
				valid_in0 <= '0';
				valid_in1 <= '0';
				valid_in2 <= '0';
				valid_in3 <= '0';
				row_ctr <= 0;
				col_ctr <= 0;
			elsif en = '1' then
				valid_in0 <= valid_in;
				valid_in1 <= valid_in0;
				valid_in2 <= valid_in1;
				valid_in3 <= valid_in2;
				valid_out <= valid_in3 and block_valid;
				if valid_in3 = '1' then
					if col_ctr = IMG_WIDTH-1 then
						col_ctr <= 0;
						if row_ctr = IMG_HEIGHT-1 then
							row_ctr <= 0;
						else
							row_ctr <= row_ctr + 1;
						end if;
					else
						col_ctr <= col_ctr + 1;
					end if;
				end if;
			end if;
			
			if en = '1' then
				mag_in0 <= mag_in;
				orient_idx0 <= orient_idx;
				-- pipe valid
				if valid_in0 = '1' then
					if buff_ctr = IMG_WIDTH-18 then
						buff_ctr <= 0;
					else
						buff_ctr <= buff_ctr + 1;
					end if;
					
					for i in 0 to 15 loop
						mag_regs(0)(i) <= fc0(i);
						idx_regs(0)(i) <= fc0_idx(i);
					end loop;
					
					-- For each line and column (except the first column), shift the values from earlier columns in the kernel
					for j in 0 to 15 loop
						for i in 0 to 14 loop
							idx_regs(i+1)(j) <= idx_regs(i)(j);
							mag_regs(i+1)(j) <= mag_regs(i)(j);
						end loop;
					end loop;
				end if;
			end if;
		end if;
	end process reg_proc;
	
	line_buff_mem_gen : for i in 0 to 14 generate
		mem_proc : process(clk)
		begin
			if rising_edge(clk) then
				if valid_in0 = '1' and en = '1' then
					-- For each line buffer below the first, put the value from the last column
					mag_lines(i)(buff_ctr) <= lc1(i);
					idx_lines(i)(buff_ctr) <= lc1_idx(i);
					
					mag_buff(i) <= mag_lines(i)(buff_ctr);
					idx_buff(i) <= idx_lines(i)(buff_ctr);
				end if;
			end if;
		end process mem_proc;
	end generate line_buff_mem_gen;
	
	shoq : sh_orient_quantizer
	port map
	(
		orient_in => orient_in,
		idx_out => orient_idx
	);
end architecture rtl;