library IEEE;

use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity hor_edge_detect is
generic(
	DATA_IN_WIDTH : natural := 8
	);
port( 
	data_in : in std_logic_vector(DATA_IN_WIDTH-1 downto 0);
	data_valid : in std_logic;
	
	en : in std_logic;
	
	data_out : out std_logic_vector(DATA_IN_WIDTH downto 0);
	data_valid_o : out std_logic;
	
	clk			:	in    	std_logic := '0';
	reset			:	in    	std_logic := '0'
);
end entity hor_edge_detect;

architecture rtl of hor_edge_detect is
	type reg_t is array(1 downto 0) of signed(DATA_IN_WIDTH-1 downto 0);
	signal dout : signed(DATA_IN_WIDTH downto 0);
	signal reg : reg_t := ((others => '0'),(others => '0'));
	signal data_o : signed(DATA_IN_WIDTH downto 0) := (others => '0');
	
	alias reg1 : signed(DATA_IN_WIDTH-1 downto 0) is reg(1);
begin
	dout <= signed('0'&data_in) - ('0'&reg1);
	data_out <= std_logic_vector(data_o);
	seq : process(clk)
	begin
		if rising_edge(clk) then
			if reset = '1' then
				data_valid_o <= '0';
			elsif en = '1' then
				data_valid_o <= data_valid;
			end if;
			if en = '1' then
				data_o <= dout;
				if data_valid = '1' then
					reg(1) <= reg(0);
					reg(0) <= signed(data_in);
				end if;
			end if;
		end if;
	end process seq;
end architecture rtl;