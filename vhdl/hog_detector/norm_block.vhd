 
library IEEE;

use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity norm_block is
port
(	
	valid_in : in std_logic := '0';
	en : in std_logic := '0';

	feat_in0 : in std_logic_vector(14 downto 0);
	feat_in1 : in std_logic_vector(14 downto 0);
	feat_in2 : in std_logic_vector(14 downto 0);
	feat_in3 : in std_logic_vector(14 downto 0);
	feat_in4 : in std_logic_vector(14 downto 0);
	feat_in5 : in std_logic_vector(14 downto 0);
	feat_in6 : in std_logic_vector(14 downto 0);
	feat_in7 : in std_logic_vector(14 downto 0);
	feat_in8 : in std_logic_vector(14 downto 0);
	feat_in9 : in std_logic_vector(14 downto 0);
	feat_in10 : in std_logic_vector(14 downto 0);
	feat_in11 : in std_logic_vector(14 downto 0);
	feat_in12 : in std_logic_vector(14 downto 0);
	feat_in13 : in std_logic_vector(14 downto 0);
	feat_in14 : in std_logic_vector(14 downto 0);
	feat_in15 : in std_logic_vector(14 downto 0);
	feat_in16 : in std_logic_vector(14 downto 0);
	feat_in17 : in std_logic_vector(14 downto 0);
	feat_in18 : in std_logic_vector(14 downto 0);
	feat_in19 : in std_logic_vector(14 downto 0);
	feat_in20 : in std_logic_vector(14 downto 0);
	feat_in21 : in std_logic_vector(14 downto 0);
	feat_in22 : in std_logic_vector(14 downto 0);
	feat_in23 : in std_logic_vector(14 downto 0);
	feat_in24 : in std_logic_vector(14 downto 0);
	feat_in25 : in std_logic_vector(14 downto 0);
	feat_in26 : in std_logic_vector(14 downto 0);
	feat_in27 : in std_logic_vector(14 downto 0);
	feat_in28 : in std_logic_vector(14 downto 0);
	feat_in29 : in std_logic_vector(14 downto 0);
	feat_in30 : in std_logic_vector(14 downto 0);
	feat_in31 : in std_logic_vector(14 downto 0);
	
	feat_out0 : out std_logic_vector(14 downto 0);
	feat_out1 : out std_logic_vector(14 downto 0);
	feat_out2 : out std_logic_vector(14 downto 0);
	feat_out3 : out std_logic_vector(14 downto 0);
	feat_out4 : out std_logic_vector(14 downto 0);
	feat_out5 : out std_logic_vector(14 downto 0);
	feat_out6 : out std_logic_vector(14 downto 0);
	feat_out7 : out std_logic_vector(14 downto 0);
	feat_out8 : out std_logic_vector(14 downto 0);
	feat_out9 : out std_logic_vector(14 downto 0);
	feat_out10 : out std_logic_vector(14 downto 0);
	feat_out11 : out std_logic_vector(14 downto 0);
	feat_out12 : out std_logic_vector(14 downto 0);
	feat_out13 : out std_logic_vector(14 downto 0);
	feat_out14 : out std_logic_vector(14 downto 0);
	feat_out15 : out std_logic_vector(14 downto 0);
	feat_out16 : out std_logic_vector(14 downto 0);
	feat_out17 : out std_logic_vector(14 downto 0);
	feat_out18 : out std_logic_vector(14 downto 0);
	feat_out19 : out std_logic_vector(14 downto 0);
	feat_out20 : out std_logic_vector(14 downto 0);
	feat_out21 : out std_logic_vector(14 downto 0);
	feat_out22 : out std_logic_vector(14 downto 0);
	feat_out23 : out std_logic_vector(14 downto 0);
	feat_out24 : out std_logic_vector(14 downto 0);
	feat_out25 : out std_logic_vector(14 downto 0);
	feat_out26 : out std_logic_vector(14 downto 0);
	feat_out27 : out std_logic_vector(14 downto 0);
	feat_out28 : out std_logic_vector(14 downto 0);
	feat_out29 : out std_logic_vector(14 downto 0);
	feat_out30 : out std_logic_vector(14 downto 0);
	feat_out31 : out std_logic_vector(14 downto 0);
	norm_out : out std_logic_vector(17 downto 0);
	
	valid_out : out std_logic;
	
	clk : in std_logic;
	reset : in std_logic
);
end entity norm_block;

architecture rtl of norm_block is
	component lin_interp_div is
	port (
		val_in : in std_logic_vector(16 downto 0);
		val_out : out std_logic_vector(17 downto 0);
		
		valid_in : in std_logic;
		valid_out : out std_logic;
		
		en : in std_logic := '0';
		clk : in std_logic := '0';
		reset : in std_logic := '0'
	);
	end component lin_interp_div;
	
	signal add0 : unsigned(16 downto 0);
	signal add1 : unsigned(16 downto 0);
	signal add2 : unsigned(16 downto 0);
	signal add3 : unsigned(16 downto 0);
	signal add4 : unsigned(16 downto 0);
	signal add5 : unsigned(16 downto 0);
	signal add6 : unsigned(16 downto 0);
	signal add7 : unsigned(16 downto 0);
	signal add8 : unsigned(16 downto 0);
	signal add9 : unsigned(16 downto 0);
	signal add10 : unsigned(16 downto 0);
	signal add11 : unsigned(16 downto 0);
	signal add12 : unsigned(16 downto 0);
	signal add13 : unsigned(16 downto 0);
	signal add14 : unsigned(16 downto 0);
	signal add15 : unsigned(16 downto 0);
	signal add16 : unsigned(16 downto 0);
	signal add17 : unsigned(16 downto 0);
	signal add18 : unsigned(16 downto 0);
	signal add19 : unsigned(16 downto 0);
	signal add20 : unsigned(16 downto 0);
	signal add21 : unsigned(16 downto 0);
	signal add22 : unsigned(16 downto 0);
	signal add23 : unsigned(16 downto 0);
	signal add24 : unsigned(16 downto 0);
	signal add25 : unsigned(16 downto 0);
	signal add26 : unsigned(16 downto 0);
	signal add27 : unsigned(16 downto 0);
	signal add28 : unsigned(16 downto 0);
	signal add29 : unsigned(16 downto 0);
	signal add30 : unsigned(16 downto 0);
	
	signal addbuff : unsigned(16 downto 0);
	signal sum_valid : std_logic := '0';
	signal valid_o : std_logic := '0';
	signal normo : std_logic_vector(17 downto 0);
	signal norm_o : std_logic_vector(17 downto 0) := (others => '0');
	
	type buff_t is array(31 downto 0) of unsigned(14 downto 0);
	
	signal buff0 : buff_t := (others => (others => '0'));
	signal buff1 : buff_t := (others => (others => '0'));
	signal buff2 : buff_t := (others => (others => '0'));
	signal buff3 : buff_t := (others => (others => '0'));
begin	
	add0 <= unsigned("00" & feat_in0) + unsigned("00" & feat_in1);
	add1 <= unsigned("00" & feat_in2) + unsigned("00" & feat_in3);
	add2 <= unsigned("00" & feat_in4) + unsigned("00" & feat_in5);
	add3 <= unsigned("00" & feat_in6) + unsigned("00" & feat_in7);
	add4 <= unsigned("00" & feat_in8) + unsigned("00" & feat_in9);
	add5 <= unsigned("00" & feat_in10) + unsigned("00" & feat_in11);
	add6 <= unsigned("00" & feat_in12) + unsigned("00" & feat_in13);
	add7 <= unsigned("00" & feat_in14) + unsigned("00" & feat_in15);
	add8 <= unsigned("00" & feat_in16) + unsigned("00" & feat_in17);
	add9 <= unsigned("00" & feat_in18) + unsigned("00" & feat_in19);
	add10 <= unsigned("00" & feat_in20) + unsigned("00" & feat_in21);
	add11 <= unsigned("00" & feat_in22) + unsigned("00" & feat_in23);
	add12 <= unsigned("00" & feat_in24) + unsigned("00" & feat_in25);
	add13 <= unsigned("00" & feat_in26) + unsigned("00" & feat_in27);
	add14 <= unsigned("00" & feat_in28) + unsigned("00" & feat_in29);
	add15 <= unsigned("00" & feat_in30) + unsigned("00" & feat_in31);
	
	add16 <= add0 + add1;
	add17 <= add2 + add3;
	add18 <= add4 + add5;
	add19 <= add6 + add7;
	add20 <= add8 + add9;
	add21 <= add10 + add11;
	add22 <= add12 + add13;
	add23 <= add14 + add15;
	
	add24 <= add16 + add17;
	add25 <= add18 + add19;
	add26 <= add20 + add21;
	add27 <= add22 + add23;
	
	add28 <= add24 + add25;
	add29 <= add26 + add27;
	
	add30 <= add28 + add29;
	
	norm : lin_interp_div
	port map
	(
	      valid_in => sum_valid,
	      val_in => std_logic_vector(addbuff),
	      valid_out => valid_out,
	      val_out => norm_out,
	      en => en,
	      clk => clk,
	      reset => reset
	);
	
	buff_proc : process(clk)
	begin
		if rising_edge(clk) then
			if reset = '1' then
				sum_valid <= '0';
			elsif en = '1' then
				sum_valid <= valid_in;
			end if;
			
			if en = '1' then
				buff0(0) <= unsigned(feat_in0);
				buff0(1) <= unsigned(feat_in1);
				buff0(2) <= unsigned(feat_in2);
				buff0(3) <= unsigned(feat_in3);
				buff0(4) <= unsigned(feat_in4);
				buff0(5) <= unsigned(feat_in5);
				buff0(6) <= unsigned(feat_in6);
				buff0(7) <= unsigned(feat_in7);
				buff0(8) <= unsigned(feat_in8);
				buff0(9) <= unsigned(feat_in9);
				buff0(10) <= unsigned(feat_in10);
				buff0(11) <= unsigned(feat_in11);
				buff0(12) <= unsigned(feat_in12);
				buff0(13) <= unsigned(feat_in13);
				buff0(14) <= unsigned(feat_in14);
				buff0(15) <= unsigned(feat_in15);
				buff0(16) <= unsigned(feat_in16);
				buff0(17) <= unsigned(feat_in17);
				buff0(18) <= unsigned(feat_in18);
				buff0(19) <= unsigned(feat_in19);
				buff0(20) <= unsigned(feat_in20);
				buff0(21) <= unsigned(feat_in21);
				buff0(22) <= unsigned(feat_in22);
				buff0(23) <= unsigned(feat_in23);
				buff0(24) <= unsigned(feat_in24);
				buff0(25) <= unsigned(feat_in25);
				buff0(26) <= unsigned(feat_in26);
				buff0(27) <= unsigned(feat_in27);
				buff0(28) <= unsigned(feat_in28);
				buff0(29) <= unsigned(feat_in29);
				buff0(30) <= unsigned(feat_in30);
				buff0(31) <= unsigned(feat_in31);
				
				buff1 <= buff0;
				buff2 <= buff1;
				buff3 <= buff2;
				
				addbuff <= add30;
			end if;
		end if;
	end process buff_proc;
	
	feat_out0 <= std_logic_vector(buff3(0));
	feat_out1 <= std_logic_vector(buff3(1));
	feat_out2 <= std_logic_vector(buff3(2));
	feat_out3 <= std_logic_vector(buff3(3));
	feat_out4 <= std_logic_vector(buff3(4));
	feat_out5 <= std_logic_vector(buff3(5));
	feat_out6 <= std_logic_vector(buff3(6));
	feat_out7 <= std_logic_vector(buff3(7));
	feat_out8 <= std_logic_vector(buff3(8));
	feat_out9 <= std_logic_vector(buff3(9));
	feat_out10 <= std_logic_vector(buff3(10));
	feat_out11 <= std_logic_vector(buff3(11));
	feat_out12 <= std_logic_vector(buff3(12));
	feat_out13 <= std_logic_vector(buff3(13));
	feat_out14 <= std_logic_vector(buff3(14));
	feat_out15 <= std_logic_vector(buff3(15));
	feat_out16 <= std_logic_vector(buff3(16));
	feat_out17 <= std_logic_vector(buff3(17));
	feat_out18 <= std_logic_vector(buff3(18));
	feat_out19 <= std_logic_vector(buff3(19));
	feat_out20 <= std_logic_vector(buff3(20));
	feat_out21 <= std_logic_vector(buff3(21));
	feat_out22 <= std_logic_vector(buff3(22));
	feat_out23 <= std_logic_vector(buff3(23));
	feat_out24 <= std_logic_vector(buff3(24));
	feat_out25 <= std_logic_vector(buff3(25));
	feat_out26 <= std_logic_vector(buff3(26));
	feat_out27 <= std_logic_vector(buff3(27));
	feat_out28 <= std_logic_vector(buff3(28));
	feat_out29 <= std_logic_vector(buff3(29));
	feat_out30 <= std_logic_vector(buff3(30));
	feat_out31 <= std_logic_vector(buff3(31));
end architecture rtl;