library IEEE;

use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity ver_edge_detect is
generic
(
	DATA_IN_WIDTH : natural := 8;
	IMG_WIDTH : natural := 320
);
port( 
	data_in : in std_logic_vector(DATA_IN_WIDTH-1 downto 0);
	data_valid : in std_logic;
	
	en : in std_logic;
	
	data_out : out std_logic_vector(DATA_IN_WIDTH downto 0);
	data_valid_o : out std_logic;
	
	clk			:	in    	std_logic := '0';
	reset			:	in    	std_logic := '0'
);
end entity ver_edge_detect;

architecture rtl of ver_edge_detect is
	type reg_t is array(IMG_WIDTH*2 - 1 downto 0) of signed(DATA_IN_WIDTH-1 downto 0);
	signal addr : natural range 0 to IMG_WIDTH*2 - 1 := 0;
	
	signal dout : signed(DATA_IN_WIDTH downto 0);
	signal reg : reg_t := (others => (others => '0'));
	signal data_o : signed(DATA_IN_WIDTH downto 0) := (others => '0');
	
	signal reg1 : signed(DATA_IN_WIDTH-1 downto 0);
begin
	reg1 <= reg(addr);
	dout <= signed('0'&data_in) - ('0'&reg1);
	data_out <= std_logic_vector(data_o);
	seq : process(clk)
	begin
		if rising_edge(clk) then
			if reset = '1' then
				data_valid_o <= '0';
			elsif en = '1' then
				data_valid_o <= data_valid;
			end if;
			
			if en = '1' then
				data_o <= dout;
				if data_valid = '1' then
					if addr = IMG_WIDTH*2 - 1 then
						addr <= 0;
					else
						addr <= addr + 1;
					end if;
				end if;
			end if;
		end if;
	end process seq;
	
	ram_proc : process(clk)
	begin
		if rising_edge(clk) then
			if data_valid = '1' and en = '1' then
				reg(addr) <= signed(data_in);
			end if;
		end if;
	end process ram_proc;
end architecture rtl;