library IEEE;

use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity hog_detector is
generic(
	USE_XIL_BRAM : boolean := FALSE;
	USE_INFERRED_BRAM : boolean := FALSE;
	IMG_WIDTH_PIXELS : natural := 320;
	IMG_HEIGHT_PIXELS : natural := 240;
	WIND_WIDTH_PIXELS : natural := 64;
	WIND_HEIGHT_PIXELS : natural := 128;
	DP_AMOUNT : natural := 2
);
port(
	data_in : in std_logic_vector(7 downto 0) := (others => '0');
	valid_in : in std_logic := '0';
	
	detect_out : out std_logic;
	valid_out : out std_logic;
	nxt_sample : out std_logic;
	
	reg_data_in : in std_logic_vector(31 downto 0) := (others => '0');
	reg_data_out : out std_logic_vector(31 downto 0);
	reg_addr : in std_logic_vector(1 downto 0) := (others => '0');
	reg_we : in std_logic := '0';
	reg_re : in std_logic := '0';
	
	pline_clk : in std_logic := '0';
	pline_rst : in std_logic := '0';
	
	clk : in std_logic := '0';
	reset : in std_logic := '0'
);
end entity hog_detector;

architecture arch of hog_detector is
	component block_extract_pline is
	generic
	(
		IMG_WIDTH : natural := 320;
		IMG_HEIGHT : natural := 240
	);
	port
	(
		data_in : in std_logic_vector(7 downto 0);
		valid_in : in std_logic;
		
		block_vec_nxt : in std_logic;
		
		feat_out0 : out std_logic_vector(14 downto 0);
		feat_out1 : out std_logic_vector(14 downto 0);
		feat_out2 : out std_logic_vector(14 downto 0);
		feat_out3 : out std_logic_vector(14 downto 0);
		feat_out4 : out std_logic_vector(14 downto 0);
		feat_out5 : out std_logic_vector(14 downto 0);
		feat_out6 : out std_logic_vector(14 downto 0);
		feat_out7 : out std_logic_vector(14 downto 0);
		feat_out8 : out std_logic_vector(14 downto 0);
		feat_out9 : out std_logic_vector(14 downto 0);
		feat_out10 : out std_logic_vector(14 downto 0);
		feat_out11 : out std_logic_vector(14 downto 0);
		feat_out12 : out std_logic_vector(14 downto 0);
		feat_out13 : out std_logic_vector(14 downto 0);
		feat_out14 : out std_logic_vector(14 downto 0);
		feat_out15 : out std_logic_vector(14 downto 0);
		feat_out16 : out std_logic_vector(14 downto 0);
		feat_out17 : out std_logic_vector(14 downto 0);
		feat_out18 : out std_logic_vector(14 downto 0);
		feat_out19 : out std_logic_vector(14 downto 0);
		feat_out20 : out std_logic_vector(14 downto 0);
		feat_out21 : out std_logic_vector(14 downto 0);
		feat_out22 : out std_logic_vector(14 downto 0);
		feat_out23 : out std_logic_vector(14 downto 0);
		feat_out24 : out std_logic_vector(14 downto 0);
		feat_out25 : out std_logic_vector(14 downto 0);
		feat_out26 : out std_logic_vector(14 downto 0);
		feat_out27 : out std_logic_vector(14 downto 0);
		feat_out28 : out std_logic_vector(14 downto 0);
		feat_out29 : out std_logic_vector(14 downto 0);
		feat_out30 : out std_logic_vector(14 downto 0);
		feat_out31 : out std_logic_vector(14 downto 0);
		norm_out : out std_logic_vector(17 downto 0);
		
		valid_out : out std_logic;
	
		
		clk : in std_logic := '0';
		reset : in std_logic := '0'
	);
	end component block_extract_pline;
	
	component svm_accumulator is
	generic(
		DP_AMOUNT_WIDTH : natural := 2;
		USE_XIL_BRAM : boolean := FALSE;
		USE_INFERRED_BRAM : boolean := FALSE;
		IMG_WIDTH_PIXELS : natural := 320;
		IMG_HEIGHT_PIXELS : natural := 240;
		WIND_WIDTH_PIXELS : natural := 64;
		WIND_HEIGHT_PIXELS : natural := 128
	);
	port(
		reg_data_in : in std_logic_vector(31 downto 0);
		reg_data_out : out std_logic_vector(31 downto 0);
		reg_addr : in std_logic_vector(1 downto 0);
		reg_we : in std_logic;
		reg_re : in std_logic;
		
		block_vec_c0 : in std_logic_vector(14 downto 0);
		block_vec_c1 : in std_logic_vector(14 downto 0);
		block_vec_c2 : in std_logic_vector(14 downto 0);
		block_vec_c3 : in std_logic_vector(14 downto 0);
		block_vec_c4 : in std_logic_vector(14 downto 0);
		block_vec_c5 : in std_logic_vector(14 downto 0);
		block_vec_c6 : in std_logic_vector(14 downto 0);
		block_vec_c7 : in std_logic_vector(14 downto 0);
		block_vec_c8 : in std_logic_vector(14 downto 0);
		block_vec_c9 : in std_logic_vector(14 downto 0);
		block_vec_c10 : in std_logic_vector(14 downto 0);
		block_vec_c11 : in std_logic_vector(14 downto 0);
		block_vec_c12 : in std_logic_vector(14 downto 0);
		block_vec_c13 : in std_logic_vector(14 downto 0);
		block_vec_c14 : in std_logic_vector(14 downto 0);
		block_vec_c15 : in std_logic_vector(14 downto 0);
		block_vec_c16 : in std_logic_vector(14 downto 0);
		block_vec_c17 : in std_logic_vector(14 downto 0);
		block_vec_c18 : in std_logic_vector(14 downto 0);
		block_vec_c19 : in std_logic_vector(14 downto 0);
		block_vec_c20 : in std_logic_vector(14 downto 0);
		block_vec_c21 : in std_logic_vector(14 downto 0);
		block_vec_c22 : in std_logic_vector(14 downto 0);
		block_vec_c23 : in std_logic_vector(14 downto 0);
		block_vec_c24 : in std_logic_vector(14 downto 0);
		block_vec_c25 : in std_logic_vector(14 downto 0);
		block_vec_c26 : in std_logic_vector(14 downto 0);
		block_vec_c27 : in std_logic_vector(14 downto 0);
		block_vec_c28 : in std_logic_vector(14 downto 0);
		block_vec_c29 : in std_logic_vector(14 downto 0);
		block_vec_c30 : in std_logic_vector(14 downto 0);
		block_vec_c31 : in std_logic_vector(14 downto 0);
		block_vec_norm_factor : in std_logic_vector(17 downto 0);
		
		block_vec_available : in std_logic;
		block_vec_nxt : out std_logic;
		
		detected : out std_logic;
		valid_out : out std_logic;
		
		pline_clk : in std_logic := '0';
		pline_rst : in std_logic := '0';
	
		clk : std_logic;
		reset : std_logic
	);
	end component svm_accumulator;

	constant WIND_WIDTH_BLOCKS : natural := (WIND_WIDTH_PIXELS/8) - 1;
	constant WIND_HEIGHT_BLOCKS : natural := (WIND_HEIGHT_PIXELS/8) - 1;

	-- SVM coef memory quantization
	type wind_len_quantized_t is array(natural range <>) of natural;
	
	function init_wind_width_q_arr(constant wind_len_blocks : natural;
					constant dp_amt : natural;
					constant dp_idx : natural)
		return wind_len_quantized_t is
		
		variable wind_len_arr : wind_len_quantized_t(dp_amt-1 downto 0);
	begin
		for i in 0 to dp_amt-1 loop
		        if i >= dp_idx then
				wind_len_arr(i) := (wind_len_blocks + dp_amt - 1 - i + dp_idx)/dp_amt;
			else
				wind_len_arr(i) := (wind_len_blocks - 1 + dp_idx - i)/dp_amt;
			end if;
		end loop;
		return wind_len_arr;
	end function;
	
	constant WIND_WIDTH_BLOCKS_QUANTIZED : wind_len_quantized_t(DP_AMOUNT-1 downto 0) :=
		init_wind_width_q_arr(WIND_WIDTH_BLOCKS,DP_AMOUNT,0);
	
	signal feat0 : std_logic_vector(14 downto 0);
	signal feat1 : std_logic_vector(14 downto 0);
	signal feat2 : std_logic_vector(14 downto 0);
	signal feat3 : std_logic_vector(14 downto 0);
	signal feat4 : std_logic_vector(14 downto 0);
	signal feat5 : std_logic_vector(14 downto 0);
	signal feat6 : std_logic_vector(14 downto 0);
	signal feat7 : std_logic_vector(14 downto 0);
	signal feat8 : std_logic_vector(14 downto 0);
	signal feat9 : std_logic_vector(14 downto 0);
	signal feat10 : std_logic_vector(14 downto 0);
	signal feat11 : std_logic_vector(14 downto 0);
	signal feat12 : std_logic_vector(14 downto 0);
	signal feat13 : std_logic_vector(14 downto 0);
	signal feat14 : std_logic_vector(14 downto 0);
	signal feat15 : std_logic_vector(14 downto 0);
	signal feat16 : std_logic_vector(14 downto 0);
	signal feat17 : std_logic_vector(14 downto 0);
	signal feat18 : std_logic_vector(14 downto 0);
	signal feat19 : std_logic_vector(14 downto 0);
	signal feat20 : std_logic_vector(14 downto 0);
	signal feat21 : std_logic_vector(14 downto 0);
	signal feat22 : std_logic_vector(14 downto 0);
	signal feat23 : std_logic_vector(14 downto 0);
	signal feat24 : std_logic_vector(14 downto 0);
	signal feat25 : std_logic_vector(14 downto 0);
	signal feat26 : std_logic_vector(14 downto 0);
	signal feat27 : std_logic_vector(14 downto 0);
	signal feat28 : std_logic_vector(14 downto 0);
	signal feat29 : std_logic_vector(14 downto 0);
	signal feat30 : std_logic_vector(14 downto 0);
	signal feat31 : std_logic_vector(14 downto 0);
	signal norm : std_logic_vector(17 downto 0);
	
	
	signal valid : std_logic;
	signal block_vec_nxt : std_logic;
begin
	nxt_sample <= block_vec_nxt;
	pline : block_extract_pline
	generic map(
		IMG_WIDTH => IMG_WIDTH_PIXELS,
		IMG_HEIGHT => IMG_HEIGHT_PIXELS
		)
	port map(
		data_in => data_in,
		valid_in => valid_in,
		block_vec_nxt => block_vec_nxt,
		
		feat_out0 => feat0,
		feat_out1 => feat1,
		feat_out2 => feat2,
		feat_out3 => feat3,
		feat_out4 => feat4,
		feat_out5 => feat5,
		feat_out6 => feat6,
		feat_out7 => feat7,
		feat_out8 => feat8,
		feat_out9 => feat9,
		feat_out10 => feat10,
		feat_out11 => feat11,
		feat_out12 => feat12,
		feat_out13 => feat13,
		feat_out14 => feat14,
		feat_out15 => feat15,
		feat_out16 => feat16,
		feat_out17 => feat17,
		feat_out18 => feat18,
		feat_out19 => feat19,
		feat_out20 => feat20,
		feat_out21 => feat21,
		feat_out22 => feat22,
		feat_out23 => feat23,
		feat_out24 => feat24,
		feat_out25 => feat25,
		feat_out26 => feat26,
		feat_out27 => feat27,
		feat_out28 => feat28,
		feat_out29 => feat29,
		feat_out30 => feat30,
		feat_out31 => feat31,
		norm_out => norm,
		valid_out => valid,
		clk => pline_clk,
		reset => pline_rst
	);
	
	svm : entity work.svm_accumulator(rtl)
	generic map
	(
		DP_AMOUNT_WIDTH => DP_AMOUNT,
		USE_XIL_BRAM => USE_XIL_BRAM,
		USE_INFERRED_BRAM => USE_INFERRED_BRAM,
		IMG_WIDTH_PIXELS => IMG_WIDTH_PIXELS,
		IMG_HEIGHT_PIXELS => IMG_HEIGHT_PIXELS,
		WIND_WIDTH_PIXELS => WIND_WIDTH_PIXELS,
		WIND_HEIGHT_PIXELS => WIND_HEIGHT_PIXELS
	)
	port map
	(
		reg_data_in => reg_data_in,
		reg_data_out => reg_data_out,
		reg_addr => reg_addr,
		reg_we => reg_we,
		reg_re => reg_re,
		block_vec_c0 => feat0,
		block_vec_c1 => feat1,
		block_vec_c2 => feat2,
		block_vec_c3 => feat3,
		block_vec_c4 => feat4,
		block_vec_c5 => feat5,
		block_vec_c6 => feat6,
		block_vec_c7 => feat7,
		block_vec_c8 => feat8,
		block_vec_c9 => feat9,
		block_vec_c10 => feat10,
		block_vec_c11 => feat11,
		block_vec_c12 => feat12,
		block_vec_c13 => feat13,
		block_vec_c14 => feat14,
		block_vec_c15 => feat15,
		block_vec_c16 => feat16,
		block_vec_c17 => feat17,
		block_vec_c18 => feat18,
		block_vec_c19 => feat19,
		block_vec_c20 => feat20,
		block_vec_c21 => feat21,
		block_vec_c22 => feat22,
		block_vec_c23 => feat23,
		block_vec_c24 => feat24,
		block_vec_c25 => feat25,
		block_vec_c26 => feat26,
		block_vec_c27 => feat27,
		block_vec_c28 => feat28,
		block_vec_c29 => feat29,
		block_vec_c30 => feat30,
		block_vec_c31 => feat31,
		block_vec_norm_factor => norm,
		block_vec_available => valid,
		block_vec_nxt => block_vec_nxt,
		valid_out => valid_out,
		detected => detect_out,
		
		pline_clk => pline_clk,
		pline_rst => pline_rst,
		
		clk => clk,
		reset => reset
	);
end architecture arch;

