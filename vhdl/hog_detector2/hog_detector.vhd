library IEEE;

use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity hog_detector is
generic(
	IMG_WIDTH_PIXELS : natural := 320;
	IMG_HEIGHT_PIXELS : natural := 240;
	WIND_WIDTH_PIXELS : natural := 64;
	WIND_HEIGHT_PIXELS : natural := 128;
	DP_AMOUNT : natural := 2
);
port(
	-- Gradient magnitude and bin index in
	mag_in : in std_logic_vector(8 downto 0);
        idx_in : in std_logic_vector(2 downto 0);
        valid_in : in std_logic;
	
	-- Detection out
	detect_out : out std_logic;
	valid_out : out std_logic;
	
	-- Flow-control signals
	read_out : out std_logic;
	
	-- Config register interface
	reg_data_in : in std_logic_vector(31 downto 0) := (others => '0');
	reg_data_out : out std_logic_vector(31 downto 0);
	reg_addr : in std_logic_vector(1 downto 0) := (others => '0');
	reg_we : in std_logic := '0';
	reg_re : in std_logic := '0';
	
	-- Pipe-line clock/reset
	pline_clk : in std_logic := '0';
	pline_rst : in std_logic := '0';
	
	-- Register interface clock/reset
	clk : in std_logic := '0';
	reset : in std_logic := '0'
);
end entity hog_detector;

architecture arch of hog_detector is
	component block_vec_extractor is
        generic
        (
                BLOCK_WIDTH_CELLS : natural := 2;
                BLOCK_HEIGHT_CELLS : natural := 2;
                
                CELL_WIDTH_PIXELS : natural := 8;
                CELL_HEIGHT_PIXELS : natural := 8;
                
                HIST_LEN : natural := 8;
                
                IMG_WIDTH : natural := 320;
                IMG_HEIGHT : natural := 240
        );
        port
        (
                mag_in : in std_logic_vector(8 downto 0);
                idx_in : in std_logic_vector(2 downto 0);
                
                valid_in : in std_logic;
                
                en : in std_logic;
                
                cell0_bin0_out : out std_logic_vector(14 downto 0); 
                cell0_bin1_out : out std_logic_vector(14 downto 0);
                cell0_bin2_out : out std_logic_vector(14 downto 0);
                cell0_bin3_out : out std_logic_vector(14 downto 0);
                cell0_bin4_out : out std_logic_vector(14 downto 0);
                cell0_bin5_out : out std_logic_vector(14 downto 0);
                cell0_bin6_out : out std_logic_vector(14 downto 0);
                cell0_bin7_out : out std_logic_vector(14 downto 0);
                
                cell1_bin0_out : out std_logic_vector(14 downto 0); 
                cell1_bin1_out : out std_logic_vector(14 downto 0);
                cell1_bin2_out : out std_logic_vector(14 downto 0);
                cell1_bin3_out : out std_logic_vector(14 downto 0);
                cell1_bin4_out : out std_logic_vector(14 downto 0);
                cell1_bin5_out : out std_logic_vector(14 downto 0);
                cell1_bin6_out : out std_logic_vector(14 downto 0);
                cell1_bin7_out : out std_logic_vector(14 downto 0);
                
                cell2_bin0_out : out std_logic_vector(14 downto 0); 
                cell2_bin1_out : out std_logic_vector(14 downto 0);
                cell2_bin2_out : out std_logic_vector(14 downto 0);
                cell2_bin3_out : out std_logic_vector(14 downto 0);
                cell2_bin4_out : out std_logic_vector(14 downto 0);
                cell2_bin5_out : out std_logic_vector(14 downto 0);
                cell2_bin6_out : out std_logic_vector(14 downto 0);
                cell2_bin7_out : out std_logic_vector(14 downto 0);
                
                cell3_bin0_out : out std_logic_vector(14 downto 0); 
                cell3_bin1_out : out std_logic_vector(14 downto 0);
                cell3_bin2_out : out std_logic_vector(14 downto 0);
                cell3_bin3_out : out std_logic_vector(14 downto 0);
                cell3_bin4_out : out std_logic_vector(14 downto 0);
                cell3_bin5_out : out std_logic_vector(14 downto 0);
                cell3_bin6_out : out std_logic_vector(14 downto 0);
                cell3_bin7_out : out std_logic_vector(14 downto 0);
                
                read_out : out std_logic;
                valid_out : out std_logic;
                
                clk : in std_logic;
                reset : in std_logic
        );
        end component block_vec_extractor;
	
	component norm_block is
	port
	(
		valid_in : in std_logic := '0';
		en : in std_logic := '0';

		feat_in0 : in std_logic_vector(14 downto 0);
		feat_in1 : in std_logic_vector(14 downto 0);
		feat_in2 : in std_logic_vector(14 downto 0);
		feat_in3 : in std_logic_vector(14 downto 0);
		feat_in4 : in std_logic_vector(14 downto 0);
		feat_in5 : in std_logic_vector(14 downto 0);
		feat_in6 : in std_logic_vector(14 downto 0);
		feat_in7 : in std_logic_vector(14 downto 0);
		feat_in8 : in std_logic_vector(14 downto 0);
		feat_in9 : in std_logic_vector(14 downto 0);
		feat_in10 : in std_logic_vector(14 downto 0);
		feat_in11 : in std_logic_vector(14 downto 0);
		feat_in12 : in std_logic_vector(14 downto 0);
		feat_in13 : in std_logic_vector(14 downto 0);
		feat_in14 : in std_logic_vector(14 downto 0);
		feat_in15 : in std_logic_vector(14 downto 0);
		feat_in16 : in std_logic_vector(14 downto 0);
		feat_in17 : in std_logic_vector(14 downto 0);
		feat_in18 : in std_logic_vector(14 downto 0);
		feat_in19 : in std_logic_vector(14 downto 0);
		feat_in20 : in std_logic_vector(14 downto 0);
		feat_in21 : in std_logic_vector(14 downto 0);
		feat_in22 : in std_logic_vector(14 downto 0);
		feat_in23 : in std_logic_vector(14 downto 0);
		feat_in24 : in std_logic_vector(14 downto 0);
		feat_in25 : in std_logic_vector(14 downto 0);
		feat_in26 : in std_logic_vector(14 downto 0);
		feat_in27 : in std_logic_vector(14 downto 0);
		feat_in28 : in std_logic_vector(14 downto 0);
		feat_in29 : in std_logic_vector(14 downto 0);
		feat_in30 : in std_logic_vector(14 downto 0);
		feat_in31 : in std_logic_vector(14 downto 0);
		
		feat_out0 : out std_logic_vector(14 downto 0);
		feat_out1 : out std_logic_vector(14 downto 0);
		feat_out2 : out std_logic_vector(14 downto 0);
		feat_out3 : out std_logic_vector(14 downto 0);
		feat_out4 : out std_logic_vector(14 downto 0);
		feat_out5 : out std_logic_vector(14 downto 0);
		feat_out6 : out std_logic_vector(14 downto 0);
		feat_out7 : out std_logic_vector(14 downto 0);
		feat_out8 : out std_logic_vector(14 downto 0);
		feat_out9 : out std_logic_vector(14 downto 0);
		feat_out10 : out std_logic_vector(14 downto 0);
		feat_out11 : out std_logic_vector(14 downto 0);
		feat_out12 : out std_logic_vector(14 downto 0);
		feat_out13 : out std_logic_vector(14 downto 0);
		feat_out14 : out std_logic_vector(14 downto 0);
		feat_out15 : out std_logic_vector(14 downto 0);
		feat_out16 : out std_logic_vector(14 downto 0);
		feat_out17 : out std_logic_vector(14 downto 0);
		feat_out18 : out std_logic_vector(14 downto 0);
		feat_out19 : out std_logic_vector(14 downto 0);
		feat_out20 : out std_logic_vector(14 downto 0);
		feat_out21 : out std_logic_vector(14 downto 0);
		feat_out22 : out std_logic_vector(14 downto 0);
		feat_out23 : out std_logic_vector(14 downto 0);
		feat_out24 : out std_logic_vector(14 downto 0);
		feat_out25 : out std_logic_vector(14 downto 0);
		feat_out26 : out std_logic_vector(14 downto 0);
		feat_out27 : out std_logic_vector(14 downto 0);
		feat_out28 : out std_logic_vector(14 downto 0);
		feat_out29 : out std_logic_vector(14 downto 0);
		feat_out30 : out std_logic_vector(14 downto 0);
		feat_out31 : out std_logic_vector(14 downto 0);
		norm_out : out std_logic_vector(17 downto 0);
		
		valid_out : out std_logic;
		
		clk : in std_logic;
		reset : in std_logic
	);
	end component norm_block;
	
	component svm_accumulator is
	generic(
		DP_AMOUNT_WIDTH : natural := 2;
		USE_XIL_BRAM : boolean := FALSE;
		USE_INFERRED_BRAM : boolean := FALSE;
		IMG_WIDTH_PIXELS : natural := 320;
		IMG_HEIGHT_PIXELS : natural := 240;
		WIND_WIDTH_PIXELS : natural := 64;
		WIND_HEIGHT_PIXELS : natural := 128;
                ACCUM_BRAM_INT_BITS : natural := 5;
                ACCUM_BRAM_FRAC_BITS : natural := 32
	);
	port(
		reg_data_in : in std_logic_vector(31 downto 0);
		reg_data_out : out std_logic_vector(31 downto 0);
		reg_addr : in std_logic_vector(1 downto 0);
		reg_we : in std_logic;
		reg_re : in std_logic;
		
		block_vec_c0 : in std_logic_vector(14 downto 0);
		block_vec_c1 : in std_logic_vector(14 downto 0);
		block_vec_c2 : in std_logic_vector(14 downto 0);
		block_vec_c3 : in std_logic_vector(14 downto 0);
		block_vec_c4 : in std_logic_vector(14 downto 0);
		block_vec_c5 : in std_logic_vector(14 downto 0);
		block_vec_c6 : in std_logic_vector(14 downto 0);
		block_vec_c7 : in std_logic_vector(14 downto 0);
		block_vec_c8 : in std_logic_vector(14 downto 0);
		block_vec_c9 : in std_logic_vector(14 downto 0);
		block_vec_c10 : in std_logic_vector(14 downto 0);
		block_vec_c11 : in std_logic_vector(14 downto 0);
		block_vec_c12 : in std_logic_vector(14 downto 0);
		block_vec_c13 : in std_logic_vector(14 downto 0);
		block_vec_c14 : in std_logic_vector(14 downto 0);
		block_vec_c15 : in std_logic_vector(14 downto 0);
		block_vec_c16 : in std_logic_vector(14 downto 0);
		block_vec_c17 : in std_logic_vector(14 downto 0);
		block_vec_c18 : in std_logic_vector(14 downto 0);
		block_vec_c19 : in std_logic_vector(14 downto 0);
		block_vec_c20 : in std_logic_vector(14 downto 0);
		block_vec_c21 : in std_logic_vector(14 downto 0);
		block_vec_c22 : in std_logic_vector(14 downto 0);
		block_vec_c23 : in std_logic_vector(14 downto 0);
		block_vec_c24 : in std_logic_vector(14 downto 0);
		block_vec_c25 : in std_logic_vector(14 downto 0);
		block_vec_c26 : in std_logic_vector(14 downto 0);
		block_vec_c27 : in std_logic_vector(14 downto 0);
		block_vec_c28 : in std_logic_vector(14 downto 0);
		block_vec_c29 : in std_logic_vector(14 downto 0);
		block_vec_c30 : in std_logic_vector(14 downto 0);
		block_vec_c31 : in std_logic_vector(14 downto 0);
		block_vec_norm_factor : in std_logic_vector(17 downto 0);
		
		block_vec_available : in std_logic;
		block_vec_nxt : out std_logic;
		
		detected : out std_logic;
		valid_out : out std_logic;
		
		pline_clk : in std_logic := '0';
		pline_rst : in std_logic := '0';
	
		clk : std_logic;
		reset : std_logic
	);
	end component svm_accumulator;

	constant WIND_WIDTH_BLOCKS : natural := (WIND_WIDTH_PIXELS/8) - 1;
	constant WIND_HEIGHT_BLOCKS : natural := (WIND_HEIGHT_PIXELS/8) - 1;

	-- SVM coef memory quantization
	type wind_len_quantized_t is array(natural range <>) of natural;
	
	function init_wind_width_q_arr(constant wind_len_blocks : natural;
					constant dp_amt : natural;
					constant dp_idx : natural)
		return wind_len_quantized_t is
		
		variable wind_len_arr : wind_len_quantized_t(dp_amt-1 downto 0);
	begin
		for i in 0 to dp_amt-1 loop
		        if i >= dp_idx then
				wind_len_arr(i) := (wind_len_blocks + dp_amt - 1 - i + dp_idx)/dp_amt;
			else
				wind_len_arr(i) := (wind_len_blocks - 1 + dp_idx - i)/dp_amt;
			end if;
		end loop;
		return wind_len_arr;
	end function;
	
	constant WIND_WIDTH_BLOCKS_QUANTIZED : wind_len_quantized_t(DP_AMOUNT-1 downto 0) :=
		init_wind_width_q_arr(WIND_WIDTH_BLOCKS,DP_AMOUNT,0);
	
	signal feat_vec_e0 : std_logic_vector(14 downto 0);
	signal feat_vec_e1 : std_logic_vector(14 downto 0);
	signal feat_vec_e2 : std_logic_vector(14 downto 0);
	signal feat_vec_e3 : std_logic_vector(14 downto 0);
	signal feat_vec_e4 : std_logic_vector(14 downto 0);
	signal feat_vec_e5 : std_logic_vector(14 downto 0);
	signal feat_vec_e6 : std_logic_vector(14 downto 0);
	signal feat_vec_e7 : std_logic_vector(14 downto 0);
	signal feat_vec_e8 : std_logic_vector(14 downto 0);
	signal feat_vec_e9 : std_logic_vector(14 downto 0);
	signal feat_vec_e10 : std_logic_vector(14 downto 0);
	signal feat_vec_e11 : std_logic_vector(14 downto 0);
	signal feat_vec_e12 : std_logic_vector(14 downto 0);
	signal feat_vec_e13 : std_logic_vector(14 downto 0);
	signal feat_vec_e14 : std_logic_vector(14 downto 0);
	signal feat_vec_e15 : std_logic_vector(14 downto 0);
	signal feat_vec_e16 : std_logic_vector(14 downto 0);
	signal feat_vec_e17 : std_logic_vector(14 downto 0);
	signal feat_vec_e18 : std_logic_vector(14 downto 0);
	signal feat_vec_e19 : std_logic_vector(14 downto 0);
	signal feat_vec_e20 : std_logic_vector(14 downto 0);
	signal feat_vec_e21 : std_logic_vector(14 downto 0);
	signal feat_vec_e22 : std_logic_vector(14 downto 0);
	signal feat_vec_e23 : std_logic_vector(14 downto 0);
	signal feat_vec_e24 : std_logic_vector(14 downto 0);
	signal feat_vec_e25 : std_logic_vector(14 downto 0);
	signal feat_vec_e26 : std_logic_vector(14 downto 0);
	signal feat_vec_e27 : std_logic_vector(14 downto 0);
	signal feat_vec_e28 : std_logic_vector(14 downto 0);
	signal feat_vec_e29 : std_logic_vector(14 downto 0);
	signal feat_vec_e30 : std_logic_vector(14 downto 0);
	signal feat_vec_e31 : std_logic_vector(14 downto 0);
	
	signal feat_out0 : std_logic_vector(14 downto 0);
	signal feat_out1 : std_logic_vector(14 downto 0);
	signal feat_out2 : std_logic_vector(14 downto 0);
	signal feat_out3 : std_logic_vector(14 downto 0);
	signal feat_out4 : std_logic_vector(14 downto 0);
	signal feat_out5 : std_logic_vector(14 downto 0);
	signal feat_out6 : std_logic_vector(14 downto 0);
	signal feat_out7 : std_logic_vector(14 downto 0);
	signal feat_out8 : std_logic_vector(14 downto 0);
	signal feat_out9 : std_logic_vector(14 downto 0);
	signal feat_out10 : std_logic_vector(14 downto 0);
	signal feat_out11 : std_logic_vector(14 downto 0);
	signal feat_out12 : std_logic_vector(14 downto 0);
	signal feat_out13 : std_logic_vector(14 downto 0);
	signal feat_out14 : std_logic_vector(14 downto 0);
	signal feat_out15 : std_logic_vector(14 downto 0);
	signal feat_out16 : std_logic_vector(14 downto 0);
	signal feat_out17 : std_logic_vector(14 downto 0);
	signal feat_out18 : std_logic_vector(14 downto 0);
	signal feat_out19 : std_logic_vector(14 downto 0);
	signal feat_out20 : std_logic_vector(14 downto 0);
	signal feat_out21 : std_logic_vector(14 downto 0);
	signal feat_out22 : std_logic_vector(14 downto 0);
	signal feat_out23 : std_logic_vector(14 downto 0);
	signal feat_out24 : std_logic_vector(14 downto 0);
	signal feat_out25 : std_logic_vector(14 downto 0);
	signal feat_out26 : std_logic_vector(14 downto 0);
	signal feat_out27 : std_logic_vector(14 downto 0);
	signal feat_out28 : std_logic_vector(14 downto 0);
	signal feat_out29 : std_logic_vector(14 downto 0);
	signal feat_out30 : std_logic_vector(14 downto 0);
	signal feat_out31 : std_logic_vector(14 downto 0);
	signal norm : std_logic_vector(17 downto 0);
	
	
	signal block_valid : std_logic;
	signal block_norm_valid : std_logic;
	signal block_vec_nxt : std_logic;
begin
	block_xtractor : block_vec_extractor
        generic map
        (
                IMG_WIDTH => IMG_WIDTH_PIXELS,
                IMG_HEIGHT => IMG_HEIGHT_PIXELS
        )
        port map
        (
                mag_in => mag_in,
                idx_in => idx_in,
                valid_in => valid_in,
                
                en => block_vec_nxt,
                
                read_out => read_out,
                valid_out => block_valid,
		
		cell0_bin0_out => feat_vec_e0,
		cell0_bin1_out => feat_vec_e1,
		cell0_bin2_out => feat_vec_e2,
		cell0_bin3_out => feat_vec_e3,
		cell0_bin4_out => feat_vec_e4,
		cell0_bin5_out => feat_vec_e5,
		cell0_bin6_out => feat_vec_e6,
		cell0_bin7_out => feat_vec_e7,
		cell1_bin0_out => feat_vec_e8,
		cell1_bin1_out => feat_vec_e9,
		cell1_bin2_out => feat_vec_e10,
		cell1_bin3_out => feat_vec_e11,
		cell1_bin4_out => feat_vec_e12,
		cell1_bin5_out => feat_vec_e13,
		cell1_bin6_out => feat_vec_e14,
		cell1_bin7_out => feat_vec_e15,
		cell2_bin0_out => feat_vec_e16,
		cell2_bin1_out => feat_vec_e17,
		cell2_bin2_out => feat_vec_e18,
		cell2_bin3_out => feat_vec_e19,
		cell2_bin4_out => feat_vec_e20,
		cell2_bin5_out => feat_vec_e21,
		cell2_bin6_out => feat_vec_e22,
		cell2_bin7_out => feat_vec_e23,
		cell3_bin0_out => feat_vec_e24,
		cell3_bin1_out => feat_vec_e25,
		cell3_bin2_out => feat_vec_e26,
		cell3_bin3_out => feat_vec_e27,
		cell3_bin4_out => feat_vec_e28,
		cell3_bin5_out => feat_vec_e29,
		cell3_bin6_out => feat_vec_e30,
		cell3_bin7_out => feat_vec_e31,
		
		clk => pline_clk,
		reset => pline_rst
	);
	
	norm_module : norm_block
	port map
	(
		feat_in0 => feat_vec_e0,
		feat_in1 => feat_vec_e1,
		feat_in2 => feat_vec_e2,
		feat_in3 => feat_vec_e3,
		feat_in4 => feat_vec_e4,
		feat_in5 => feat_vec_e5,
		feat_in6 => feat_vec_e6,
		feat_in7 => feat_vec_e7,
		feat_in8 => feat_vec_e8,
		feat_in9 => feat_vec_e9,
		feat_in10 => feat_vec_e10,
		feat_in11 => feat_vec_e11,
		feat_in12 => feat_vec_e12,
		feat_in13 => feat_vec_e13,
		feat_in14 => feat_vec_e14,
		feat_in15 => feat_vec_e15,
		feat_in16 => feat_vec_e16,
		feat_in17 => feat_vec_e17,
		feat_in18 => feat_vec_e18,
		feat_in19 => feat_vec_e19,
		feat_in20 => feat_vec_e20,
		feat_in21 => feat_vec_e21,
		feat_in22 => feat_vec_e22,
		feat_in23 => feat_vec_e23,
		feat_in24 => feat_vec_e24,
		feat_in25 => feat_vec_e25,
		feat_in26 => feat_vec_e26,
		feat_in27 => feat_vec_e27,
		feat_in28 => feat_vec_e28,
		feat_in29 => feat_vec_e29,
		feat_in30 => feat_vec_e30,
		feat_in31 => feat_vec_e31,
		
		valid_in => block_valid,
		en => block_vec_nxt,
		
		feat_out0 => feat_out0,
		feat_out1 => feat_out1,
		feat_out2 => feat_out2,
		feat_out3 => feat_out3,
		feat_out4 => feat_out4,
		feat_out5 => feat_out5,
		feat_out6 => feat_out6,
		feat_out7 => feat_out7,
		feat_out8 => feat_out8,
		feat_out9 => feat_out9,
		feat_out10 => feat_out10,
		feat_out11 => feat_out11,
		feat_out12 => feat_out12,
		feat_out13 => feat_out13,
		feat_out14 => feat_out14,
		feat_out15 => feat_out15,
		feat_out16 => feat_out16,
		feat_out17 => feat_out17,
		feat_out18 => feat_out18,
		feat_out19 => feat_out19,
		feat_out20 => feat_out20,
		feat_out21 => feat_out21,
		feat_out22 => feat_out22,
		feat_out23 => feat_out23,
		feat_out24 => feat_out24,
		feat_out25 => feat_out25,
		feat_out26 => feat_out26,
		feat_out27 => feat_out27,
		feat_out28 => feat_out28,
		feat_out29 => feat_out29,
		feat_out30 => feat_out30,
		feat_out31 => feat_out31,
		norm_out => norm,
		valid_out => block_norm_valid,
		
		clk => pline_clk,
		reset => pline_rst
	);
	
	svm : entity work.svm_accumulator(rtl)
	generic map
	(
		DP_AMOUNT_WIDTH => DP_AMOUNT,
		IMG_WIDTH_PIXELS => IMG_WIDTH_PIXELS,
		IMG_HEIGHT_PIXELS => IMG_HEIGHT_PIXELS,
		WIND_WIDTH_PIXELS => WIND_WIDTH_PIXELS,
		WIND_HEIGHT_PIXELS => WIND_HEIGHT_PIXELS
	)
	port map
	(
		reg_data_in => reg_data_in,
		reg_data_out => reg_data_out,
		reg_addr => reg_addr,
		reg_we => reg_we,
		reg_re => reg_re,
		block_vec_c0 => feat_out0,
		block_vec_c1 => feat_out1,
		block_vec_c2 => feat_out2,
		block_vec_c3 => feat_out3,
		block_vec_c4 => feat_out4,
		block_vec_c5 => feat_out5,
		block_vec_c6 => feat_out6,
		block_vec_c7 => feat_out7,
		block_vec_c8 => feat_out8,
		block_vec_c9 => feat_out9,
		block_vec_c10 => feat_out10,
		block_vec_c11 => feat_out11,
		block_vec_c12 => feat_out12,
		block_vec_c13 => feat_out13,
		block_vec_c14 => feat_out14,
		block_vec_c15 => feat_out15,
		block_vec_c16 => feat_out16,
		block_vec_c17 => feat_out17,
		block_vec_c18 => feat_out18,
		block_vec_c19 => feat_out19,
		block_vec_c20 => feat_out20,
		block_vec_c21 => feat_out21,
		block_vec_c22 => feat_out22,
		block_vec_c23 => feat_out23,
		block_vec_c24 => feat_out24,
		block_vec_c25 => feat_out25,
		block_vec_c26 => feat_out26,
		block_vec_c27 => feat_out27,
		block_vec_c28 => feat_out28,
		block_vec_c29 => feat_out29,
		block_vec_c30 => feat_out30,
		block_vec_c31 => feat_out31,
		block_vec_norm_factor => norm,
		block_vec_available => block_norm_valid,
		block_vec_nxt => block_vec_nxt,
		
		valid_out => valid_out,
		detected => detect_out,
		
		pline_clk => pline_clk,
		pline_rst => pline_rst,
		
		clk => clk,
		reset => reset
	);
end architecture arch;

