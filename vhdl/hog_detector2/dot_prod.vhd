 
library IEEE;

use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity dot_prod is
port
(	
	valid_in : in std_logic := '0';

	x_c0 : in std_logic_vector(14 downto 0);
	x_c1 : in std_logic_vector(14 downto 0);
	x_c2 : in std_logic_vector(14 downto 0);
	x_c3 : in std_logic_vector(14 downto 0);
	x_c4 : in std_logic_vector(14 downto 0);
	x_c5 : in std_logic_vector(14 downto 0);
	x_c6 : in std_logic_vector(14 downto 0);
	x_c7 : in std_logic_vector(14 downto 0);
	x_c8 : in std_logic_vector(14 downto 0);
	x_c9 : in std_logic_vector(14 downto 0);
	x_c10 : in std_logic_vector(14 downto 0);
	x_c11 : in std_logic_vector(14 downto 0);
	x_c12 : in std_logic_vector(14 downto 0);
	x_c13 : in std_logic_vector(14 downto 0);
	x_c14 : in std_logic_vector(14 downto 0);
	x_c15 : in std_logic_vector(14 downto 0);
	x_c16 : in std_logic_vector(14 downto 0);
	x_c17 : in std_logic_vector(14 downto 0);
	x_c18 : in std_logic_vector(14 downto 0);
	x_c19 : in std_logic_vector(14 downto 0);
	x_c20 : in std_logic_vector(14 downto 0);
	x_c21 : in std_logic_vector(14 downto 0);
	x_c22 : in std_logic_vector(14 downto 0);
	x_c23 : in std_logic_vector(14 downto 0);
	x_c24 : in std_logic_vector(14 downto 0);
	x_c25 : in std_logic_vector(14 downto 0);
	x_c26 : in std_logic_vector(14 downto 0);
	x_c27 : in std_logic_vector(14 downto 0);
	x_c28 : in std_logic_vector(14 downto 0);
	x_c29 : in std_logic_vector(14 downto 0);
	x_c30 : in std_logic_vector(14 downto 0);
	x_c31 : in std_logic_vector(14 downto 0);
	
	w_c0 : in std_logic_vector(17 downto 0);
	w_c1 : in std_logic_vector(17 downto 0);
	w_c2 : in std_logic_vector(17 downto 0);
	w_c3 : in std_logic_vector(17 downto 0);
	w_c4 : in std_logic_vector(17 downto 0);
	w_c5 : in std_logic_vector(17 downto 0);
	w_c6 : in std_logic_vector(17 downto 0);
	w_c7 : in std_logic_vector(17 downto 0);
	w_c8 : in std_logic_vector(17 downto 0);
	w_c9 : in std_logic_vector(17 downto 0);
	w_c10 : in std_logic_vector(17 downto 0);
	w_c11 : in std_logic_vector(17 downto 0);
	w_c12 : in std_logic_vector(17 downto 0);
	w_c13 : in std_logic_vector(17 downto 0);
	w_c14 : in std_logic_vector(17 downto 0);
	w_c15 : in std_logic_vector(17 downto 0);
	w_c16 : in std_logic_vector(17 downto 0);
	w_c17 : in std_logic_vector(17 downto 0);
	w_c18 : in std_logic_vector(17 downto 0);
	w_c19 : in std_logic_vector(17 downto 0);
	w_c20 : in std_logic_vector(17 downto 0);
	w_c21 : in std_logic_vector(17 downto 0);
	w_c22 : in std_logic_vector(17 downto 0);
	w_c23 : in std_logic_vector(17 downto 0);
	w_c24 : in std_logic_vector(17 downto 0);
	w_c25 : in std_logic_vector(17 downto 0);
	w_c26 : in std_logic_vector(17 downto 0);
	w_c27 : in std_logic_vector(17 downto 0);
	w_c28 : in std_logic_vector(17 downto 0);
	w_c29 : in std_logic_vector(17 downto 0);
	w_c30 : in std_logic_vector(17 downto 0);
	w_c31 : in std_logic_vector(17 downto 0);
	
	val_out : out std_logic_vector(38 downto 0);
	
	valid_out : out std_logic;
	
	clk : in std_logic;
	reset : in std_logic
);
end entity dot_prod;

architecture rtl of dot_prod is
	signal m_c0 : signed(33 downto 0);
	signal m_c1 : signed(33 downto 0);
	signal m_c2 : signed(33 downto 0);
	signal m_c3 : signed(33 downto 0);
	signal m_c4 : signed(33 downto 0);
	signal m_c5 : signed(33 downto 0);
	signal m_c6 : signed(33 downto 0);
	signal m_c7 : signed(33 downto 0);
	signal m_c8 : signed(33 downto 0);
	signal m_c9 : signed(33 downto 0);
	signal m_c10 : signed(33 downto 0);
	signal m_c11 : signed(33 downto 0);
	signal m_c12 : signed(33 downto 0);
	signal m_c13 : signed(33 downto 0);
	signal m_c14 : signed(33 downto 0);
	signal m_c15 : signed(33 downto 0);
	signal m_c16 : signed(33 downto 0);
	signal m_c17 : signed(33 downto 0);
	signal m_c18 : signed(33 downto 0);
	signal m_c19 : signed(33 downto 0);
	signal m_c20 : signed(33 downto 0);
	signal m_c21 : signed(33 downto 0);
	signal m_c22 : signed(33 downto 0);
	signal m_c23 : signed(33 downto 0);
	signal m_c24 : signed(33 downto 0);
	signal m_c25 : signed(33 downto 0);
	signal m_c26 : signed(33 downto 0);
	signal m_c27 : signed(33 downto 0);
	signal m_c28 : signed(33 downto 0);
	signal m_c29 : signed(33 downto 0);
	signal m_c30 : signed(33 downto 0);
	signal m_c31 : signed(33 downto 0);
	
	signal mbuff0 : signed(33 downto 0) := (others => '0');
	signal mbuff1 : signed(33 downto 0) := (others => '0');
	signal mbuff2 : signed(33 downto 0) := (others => '0');
	signal mbuff3 : signed(33 downto 0) := (others => '0');
	signal mbuff4 : signed(33 downto 0) := (others => '0');
	signal mbuff5 : signed(33 downto 0) := (others => '0');
	signal mbuff6 : signed(33 downto 0) := (others => '0');
	signal mbuff7 : signed(33 downto 0) := (others => '0');
	signal mbuff8 : signed(33 downto 0) := (others => '0');
	signal mbuff9 : signed(33 downto 0) := (others => '0');
	signal mbuff10 : signed(33 downto 0) := (others => '0');
	signal mbuff11 : signed(33 downto 0) := (others => '0');
	signal mbuff12 : signed(33 downto 0) := (others => '0');
	signal mbuff13 : signed(33 downto 0) := (others => '0');
	signal mbuff14 : signed(33 downto 0) := (others => '0');
	signal mbuff15 : signed(33 downto 0) := (others => '0');
	signal mbuff16 : signed(33 downto 0) := (others => '0');
	signal mbuff17 : signed(33 downto 0) := (others => '0');
	signal mbuff18 : signed(33 downto 0) := (others => '0');
	signal mbuff19 : signed(33 downto 0) := (others => '0');
	signal mbuff20 : signed(33 downto 0) := (others => '0');
	signal mbuff21 : signed(33 downto 0) := (others => '0');
	signal mbuff22 : signed(33 downto 0) := (others => '0');
	signal mbuff23 : signed(33 downto 0) := (others => '0');
	signal mbuff24 : signed(33 downto 0) := (others => '0');
	signal mbuff25 : signed(33 downto 0) := (others => '0');
	signal mbuff26 : signed(33 downto 0) := (others => '0');
	signal mbuff27 : signed(33 downto 0) := (others => '0');
	signal mbuff28 : signed(33 downto 0) := (others => '0');
	signal mbuff29 : signed(33 downto 0) := (others => '0');
	signal mbuff30 : signed(33 downto 0) := (others => '0');
	signal mbuff31 : signed(33 downto 0) := (others => '0');
	
	signal add0 : signed(34 downto 0);
	signal add1 : signed(34 downto 0);
	signal add2 : signed(34 downto 0);
	signal add3 : signed(34 downto 0);
	signal add4 : signed(34 downto 0);
	signal add5 : signed(34 downto 0);
	signal add6 : signed(34 downto 0);
	signal add7 : signed(34 downto 0);
	signal add8 : signed(34 downto 0);
	signal add9 : signed(34 downto 0);
	signal add10 : signed(34 downto 0);
	signal add11 : signed(34 downto 0);
	signal add12 : signed(34 downto 0);
	signal add13 : signed(34 downto 0);
	signal add14 : signed(34 downto 0);
	signal add15 : signed(34 downto 0);
	
	signal add16 : signed(35 downto 0);
	signal add17 : signed(35 downto 0);
	signal add18 : signed(35 downto 0);
	signal add19 : signed(35 downto 0);
	signal add20 : signed(35 downto 0);
	signal add21 : signed(35 downto 0);
	signal add22 : signed(35 downto 0);
	signal add23 : signed(35 downto 0);
	
	signal add24 : signed(36 downto 0);
	signal add25 : signed(36 downto 0);
	signal add26 : signed(36 downto 0);
	signal add27 : signed(36 downto 0);
	
	signal add28 : signed(37 downto 0);
	signal add29 : signed(37 downto 0);
	
	signal add30 : signed(38 downto 0);
	
	signal addbuff : signed(38 downto 0);
	
	signal valid : std_logic_vector(1 downto 0) := "00";
begin
	m_c0 <= signed('0'&x_c0) * signed(w_c0);
	m_c1 <= signed('0'&x_c1) * signed(w_c1);
	m_c2 <= signed('0'&x_c2) * signed(w_c2);
	m_c3 <= signed('0'&x_c3) * signed(w_c3);
	m_c4 <= signed('0'&x_c4) * signed(w_c4);
	m_c5 <= signed('0'&x_c5) * signed(w_c5);
	m_c6 <= signed('0'&x_c6) * signed(w_c6);
	m_c7 <= signed('0'&x_c7) * signed(w_c7);
	m_c8 <= signed('0'&x_c8) * signed(w_c8);
	m_c9 <= signed('0'&x_c9) * signed(w_c9);
	m_c10 <= signed('0'&x_c10) * signed(w_c10);
	m_c11 <= signed('0'&x_c11) * signed(w_c11);
	m_c12 <= signed('0'&x_c12) * signed(w_c12);
	m_c13 <= signed('0'&x_c13) * signed(w_c13);
	m_c14 <= signed('0'&x_c14) * signed(w_c14);
	m_c15 <= signed('0'&x_c15) * signed(w_c15);
	m_c16 <= signed('0'&x_c16) * signed(w_c16);
	m_c17 <= signed('0'&x_c17) * signed(w_c17);
	m_c18 <= signed('0'&x_c18) * signed(w_c18);
	m_c19 <= signed('0'&x_c19) * signed(w_c19);
	m_c20 <= signed('0'&x_c20) * signed(w_c20);
	m_c21 <= signed('0'&x_c21) * signed(w_c21);
	m_c22 <= signed('0'&x_c22) * signed(w_c22);
	m_c23 <= signed('0'&x_c23) * signed(w_c23);
	m_c24 <= signed('0'&x_c24) * signed(w_c24);
	m_c25 <= signed('0'&x_c25) * signed(w_c25);
	m_c26 <= signed('0'&x_c26) * signed(w_c26);
	m_c27 <= signed('0'&x_c27) * signed(w_c27);
	m_c28 <= signed('0'&x_c28) * signed(w_c28);
	m_c29 <= signed('0'&x_c29) * signed(w_c29);
	m_c30 <= signed('0'&x_c30) * signed(w_c30);
	m_c31 <= signed('0'&x_c31) * signed(w_c31);
	
	
	
	
	val_out <= std_logic_vector(addbuff);
	valid_out <= valid(1);
	valid_proc : process(clk)
	begin
		if rising_edge(clk) then
			if reset = '1' then
				valid <= "00";
			else
				valid(1) <= valid(0);
				valid(0) <= valid_in;
			end if;
		end if;
	end process valid_proc;
	pline_proc : process(clk)
	begin
		-- if reset = '1' then
-- 			mbuff0 <= (others => '0');
-- 			mbuff1 <= (others => '0');
-- 			mbuff2 <= (others => '0');
-- 			mbuff3 <= (others => '0');
-- 			mbuff4 <= (others => '0');
-- 			mbuff5 <= (others => '0');
-- 			mbuff6 <= (others => '0');
-- 			mbuff7 <= (others => '0');
-- 			mbuff8 <= (others => '0');
-- 			mbuff9 <= (others => '0');
-- 			mbuff10 <= (others => '0');
-- 			mbuff11 <= (others => '0');
-- 			mbuff12 <= (others => '0');
-- 			mbuff13 <= (others => '0');
-- 			mbuff14 <= (others => '0');
-- 			mbuff15 <= (others => '0');
-- 			mbuff16 <= (others => '0');
-- 			mbuff17 <= (others => '0');
-- 			mbuff18 <= (others => '0');
-- 			mbuff19 <= (others => '0');
-- 			mbuff20 <= (others => '0');
-- 			mbuff21 <= (others => '0');
-- 			mbuff22 <= (others => '0');
-- 			mbuff23 <= (others => '0');
-- 			mbuff24 <= (others => '0');
-- 			mbuff25 <= (others => '0');
-- 			mbuff26 <= (others => '0');
-- 			mbuff27 <= (others => '0');
-- 			mbuff28 <= (others => '0');
-- 			mbuff29 <= (others => '0');
-- 			mbuff30 <= (others => '0');
-- 			mbuff31 <= (others => '0');
-- 			addbuff <= (others => '0');
--		elsif rising_edge(clk) then
		if rising_edge(clk) then
			mbuff0 <= m_c0;
			mbuff1 <= m_c1;
			mbuff2 <= m_c2;
			mbuff3 <= m_c3;
			mbuff4 <= m_c4;
			mbuff5 <= m_c5;
			mbuff6 <= m_c6;
			mbuff7 <= m_c7;
			mbuff8 <= m_c8;
			mbuff9 <= m_c9;
			mbuff10 <= m_c10;
			mbuff11 <= m_c11;
			mbuff12 <= m_c12;
			mbuff13 <= m_c13;
			mbuff14 <= m_c14;
			mbuff15 <= m_c15;
			mbuff16 <= m_c16;
			mbuff17 <= m_c17;
			mbuff18 <= m_c18;
			mbuff19 <= m_c19;
			mbuff20 <= m_c20;
			mbuff21 <= m_c21;
			mbuff22 <= m_c22;
			mbuff23 <= m_c23;
			mbuff24 <= m_c24;
			mbuff25 <= m_c25;
			mbuff26 <= m_c26;
			mbuff27 <= m_c27;
			mbuff28 <= m_c28;
			mbuff29 <= m_c29;
			mbuff30 <= m_c30;
			mbuff31 <= m_c31;
			
			add0 <= (mbuff0(33)&mbuff0) + (mbuff1(33)&mbuff1);
			add1 <= (mbuff2(33)&mbuff2) + (mbuff3(33)&mbuff3);
			add2 <= (mbuff4(33)&mbuff4) + (mbuff5(33)&mbuff5);
			add3 <= (mbuff6(33)&mbuff6) + (mbuff7(33)&mbuff7);
			add4 <= (mbuff8(33)&mbuff8) + (mbuff9(33)&mbuff9);
			add5 <= (mbuff10(33)&mbuff10) + (mbuff11(33)&mbuff11);
			add6 <= (mbuff12(33)&mbuff12) + (mbuff13(33)&mbuff13);
			add7 <= (mbuff14(33)&mbuff14) + (mbuff15(33)&mbuff15);
			add8 <= (mbuff16(33)&mbuff16) + (mbuff17(33)&mbuff17);
			add9 <= (mbuff18(33)&mbuff18) + (mbuff19(33)&mbuff19);
			add10 <= (mbuff20(33)&mbuff20) + (mbuff21(33)&mbuff21);
			add11 <= (mbuff22(33)&mbuff22) + (mbuff23(33)&mbuff23);
			add12 <= (mbuff24(33)&mbuff24) + (mbuff25(33)&mbuff25);
			add13 <= (mbuff26(33)&mbuff26) + (mbuff27(33)&mbuff27);
			add14 <= (mbuff28(33)&mbuff28) + (mbuff29(33)&mbuff29);
			add15 <= (mbuff30(33)&mbuff30) + (mbuff31(33)&mbuff31);
			
			add16 <= (add0(34)&add0) + (add1(34)&add1);
			add17 <= (add2(34)&add2) + (add3(34)&add3);
			add18 <= (add4(34)&add4) + (add5(34)&add5);
			add19 <= (add6(34)&add6) + (add7(34)&add7);
			add20 <= (add8(34)&add8) + (add9(34)&add9);
			add21 <= (add10(34)&add10) + (add11(34)&add11);
			add22 <= (add12(34)&add12) + (add13(34)&add13);
			add23 <= (add14(34)&add14) + (add15(34)&add15);
			
			add24 <= (add16(35)&add16) + (add17(35)&add17);
			add25 <= (add18(35)&add18) + (add19(35)&add19);
			add26 <= (add20(35)&add20) + (add21(35)&add21);
			add27 <= (add22(35)&add22) + (add23(35)&add23);
			
			add28 <= (add24(36)&add24) + (add25(36)&add25);
			add29 <= (add26(36)&add26) + (add27(36)&add27);
			
			add30 <= (add28(37)&add28) + (add29(37)&add29);
	
			
		end if;
	end process pline_proc;
	addbuff <= add30;
end architecture rtl;