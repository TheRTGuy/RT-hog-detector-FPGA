library IEEE;

use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity addr_distr is
generic
(
	IMG_WIDTH_PIXELS : natural := 320;
	IMG_HEIGHT_PIXELS : natural := 240;
	WIND_WIDTH_PIXELS : natural := 64;
	WIND_HEIGHT_PIXELS : natural := 128;
	-- m
	DP_AMOUNT_WIDTH : natural := 3;
	DP_AMOUNT_HEIGHT : natural := 1;
	-- p
	DP_INDEX_WIDTH : natural := 0;
	DP_INDEX_HEIGHT : natural := 0
);
port
(
	en : in std_logic := '0';
	sync_in : in std_logic;
	mem_we : out std_logic;
	ready : out std_logic;
	addr_out : out std_logic_vector(31 downto 0);
	block_modulo_width : out std_logic_vector(31 downto 0);
	block_modulo_height : out std_logic_vector(31 downto 0);
	coef_addr_out : out std_logic_vector(31 downto 0);
	
	clk : in std_logic := '0';
	reset : in std_logic := '0'
);
end entity addr_distr;

architecture rtl of addr_distr is
	function adj_qlen( constant len : natural; constant idx : natural ) 
	return natural is
	begin
		if idx = 0 then
			return len;
		end if;
		return len + 1;
	end function;
	function calc_amt ( constant len : natural;
			    constant max_wind : natural; 
			    k : natural; 
			    constant adj : natural) 
			    return natural is
		variable v0 : natural;
		variable v1 : natural;
	begin
		v0 := len - k;
		v1 := k + (1 - adj);
		if v0 >= max_wind then
			if v1 < max_wind then
				return v1;
			end if;
			return max_wind;
		end if;
		return v0;
	end function;
	
	function minwind(constant max_wind : natural;
	                 constant wind_blocks : natural) 
	return natural is
	begin
		if max_wind > wind_blocks then
			return wind_blocks;
		else
			return max_wind;
		end if;
	end function;
	
	function calc_max_wind( constant wind_len : natural; 
				constant dp_amt : natural ) 
	return natural is
	begin
		if (wind_len mod dp_amt) = 0 then
			return wind_len / dp_amt;
		end if;
		return ( wind_len / dp_amt ) + 1;
	end function;
	
	function calc_wnd_amt_quantized( constant wind_amt_tot : natural;
					  constant img_len_blocks : natural;
					  constant wind_len_blocks : natural;
					  constant dp_index : natural;
					  constant dp_amt : natural)
					  return natural is
		variable b : natural := ((wind_amt_tot-1)/8) mod dp_amt;
		variable c : natural := (wind_amt_tot-1) mod 8;
		variable amt : natural := ((img_len_blocks-wind_len_blocks + (dp_amt-1) - dp_index)/dp_amt)*8;
	begin
		if b = dp_index then
			return amt+c+1;
		else
			return amt;
		end if;
	end function;
					  
	
	type wind_len_quantized_t is array(natural range <>) of natural;
	
	function init_wind_width_q_arr(constant wind_len_blocks : natural;
					constant dp_amt : natural;
					constant dp_idx : natural)
		return wind_len_quantized_t is
		
		variable wind_len_arr : wind_len_quantized_t(dp_amt-1 downto 0);
	begin
		for i in 0 to dp_amt-1 loop
		        if i >= dp_idx then
				wind_len_arr(i) := (wind_len_blocks + dp_amt - 1 - i + dp_idx)/dp_amt;
			else
				wind_len_arr(i) := (wind_len_blocks - 1 + dp_idx - i)/dp_amt;
			end if;
-- 			wind_len_arr(i) := (wind_len_blocks + dp_amt - 1 - i)/dp_amt;
		end loop;
		return wind_len_arr;
	end function;
	
	

	constant BLOCK_COL_CTR_INC_VAL : natural := (DP_AMOUNT_WIDTH - 1 + DP_INDEX_WIDTH) mod DP_AMOUNT_WIDTH;
	constant BLOCK_ROW_CTR_INC_VAL : natural := (DP_AMOUNT_HEIGHT - 1 + DP_INDEX_HEIGHT) mod DP_AMOUNT_HEIGHT;
	
	-- n
	constant IMG_WIDTH_BLOCKS : natural := (IMG_WIDTH_PIXELS/8) - 1;
	constant IMG_HEIGHT_BLOCKS : natural := (IMG_HEIGHT_PIXELS/8) - 1;
	-- y
	constant WIND_WIDTH_BLOCKS : natural := (WIND_WIDTH_PIXELS/8) - 1;
	constant WIND_HEIGHT_BLOCKS : natural := (WIND_HEIGHT_PIXELS/8) - 1;
	
	constant WIND_WIDTH_BLOCKS_QUANTIZED : wind_len_quantized_t(DP_AMOUNT_WIDTH-1 downto 0) := init_wind_width_q_arr(WIND_WIDTH_BLOCKS,DP_AMOUNT_WIDTH,DP_INDEX_WIDTH);
	
	-- w
	constant IMG_WIDTH_BLOCKS_ADJ : natural := ((IMG_WIDTH_BLOCKS-DP_INDEX_WIDTH-WIND_WIDTH_BLOCKS)/DP_AMOUNT_WIDTH +
	                                              (WIND_WIDTH_BLOCKS-1)/DP_AMOUNT_WIDTH + adj_qlen(1,DP_INDEX_WIDTH));
	constant IMG_HEIGHT_BLOCKS_ADJ : natural := ((IMG_HEIGHT_BLOCKS-DP_INDEX_HEIGHT-WIND_HEIGHT_BLOCKS)/DP_AMOUNT_HEIGHT +
	                                              (WIND_HEIGHT_BLOCKS-1)/DP_AMOUNT_HEIGHT + adj_qlen(1,DP_INDEX_HEIGHT));
	
	constant IMG_WIDTH_BLOCKS_QUANTIZED : 
		natural := adj_qlen((IMG_WIDTH_BLOCKS-1-DP_INDEX_WIDTH)/DP_AMOUNT_WIDTH,DP_INDEX_WIDTH);
	constant IMG_HEIGHT_BLOCKS_QUANTIZED : 
		natural := adj_qlen((IMG_HEIGHT_BLOCKS-1-DP_INDEX_HEIGHT)/DP_AMOUNT_HEIGHT,DP_INDEX_HEIGHT);
		
	constant WIND_AMT_COL : natural := IMG_WIDTH_PIXELS - WIND_WIDTH_PIXELS + 1;
	constant WIND_AMT_ROW : natural := IMG_HEIGHT_PIXELS - WIND_HEIGHT_PIXELS + 1;
	constant WIND_AMT_TOT : natural := WIND_AMT_ROW*WIND_AMT_COL;
	
-- 	constant WIND_AMT_COL_QUANTIZED : natural := (IMG_WIDTH_PIXELS - WIND_WIDTH_PIXELS - DP_INDEX_WIDTH)/DP_AMOUNT_WIDTH + 1;
-- 	constant WIND_AMT_ROW_QUANTIZED : natural := (IMG_HEIGHT_PIXELS - WIND_HEIGHT_PIXELS - DP_INDEX_HEIGHT)/DP_AMOUNT_HEIGHT + 1;
-- 	constant WIND_AMT_TOT_QUANTIZED : natural := WIND_AMT_ROW_QUANTIZED*WIND_AMT_COL_QUANTIZED;
	constant WIND_AMT_COL_QUANTIZED : natural := calc_wnd_amt_quantized(WIND_AMT_COL,IMG_WIDTH_BLOCKS,WIND_WIDTH_BLOCKS,DP_INDEX_WIDTH,DP_AMOUNT_WIDTH);
	constant WIND_AMT_ROW_QUANTIZED : natural := calc_wnd_amt_quantized(WIND_AMT_ROW,IMG_HEIGHT_BLOCKS,WIND_HEIGHT_BLOCKS,DP_INDEX_HEIGHT,DP_AMOUNT_HEIGHT);
	constant WIND_AMT_TOT_QUANTIZED : natural := WIND_AMT_ROW_QUANTIZED*WIND_AMT_COL_QUANTIZED;
	
	constant WIND_AMT_COL_BLOCKS : natural := IMG_WIDTH_BLOCKS - WIND_WIDTH_BLOCKS + 1;
	constant WIND_AMT_ROW_BLOCKS : natural := IMG_HEIGHT_BLOCKS - WIND_HEIGHT_BLOCKS + 1;
	constant WIND_AMT_TOT_BLOCKS : natural := WIND_AMT_COL_BLOCKS*WIND_AMT_ROW_BLOCKS;
	
	constant WIND_AMT_COL_BLOCKS_QUANTIZED : natural := (IMG_WIDTH_BLOCKS - WIND_WIDTH_BLOCKS - DP_INDEX_WIDTH)/DP_AMOUNT_WIDTH + 1;
	constant WIND_AMT_ROW_BLOCKS_QUANTIZED : natural := (IMG_HEIGHT_BLOCKS - WIND_HEIGHT_BLOCKS - DP_INDEX_HEIGHT)/DP_AMOUNT_HEIGHT + 1;
	constant WIND_AMT_TOT_BLOCKS_QUANTIZED : natural := WIND_AMT_COL_BLOCKS_QUANTIZED*WIND_AMT_ROW_BLOCKS_QUANTIZED;
	
-- 	constant WIND_REMAINDER_WIDTH : natural := IMG_WIDTH_PIXELS - (IMG_WIDTH_BLOCKS+1)*8;
-- 	constant WIND_REMAINDER_HEIGHT : natural := IMG_HEIGHT_PIXELS - (IMG_HEIGHT_BLOCKS+1)*8;
	constant WIND_REMAINDER_WIDTH : natural := (WIND_AMT_COL_QUANTIZED-1) mod 8;
	constant WIND_REMAINDER_HEIGHT : natural := (WIND_AMT_ROW_QUANTIZED-1) mod 8;
	constant BLOCK_REMAINDER_WIDTH : natural := (IMG_WIDTH_PIXELS - 16) mod 8;
	constant BLOCK_REMAINDER_HEIGHT : natural := (IMG_HEIGHT_PIXELS - 16) mod 8;
	
	-- l
	constant MAX_WINDOWS_PER_DP_COL : natural := calc_max_wind(WIND_WIDTH_BLOCKS,DP_AMOUNT_WIDTH);
	constant MAX_WINDOWS_PER_DP_ROW : natural := calc_max_wind(WIND_HEIGHT_BLOCKS,DP_AMOUNT_HEIGHT);
	
	-- k
	signal block_col_ctr : natural range 0 to IMG_WIDTH_BLOCKS_QUANTIZED := 0;
	signal block_row_ctr : natural range 0 to IMG_HEIGHT_BLOCKS_QUANTIZED := 0;
	
	-- c
	signal block_col_ctr_modulo : natural range 0 to DP_AMOUNT_WIDTH-1 := 0;
	signal block_col_ctr_modulo_adj : natural range 0 to DP_AMOUNT_WIDTH-1;
	signal block_col_ctr_modulo_nxt : natural range 0 to DP_AMOUNT_WIDTH-1;
	signal block_row_ctr_modulo : natural range 0 to DP_AMOUNT_HEIGHT-1 := 0;
	signal block_row_ctr_modulo_adj : natural range 0 to DP_AMOUNT_HEIGHT-1;
	signal block_row_ctr_modulo_nxt : natural range 0 to DP_AMOUNT_HEIGHT-1;
	
	type coef_base_t is array(natural range <>) of natural range 0 to WIND_WIDTH_BLOCKS*WIND_HEIGHT_BLOCKS;
	signal coef_addr_ctr : natural range 0 to WIND_WIDTH_BLOCKS*WIND_HEIGHT_BLOCKS := 0;
	signal coef_addr_ctr_nxt : natural range 0 to WIND_WIDTH_BLOCKS*WIND_HEIGHT_BLOCKS;
-- 	signal coef_addr_ctr_base : natural range 0 to WIND_WIDTH_BLOCKS*WIND_HEIGHT_BLOCKS := 0;
-- 	signal coef_addr_ctr_base_nxt : natural range 0 to WIND_WIDTH_BLOCKS*WIND_HEIGHT_BLOCKS;
	signal coef_addr_ctr_base : coef_base_t(DP_AMOUNT_WIDTH-1 downto 0) := (others => 0);
	signal coef_addr_ctr_base_nxt : coef_base_t(DP_AMOUNT_WIDTH-1 downto 0);
	signal coef_addr_iterator : natural range 0 to WIND_WIDTH_BLOCKS*WIND_HEIGHT_BLOCKS := 0;
	signal coef_addr_ofs : natural range 0 to WIND_WIDTH_BLOCKS*WIND_HEIGHT_BLOCKS := 0;
	
	signal wind_width_curr : natural range 0 to WIND_WIDTH_BLOCKS;
	
	
	signal wind_addr_ctr : natural range 0 to WIND_AMT_TOT_QUANTIZED-1 := 0;
	signal wind_addr_ctr_nxt : natural range 0 to WIND_AMT_TOT_QUANTIZED-1;
	signal wind_addr_iterator : natural range 0 to WIND_AMT_TOT_QUANTIZED-1 := 0; 
	signal wind_addr_ofs : natural range 0 to  WIND_AMT_TOT_QUANTIZED-1 := 0;
	
	signal wind_block_amt_row : natural range 0 to MAX_WINDOWS_PER_DP_ROW;
	signal wind_block_amt_row_ofs : natural range 0 to 1;
	signal wind_block_amt_row_adj : integer range -1 to MAX_WINDOWS_PER_DP_ROW; -- TODO -> change back to natural.. and prob pipe-line this shit
	
	signal wind_block_amt_col : natural range 0 to MAX_WINDOWS_PER_DP_COL;
	signal wind_block_amt_col_ofs : natural range 0 to 1;
	signal wind_block_amt_col_adj : integer range -1 to MAX_WINDOWS_PER_DP_COL; -- TODO -> change back to natural.. and prob pipe-line this shit
	
	
	signal wind_block_ctr_row : natural range 0 to MAX_WINDOWS_PER_DP_ROW-1 := 0;
	signal wind_block_ctr_col : natural range 0 to MAX_WINDOWS_PER_DP_COL-1 := 0;
	signal wind_block_ctr_row_init : natural range 0 to 1;
	signal wind_block_ctr_col_init : natural range 0 to 1;
	
	
	
	signal pix_ctr_col : natural range 0 to 7 := 0;
	signal pix_ctr_row : natural range 0 to 7 := 0;
	signal pix_ctr_col_end : natural range 0 to 7;
	signal pix_ctr_row_end : natural range 0 to 7;
	
	signal halt : std_logic := '0';
	signal block_distributed : boolean;
	signal wind_block_row_empty : boolean;
	signal wind_block_col_empty : boolean;
	signal wind_block_row_complete : boolean;
	signal wind_block_col_complete : boolean;
	signal img_width_complete : boolean;
begin
	block_modulo_width <= std_logic_vector(to_unsigned(block_col_ctr_modulo_adj,32));
	block_modulo_height <= std_logic_vector(to_unsigned(block_row_ctr_modulo_adj,32));
	mem_we <= '0' when (wind_block_col_empty or wind_block_row_empty) else (en and not halt);
	ready <= '1' when block_distributed else (not en);
			  
	
	img_width_complete <= block_col_ctr = IMG_WIDTH_BLOCKS_QUANTIZED and block_col_ctr_modulo = ((IMG_WIDTH_BLOCKS-1) mod DP_AMOUNT_WIDTH);
	wind_block_col_empty <= wind_block_amt_col_adj = 0 or (wind_block_amt_col_adj = wind_block_ctr_col_init and wind_block_ctr_col_init = 1);
	wind_block_row_empty <= wind_block_amt_row_adj = 0 or (wind_block_amt_row_adj = wind_block_ctr_row_init and wind_block_ctr_row_init = 1);
	wind_block_col_complete <= wind_block_ctr_col = (wind_block_amt_col_adj-1-wind_block_ctr_col_init);
	wind_block_row_complete <= wind_block_ctr_row = (wind_block_amt_row_adj-1-wind_block_ctr_row_init);
	block_distributed <= wind_block_col_empty or wind_block_row_empty or (wind_block_col_complete and wind_block_row_complete);
		
	wind_width_curr <= WIND_WIDTH_BLOCKS_QUANTIZED(block_col_ctr_modulo);
		
	g0 : if DP_INDEX_WIDTH /= 0 generate
		block_col_ctr_modulo_adj <= block_col_ctr_modulo - DP_INDEX_WIDTH when block_col_ctr_modulo >= DP_INDEX_WIDTH 
		                               else (DP_AMOUNT_WIDTH-DP_INDEX_WIDTH) + block_col_ctr_modulo;
		wind_block_amt_col <= calc_amt(IMG_WIDTH_BLOCKS_ADJ,
						minwind(MAX_WINDOWS_PER_DP_COL,WIND_AMT_COL_BLOCKS_QUANTIZED),
						block_col_ctr,
						1);
	end generate;
	g1 : if DP_INDEX_WIDTH = 0 generate
		block_col_ctr_modulo_adj <= block_col_ctr_modulo;
		wind_block_amt_col <= calc_amt(IMG_WIDTH_BLOCKS_ADJ, 
						minwind(MAX_WINDOWS_PER_DP_COL,WIND_AMT_COL_BLOCKS_QUANTIZED), 
						block_col_ctr,
						0);
	end generate;
	
	h0 : if DP_INDEX_HEIGHT /= 0 generate
		block_row_ctr_modulo_adj <= block_row_ctr_modulo - DP_INDEX_HEIGHT when block_row_ctr_modulo >= DP_INDEX_HEIGHT
		                               else (DP_AMOUNT_HEIGHT-DP_INDEX_HEIGHT) + block_row_ctr_modulo;
		wind_block_amt_row <= calc_amt(IMG_HEIGHT_BLOCKS_ADJ,
						minwind(MAX_WINDOWS_PER_DP_ROW,WIND_AMT_ROW_BLOCKS_QUANTIZED),
						block_row_ctr,
						1);
	end generate;
	h1 : if DP_INDEX_HEIGHT = 0 generate
		block_row_ctr_modulo_adj <= block_row_ctr_modulo;
		wind_block_amt_row <= calc_amt(IMG_HEIGHT_BLOCKS_ADJ, 
						minwind(MAX_WINDOWS_PER_DP_ROW,WIND_AMT_ROW_BLOCKS_QUANTIZED), 
						block_row_ctr,
						0);
	end generate;
	
	
	-- We don't need to adjust the amount if the window width is perfect multiple of the amount of dot products
	g2 : if WIND_WIDTH_BLOCKS mod DP_AMOUNT_WIDTH = 0 generate
		wind_block_amt_col_adj <= wind_block_amt_col;
	end generate;
	
	g3 : if WIND_WIDTH_BLOCKS mod DP_AMOUNT_WIDTH /= 0 generate
		g4 : if DP_INDEX_WIDTH = 0 generate 
			wind_block_amt_col_adj <= wind_block_amt_col - wind_block_amt_col_ofs when block_col_ctr >= MAX_WINDOWS_PER_DP_COL - 1 
				else wind_block_amt_col;
		end generate;
		g5 : if DP_INDEX_WIDTH /= 0 generate
			wind_block_amt_col_adj <= wind_block_amt_col - wind_block_amt_col_ofs when block_col_ctr >= MAX_WINDOWS_PER_DP_COL 
				else wind_block_amt_col;
		end generate;
		wind_block_amt_col_ofs <= 1 when block_col_ctr_modulo_adj >= (WIND_WIDTH_BLOCKS mod DP_AMOUNT_WIDTH) else 0;
	end generate;
	
	h2 : if WIND_HEIGHT_BLOCKS mod DP_AMOUNT_HEIGHT = 0 generate
		wind_block_amt_row_adj <= wind_block_amt_row;
	end generate;
	
	h3 : if WIND_HEIGHT_BLOCKS mod DP_AMOUNT_HEIGHT /= 0 generate
		h4 : if DP_INDEX_HEIGHT = 0 generate 
			wind_block_amt_row_adj <= wind_block_amt_row - wind_block_amt_row_ofs when block_row_ctr >= MAX_WINDOWS_PER_DP_ROW - 1 
				else wind_block_amt_row;
		end generate;
		h5 : if DP_INDEX_HEIGHT /= 0 generate
			wind_block_amt_row_adj <= wind_block_amt_row - wind_block_amt_row_ofs when block_row_ctr >= MAX_WINDOWS_PER_DP_ROW 
				else wind_block_amt_row;
		end generate;
		wind_block_amt_row_ofs <= 1 when block_row_ctr_modulo_adj >= (WIND_HEIGHT_BLOCKS mod DP_AMOUNT_HEIGHT) else 0;
	end generate;
	
	
	wind_block_ctr_col_init <= 1 when (IMG_WIDTH_BLOCKS_ADJ - MAX_WINDOWS_PER_DP_COL <= block_col_ctr) and 
						     pix_ctr_col > WIND_REMAINDER_WIDTH else 0;
	wind_block_ctr_row_init <= 1 when (IMG_HEIGHT_BLOCKS_ADJ - MAX_WINDOWS_PER_DP_ROW <= block_row_ctr) and 
						     pix_ctr_row > WIND_REMAINDER_HEIGHT else 0;
	
	addr_adj : process(wind_block_ctr_col_init,
			   wind_addr_iterator,pix_ctr_col,
			   coef_addr_iterator,
			   wind_width_curr,
			   wind_block_ctr_row_init)
		variable addr_adj_col : natural range 0 to WIND_AMT_TOT_QUANTIZED-1;
		variable coef_addr : unsigned(31 downto 0);
	begin
		if wind_block_ctr_col_init = 1 then
			addr_adj_col := wind_addr_iterator - 8;
		else
			addr_adj_col := wind_addr_iterator;
		end if;
		coef_addr := to_unsigned(coef_addr_iterator,32) + 
			      to_unsigned(wind_block_ctr_col_init,32);
		addr_out <= std_logic_vector(to_unsigned(addr_adj_col,32) + to_unsigned(pix_ctr_col,32));
		
		
		if wind_block_ctr_row_init = 1 then
			coef_addr_out <= std_logic_vector(coef_addr + to_unsigned(wind_width_curr,32));
		else
			coef_addr_out <= std_logic_vector(coef_addr);
		end if;			 
	end process addr_adj;
	
	
				     
	window_block_counter_loop : process(clk)
	begin
		if rising_edge(clk) then
			if reset = '1' then
				wind_block_ctr_col <= 0;
				wind_block_ctr_row <= 0;
				wind_addr_iterator <= 0;
				coef_addr_iterator <= 0;
				wind_addr_ofs <= 0;
				coef_addr_ofs <= 0;
				halt <= '0';
			elsif en = '1' then
				if wind_block_col_complete or wind_block_col_empty then
					if wind_block_row_complete or wind_block_col_empty then
						if sync_in = '1' then
							halt <= '0';
							wind_block_ctr_col <= 0;
							wind_block_ctr_row <= 0;
							
							wind_addr_ofs <= 0;
							wind_addr_iterator <= wind_addr_ctr_nxt;
							if pix_ctr_col = pix_ctr_col_end then
								if img_width_complete then
									coef_addr_iterator <= coef_addr_ctr_nxt + coef_addr_ctr_base_nxt(0);
									coef_addr_ofs <= coef_addr_ctr_base_nxt(0);
								else
									coef_addr_iterator <= coef_addr_ctr_nxt + coef_addr_ctr_base_nxt(block_col_ctr_modulo_nxt);
									coef_addr_ofs <= coef_addr_ctr_base_nxt(block_col_ctr_modulo_nxt);
								end if;
							else
								coef_addr_iterator <= coef_addr_ctr_nxt + coef_addr_ctr_base_nxt(block_col_ctr_modulo);
								coef_addr_ofs <= coef_addr_ctr_base_nxt(block_col_ctr_modulo);
							end if;
						else
							halt <= '1';
						end if;
					else
						wind_block_ctr_col <= 0;
						wind_block_ctr_row <= wind_block_ctr_row + 1;
						
						coef_addr_iterator <= (coef_addr_ctr + coef_addr_ofs) + wind_width_curr;
						wind_addr_iterator <= (wind_addr_ctr - wind_addr_ofs) - WIND_AMT_COL_QUANTIZED*8;
						coef_addr_ofs <= coef_addr_ofs + wind_width_curr;
						wind_addr_ofs <= wind_addr_ofs + WIND_AMT_COL_QUANTIZED*8;
					end if;
				else
					wind_block_ctr_col <= wind_block_ctr_col + 1;
					coef_addr_iterator <= coef_addr_iterator + 1;
					wind_addr_iterator <= wind_addr_iterator - 8;
				end if;
			end if;
		end if;
	end process window_block_counter_loop;
	
	block_col_ctr_modulo_nxt <= 0 when block_col_ctr_modulo = DP_AMOUNT_WIDTH-1 else block_col_ctr_modulo + 1;
	block_row_ctr_modulo_nxt <= 0 when block_row_ctr_modulo = DP_AMOUNT_HEIGHT-1 else block_row_ctr_modulo + 1;
	
	pix_ctr_col_end <= BLOCK_REMAINDER_WIDTH when img_width_complete
			    else 7;
	pix_ctr_row_end <= BLOCK_REMAINDER_HEIGHT when block_row_ctr = IMG_HEIGHT_BLOCKS_QUANTIZED and block_row_ctr_modulo = ((IMG_HEIGHT_BLOCKS-1) mod DP_AMOUNT_HEIGHT)
			    else 7;
	block_loop : process(clk)
	begin
		if rising_edge(clk) then
			if reset = '1' then
				block_col_ctr_modulo <= 0;
				block_col_ctr <= 0;
				pix_ctr_col <= 0;
			elsif en = '1' and sync_in = '1' then
				if pix_ctr_col = pix_ctr_col_end then
					pix_ctr_col <= 0;
					if img_width_complete then
						block_col_ctr_modulo <= 0;
						block_col_ctr <= 0;
						if pix_ctr_row = pix_ctr_row_end then
							pix_ctr_row <= 0;
							if block_row_ctr = IMG_HEIGHT_BLOCKS_QUANTIZED and block_row_ctr_modulo = ((IMG_HEIGHT_BLOCKS-1) mod DP_AMOUNT_HEIGHT) then
								block_row_ctr_modulo <= 0;
								block_row_ctr <= 0;
							else
								block_row_ctr_modulo <= block_row_ctr_modulo_nxt;
								if block_row_ctr_modulo = BLOCK_ROW_CTR_INC_VAL then
									block_row_ctr <= block_row_ctr + 1;
								end if;
							end if;
						else
							pix_ctr_row <= pix_ctr_row + 1;
						end if;
					else
						block_col_ctr_modulo <= block_col_ctr_modulo_nxt;
						if block_col_ctr_modulo = BLOCK_COL_CTR_INC_VAL then
							block_col_ctr <= block_col_ctr + 1;
						end if;
					end if;
				else
					pix_ctr_col <= pix_ctr_col + 1;
				end if;
			end if;
		end if;
	end process block_loop;
	
	addr_next : process(wind_addr_ctr,coef_addr_ctr,
			    pix_ctr_col,pix_ctr_col_end,
			    block_col_ctr, block_col_ctr_modulo,
			    pix_ctr_row,pix_ctr_row_end,
			    block_row_ctr, block_row_ctr_modulo,
			    coef_addr_ctr_base,img_width_complete,
				 wind_block_amt_col_adj,wind_block_amt_row_adj
			    )
	begin
		if pix_ctr_col = pix_ctr_col_end then
			if img_width_complete then
				coef_addr_ctr_nxt <= 0;
-- 				
				if pix_ctr_row = pix_ctr_row_end then
					if block_row_ctr = IMG_HEIGHT_BLOCKS_QUANTIZED and block_row_ctr_modulo = ((IMG_HEIGHT_BLOCKS-1) mod DP_AMOUNT_HEIGHT) then
						coef_addr_ctr_base_nxt <= (others => 0);
					elsif IMG_HEIGHT_BLOCKS_ADJ - MAX_WINDOWS_PER_DP_ROW > block_row_ctr then
						coef_addr_ctr_base_nxt <= coef_addr_ctr_base;
					else
						if block_row_ctr_modulo = BLOCK_ROW_CTR_INC_VAL then
							for i in 0 to DP_AMOUNT_WIDTH-1 loop
								coef_addr_ctr_base_nxt(i) <= coef_addr_ctr_base(i) + WIND_WIDTH_BLOCKS_QUANTIZED(i);
							end loop;
						else
							coef_addr_ctr_base_nxt <= coef_addr_ctr_base;
						end if;
					end if;
				else 
					coef_addr_ctr_base_nxt <= coef_addr_ctr_base;
				end if;
				
				if  wind_addr_ctr < (WIND_AMT_TOT_QUANTIZED-1-WIND_REMAINDER_WIDTH) then
					if pix_ctr_row /= pix_ctr_row_end or block_row_ctr_modulo = BLOCK_ROW_CTR_INC_VAL then
						wind_addr_ctr_nxt <= wind_addr_ctr + (WIND_REMAINDER_WIDTH + 1);
					else
						wind_addr_ctr_nxt <= wind_addr_ctr - (WIND_AMT_COL_QUANTIZED*8 - (WIND_REMAINDER_WIDTH + 1));
					end if;
				else
					if pix_ctr_row = pix_ctr_row_end and
					   block_row_ctr = IMG_HEIGHT_BLOCKS_QUANTIZED and
					   block_row_ctr_modulo = ((IMG_HEIGHT_BLOCKS-1) mod DP_AMOUNT_HEIGHT)
					   then
						wind_addr_ctr_nxt <= 0;
					else
						wind_addr_ctr_nxt <= wind_addr_ctr - (WIND_AMT_COL_QUANTIZED*8 - (WIND_REMAINDER_WIDTH + 1));
					end if;
				end if;
			elsif wind_block_amt_col_adj > 0 and wind_block_amt_row_adj > 0 then
				coef_addr_ctr_base_nxt <= coef_addr_ctr_base;
				if IMG_WIDTH_BLOCKS_ADJ - MAX_WINDOWS_PER_DP_COL > block_col_ctr then
					if block_col_ctr_modulo = BLOCK_COL_CTR_INC_VAL then
						wind_addr_ctr_nxt <= wind_addr_ctr + 8;
					else
						wind_addr_ctr_nxt <= wind_addr_ctr;
					end if;
					coef_addr_ctr_nxt <= coef_addr_ctr;
				else
					wind_addr_ctr_nxt <= wind_addr_ctr;
					if block_col_ctr_modulo = BLOCK_COL_CTR_INC_VAL then
-- 						coef_addr_ctr_nxt <= coef_addr_ctr + DP_AMOUNT_WIDTH;
						coef_addr_ctr_nxt <= coef_addr_ctr + 1;
					else
						coef_addr_ctr_nxt <= coef_addr_ctr;
					end if;
				end if;
			else
				coef_addr_ctr_base_nxt <= coef_addr_ctr_base;
				wind_addr_ctr_nxt <= wind_addr_ctr;
				coef_addr_ctr_nxt <= coef_addr_ctr;
			end if;
		else
			coef_addr_ctr_base_nxt <= coef_addr_ctr_base;
			wind_addr_ctr_nxt <= wind_addr_ctr;
			coef_addr_ctr_nxt <= coef_addr_ctr;
		end if;
	end process addr_next;
	
	
	addr_proc : process(clk)
	begin
		if rising_edge(clk) then
			if reset = '1' then
				wind_addr_ctr <= 0;
				coef_addr_ctr <= 0;
				coef_addr_ctr_base <= (others => 0);
			elsif en = '1' and sync_in = '1'  then
				wind_addr_ctr <= wind_addr_ctr_nxt;
				coef_addr_ctr <= coef_addr_ctr_nxt;
				coef_addr_ctr_base <= coef_addr_ctr_base_nxt;
			end if;
		end if;
	end process addr_proc;
	
end architecture rtl;
