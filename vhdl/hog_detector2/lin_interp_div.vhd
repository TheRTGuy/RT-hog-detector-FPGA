library IEEE;

use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity lin_interp_div is
port (
	val_in : in std_logic_vector(16 downto 0);
	val_out : out std_logic_vector(17 downto 0);
	
	valid_in : in std_logic;
	valid_out : out std_logic;
	
	en : in std_logic := '0';
	clk : in std_logic := '0';
	reset : in std_logic := '0'
);
end entity lin_interp_div;

architecture rtl of lin_interp_div is
	signal idx : natural range 0 to 76;
	signal idx0 : natural range 0 to 76 := 0;
	signal idx1 : natural range 0 to 76 := 0;
	
	signal cmp : std_logic_vector(76 downto 0);
	
	signal add1 : unsigned(17 downto 0);
	signal x11285 : unsigned(17 downto 0);
	signal x195 : unsigned(17 downto 0);
	signal x91 : unsigned(17 downto 0);
	signal x25 : unsigned(17 downto 0);
	type xi_inv_t is array(76 downto 0) of signed(17 downto 0);
	constant xi_inv : xi_inv_t := (
		0 => to_signed(0,18),
		1 => to_signed(-1,18),
		2 => to_signed(-2,18),
		3 => to_signed(-3,18),
		4 => to_signed(-4,18),
		5 => to_signed(-5,18),
		6 => to_signed(-6,18),
		7 => to_signed(-7,18),
		8 => to_signed(-8,18),
		9 => to_signed(-9,18),
		10 => to_signed(-10,18),
		11 => to_signed(-20,18),
		12 => to_signed(-30,18),
		13 => to_signed(-40,18),
		14 => to_signed(-50,18),
		15 => to_signed(-60,18),
		16 => to_signed(-70,18),
		17 => to_signed(-80,18),
		18 => to_signed(-90,18),
		19 => to_signed(-100,18),
		20 => to_signed(-120,18),
		21 => to_signed(-140,18),
		22 => to_signed(-160,18),
		23 => to_signed(-180,18),
		24 => to_signed(-200,18),
		25 => to_signed(-220,18),
		26 => to_signed(-240,18),
		27 => to_signed(-260,18),
		28 => to_signed(-280,18),
		29 => to_signed(-300,18),
		30 => to_signed(-320,18),
		31 => to_signed(-340,18),
		32 => to_signed(-360,18),
		33 => to_signed(-380,18),
		34 => to_signed(-400,18),
		35 => to_signed(-500,18),
		36 => to_signed(-600,18),
		37 => to_signed(-700,18),
		38 => to_signed(-800,18),
		39 => to_signed(-900,18),
		40 => to_signed(-1000,18),
		41 => to_signed(-1200,18),
		42 => to_signed(-1400,18),
		43 => to_signed(-1600,18),
		44 => to_signed(-1800,18),
		45 => to_signed(-2000,18),
		46 => to_signed(-2200,18),
		47 => to_signed(-2400,18),
		48 => to_signed(-2600,18),
		49 => to_signed(-2800,18),
		50 => to_signed(-3000,18),
		51 => to_signed(-3200,18),
		52 => to_signed(-3400,18),
		53 => to_signed(-3600,18),
		54 => to_signed(-3800,18),
		55 => to_signed(-4000,18),
		56 => to_signed(-5000,18),
		57 => to_signed(-6000,18),
		58 => to_signed(-7000,18),
		59 => to_signed(-8000,18),
		60 => to_signed(-9000,18),
		61 => to_signed(-10000,18),
		62 => to_signed(-15000,18),
		63 => to_signed(-20000,18),
		64 => to_signed(-25000,18),
		65 => to_signed(-30000,18),
		66 => to_signed(-35000,18),
		67 => to_signed(-40000,18),
		68 => to_signed(-50000,18),
		69 => to_signed(-60000,18),
		70 => to_signed(-70000,18),
		71 => to_signed(-80000,18),
		72 => to_signed(-90000,18),
		73 => to_signed(-100000,18),
		74 => to_signed(-110000,18),
		75 => to_signed(-120000,18),
		76 => to_signed(-130000,18)
	);
	type yi_t is array(76 downto 0) of unsigned(17 downto 0);
	constant yi : yi_t := (
		0 => to_unsigned(65536,18),
		1 => to_unsigned(32768,18),
		2 => to_unsigned(21845,18),
		3 => to_unsigned(16384,18),
		4 => to_unsigned(13107,18),
		5 => to_unsigned(10922,18),
		6 => to_unsigned(9362,18),
		7 => to_unsigned(8192,18),
		8 => to_unsigned(7281,18),
		9 => to_unsigned(6553,18),
		10 => to_unsigned(5957,18),
		11 => to_unsigned(3120,18),
		12 => to_unsigned(2114,18),
		13 => to_unsigned(1598,18),
		14 => to_unsigned(1285,18),
		15 => to_unsigned(1074,18),
		16 => to_unsigned(923,18),
		17 => to_unsigned(809,18),
		18 => to_unsigned(720,18),
		19 => to_unsigned(648,18),
		20 => to_unsigned(541,18),
		21 => to_unsigned(464,18),
		22 => to_unsigned(407,18),
		23 => to_unsigned(362,18),
		24 => to_unsigned(326,18),
		25 => to_unsigned(296,18),
		26 => to_unsigned(271,18),
		27 => to_unsigned(251,18),
		28 => to_unsigned(233,18),
		29 => to_unsigned(217,18),
		30 => to_unsigned(204,18),
		31 => to_unsigned(192,18),
		32 => to_unsigned(181,18),
		33 => to_unsigned(172,18),
		34 => to_unsigned(163,18),
		35 => to_unsigned(130,18),
		36 => to_unsigned(109,18),
		37 => to_unsigned(93,18),
		38 => to_unsigned(81,18),
		39 => to_unsigned(72,18),
		40 => to_unsigned(65,18),
		41 => to_unsigned(54,18),
		42 => to_unsigned(46,18),
		43 => to_unsigned(40,18),
		44 => to_unsigned(36,18),
		45 => to_unsigned(32,18),
		46 => to_unsigned(29,18),
		47 => to_unsigned(27,18),
		48 => to_unsigned(25,18),
		49 => to_unsigned(23,18),
		50 => to_unsigned(21,18),
		51 => to_unsigned(20,18),
		52 => to_unsigned(19,18),
		53 => to_unsigned(18,18),
		54 => to_unsigned(17,18),
		55 => to_unsigned(16,18),
		56 => to_unsigned(13,18),
		57 => to_unsigned(10,18),
		58 => to_unsigned(9,18),
		59 => to_unsigned(8,18),
		60 => to_unsigned(7,18),
		61 => to_unsigned(6,18),
		62 => to_unsigned(4,18),
		63 => to_unsigned(3,18),
		64 => to_unsigned(2,18),
		65 => to_unsigned(2,18),
		66 => to_unsigned(1,18),
		67 => to_unsigned(1,18),
		68 => to_unsigned(1,18),
		69 => to_unsigned(1,18),
		70 => to_unsigned(0,18),
		71 => to_unsigned(0,18),
		72 => to_unsigned(0,18),
		73 => to_unsigned(0,18),
		74 => to_unsigned(0,18),
		75 => to_unsigned(0,18),
		76 => to_unsigned(0,18)
	);
	
	signal val_in0 : std_logic_vector(16 downto 0) := (others => '0');
	signal add0 : unsigned(17 downto 0) := (others => '0');
	signal valid : std_logic_vector(2 downto 0) := (others => '0');
	signal yi_q : unsigned(17 downto 0) := (others => '0');
	signal df_mult_q : unsigned(17 downto 0) := (others => '0');
	type mult_arr is array(26 downto 0) of unsigned(17 downto 0);
	signal df_mult : mult_arr;
begin
	cmp(0) <= '1' when unsigned(val_in) < to_unsigned(1,18) else '0';
	cmp(1) <= '1' when unsigned(val_in) < to_unsigned(2,18) else '0';
	cmp(2) <= '1' when unsigned(val_in) < to_unsigned(3,18) else '0';
	cmp(3) <= '1' when unsigned(val_in) < to_unsigned(4,18) else '0';
	cmp(4) <= '1' when unsigned(val_in) < to_unsigned(5,18) else '0';
	cmp(5) <= '1' when unsigned(val_in) < to_unsigned(6,18) else '0';
	cmp(6) <= '1' when unsigned(val_in) < to_unsigned(7,18) else '0';
	cmp(7) <= '1' when unsigned(val_in) < to_unsigned(8,18) else '0';
	cmp(8) <= '1' when unsigned(val_in) < to_unsigned(9,18) else '0';
	cmp(9) <= '1' when unsigned(val_in) < to_unsigned(10,18) else '0';
	cmp(10) <= '1' when unsigned(val_in) < to_unsigned(20,18) else '0';
	cmp(11) <= '1' when unsigned(val_in) < to_unsigned(30,18) else '0';
	cmp(12) <= '1' when unsigned(val_in) < to_unsigned(40,18) else '0';
	cmp(13) <= '1' when unsigned(val_in) < to_unsigned(50,18) else '0';
	cmp(14) <= '1' when unsigned(val_in) < to_unsigned(60,18) else '0';
	cmp(15) <= '1' when unsigned(val_in) < to_unsigned(70,18) else '0';
	cmp(16) <= '1' when unsigned(val_in) < to_unsigned(80,18) else '0';
	cmp(17) <= '1' when unsigned(val_in) < to_unsigned(90,18) else '0';
	cmp(18) <= '1' when unsigned(val_in) < to_unsigned(100,18) else '0';
	cmp(19) <= '1' when unsigned(val_in) < to_unsigned(120,18) else '0';
	cmp(20) <= '1' when unsigned(val_in) < to_unsigned(140,18) else '0';
	cmp(21) <= '1' when unsigned(val_in) < to_unsigned(160,18) else '0';
	cmp(22) <= '1' when unsigned(val_in) < to_unsigned(180,18) else '0';
	cmp(23) <= '1' when unsigned(val_in) < to_unsigned(200,18) else '0';
	cmp(24) <= '1' when unsigned(val_in) < to_unsigned(220,18) else '0';
	cmp(25) <= '1' when unsigned(val_in) < to_unsigned(240,18) else '0';
	cmp(26) <= '1' when unsigned(val_in) < to_unsigned(260,18) else '0';
	cmp(27) <= '1' when unsigned(val_in) < to_unsigned(280,18) else '0';
	cmp(28) <= '1' when unsigned(val_in) < to_unsigned(300,18) else '0';
	cmp(29) <= '1' when unsigned(val_in) < to_unsigned(320,18) else '0';
	cmp(30) <= '1' when unsigned(val_in) < to_unsigned(340,18) else '0';
	cmp(31) <= '1' when unsigned(val_in) < to_unsigned(360,18) else '0';
	cmp(32) <= '1' when unsigned(val_in) < to_unsigned(380,18) else '0';
	cmp(33) <= '1' when unsigned(val_in) < to_unsigned(400,18) else '0';
	cmp(34) <= '1' when unsigned(val_in) < to_unsigned(500,18) else '0';
	cmp(35) <= '1' when unsigned(val_in) < to_unsigned(600,18) else '0';
	cmp(36) <= '1' when unsigned(val_in) < to_unsigned(700,18) else '0';
	cmp(37) <= '1' when unsigned(val_in) < to_unsigned(800,18) else '0';
	cmp(38) <= '1' when unsigned(val_in) < to_unsigned(900,18) else '0';
	cmp(39) <= '1' when unsigned(val_in) < to_unsigned(1000,18) else '0';
	cmp(40) <= '1' when unsigned(val_in) < to_unsigned(1200,18) else '0';
	cmp(41) <= '1' when unsigned(val_in) < to_unsigned(1400,18) else '0';
	cmp(42) <= '1' when unsigned(val_in) < to_unsigned(1600,18) else '0';
	cmp(43) <= '1' when unsigned(val_in) < to_unsigned(1800,18) else '0';
	cmp(44) <= '1' when unsigned(val_in) < to_unsigned(2000,18) else '0';
	cmp(45) <= '1' when unsigned(val_in) < to_unsigned(2200,18) else '0';
	cmp(46) <= '1' when unsigned(val_in) < to_unsigned(2400,18) else '0';
	cmp(47) <= '1' when unsigned(val_in) < to_unsigned(2600,18) else '0';
	cmp(48) <= '1' when unsigned(val_in) < to_unsigned(2800,18) else '0';
	cmp(49) <= '1' when unsigned(val_in) < to_unsigned(3000,18) else '0';
	cmp(50) <= '1' when unsigned(val_in) < to_unsigned(3200,18) else '0';
	cmp(51) <= '1' when unsigned(val_in) < to_unsigned(3400,18) else '0';
	cmp(52) <= '1' when unsigned(val_in) < to_unsigned(3600,18) else '0';
	cmp(53) <= '1' when unsigned(val_in) < to_unsigned(3800,18) else '0';
	cmp(54) <= '1' when unsigned(val_in) < to_unsigned(4000,18) else '0';
	cmp(55) <= '1' when unsigned(val_in) < to_unsigned(5000,18) else '0';
	cmp(56) <= '1' when unsigned(val_in) < to_unsigned(6000,18) else '0';
	cmp(57) <= '1' when unsigned(val_in) < to_unsigned(7000,18) else '0';
	cmp(58) <= '1' when unsigned(val_in) < to_unsigned(8000,18) else '0';
	cmp(59) <= '1' when unsigned(val_in) < to_unsigned(9000,18) else '0';
	cmp(60) <= '1' when unsigned(val_in) < to_unsigned(10000,18) else '0';
	cmp(61) <= '1' when unsigned(val_in) < to_unsigned(15000,18) else '0';
	cmp(62) <= '1' when unsigned(val_in) < to_unsigned(20000,18) else '0';
	cmp(63) <= '1' when unsigned(val_in) < to_unsigned(25000,18) else '0';
	cmp(64) <= '1' when unsigned(val_in) < to_unsigned(30000,18) else '0';
	cmp(65) <= '1' when unsigned(val_in) < to_unsigned(35000,18) else '0';
	cmp(66) <= '1' when unsigned(val_in) < to_unsigned(40000,18) else '0';
	cmp(67) <= '1' when unsigned(val_in) < to_unsigned(50000,18) else '0';
	cmp(68) <= '1' when unsigned(val_in) < to_unsigned(60000,18) else '0';
	cmp(69) <= '1' when unsigned(val_in) < to_unsigned(70000,18) else '0';
	cmp(70) <= '1' when unsigned(val_in) < to_unsigned(80000,18) else '0';
	cmp(71) <= '1' when unsigned(val_in) < to_unsigned(90000,18) else '0';
	cmp(72) <= '1' when unsigned(val_in) < to_unsigned(100000,18) else '0';
	cmp(73) <= '1' when unsigned(val_in) < to_unsigned(110000,18) else '0';
	cmp(74) <= '1' when unsigned(val_in) < to_unsigned(120000,18) else '0';
	cmp(75) <= '1' when unsigned(val_in) < to_unsigned(130000,18) else '0';
	cmp(76) <= '1' when unsigned(val_in) < to_unsigned(140000,18) else '0';

	with cmp select idx <=
		0 when "11111111111111111111111111111111111111111111111111111111111111111111111111111",
		1 when "11111111111111111111111111111111111111111111111111111111111111111111111111110",
		2 when "11111111111111111111111111111111111111111111111111111111111111111111111111100",
		3 when "11111111111111111111111111111111111111111111111111111111111111111111111111000",
		4 when "11111111111111111111111111111111111111111111111111111111111111111111111110000",
		5 when "11111111111111111111111111111111111111111111111111111111111111111111111100000",
		6 when "11111111111111111111111111111111111111111111111111111111111111111111111000000",
		7 when "11111111111111111111111111111111111111111111111111111111111111111111110000000",
		8 when "11111111111111111111111111111111111111111111111111111111111111111111100000000",
		9 when "11111111111111111111111111111111111111111111111111111111111111111111000000000",
		10 when "11111111111111111111111111111111111111111111111111111111111111111110000000000",
		11 when "11111111111111111111111111111111111111111111111111111111111111111100000000000",
		12 when "11111111111111111111111111111111111111111111111111111111111111111000000000000",
		13 when "11111111111111111111111111111111111111111111111111111111111111110000000000000",
		14 when "11111111111111111111111111111111111111111111111111111111111111100000000000000",
		15 when "11111111111111111111111111111111111111111111111111111111111111000000000000000",
		16 when "11111111111111111111111111111111111111111111111111111111111110000000000000000",
		17 when "11111111111111111111111111111111111111111111111111111111111100000000000000000",
		18 when "11111111111111111111111111111111111111111111111111111111111000000000000000000",
		19 when "11111111111111111111111111111111111111111111111111111111110000000000000000000",
		20 when "11111111111111111111111111111111111111111111111111111111100000000000000000000",
		21 when "11111111111111111111111111111111111111111111111111111111000000000000000000000",
		22 when "11111111111111111111111111111111111111111111111111111110000000000000000000000",
		23 when "11111111111111111111111111111111111111111111111111111100000000000000000000000",
		24 when "11111111111111111111111111111111111111111111111111111000000000000000000000000",
		25 when "11111111111111111111111111111111111111111111111111110000000000000000000000000",
		26 when "11111111111111111111111111111111111111111111111111100000000000000000000000000",
		27 when "11111111111111111111111111111111111111111111111111000000000000000000000000000",
		28 when "11111111111111111111111111111111111111111111111110000000000000000000000000000",
		29 when "11111111111111111111111111111111111111111111111100000000000000000000000000000",
		30 when "11111111111111111111111111111111111111111111111000000000000000000000000000000",
		31 when "11111111111111111111111111111111111111111111110000000000000000000000000000000",
		32 when "11111111111111111111111111111111111111111111100000000000000000000000000000000",
		33 when "11111111111111111111111111111111111111111111000000000000000000000000000000000",
		34 when "11111111111111111111111111111111111111111110000000000000000000000000000000000",
		35 when "11111111111111111111111111111111111111111100000000000000000000000000000000000",
		36 when "11111111111111111111111111111111111111111000000000000000000000000000000000000",
		37 when "11111111111111111111111111111111111111110000000000000000000000000000000000000",
		38 when "11111111111111111111111111111111111111100000000000000000000000000000000000000",
		39 when "11111111111111111111111111111111111111000000000000000000000000000000000000000",
		40 when "11111111111111111111111111111111111110000000000000000000000000000000000000000",
		41 when "11111111111111111111111111111111111100000000000000000000000000000000000000000",
		42 when "11111111111111111111111111111111111000000000000000000000000000000000000000000",
		43 when "11111111111111111111111111111111110000000000000000000000000000000000000000000",
		44 when "11111111111111111111111111111111100000000000000000000000000000000000000000000",
		45 when "11111111111111111111111111111111000000000000000000000000000000000000000000000",
		46 when "11111111111111111111111111111110000000000000000000000000000000000000000000000",
		47 when "11111111111111111111111111111100000000000000000000000000000000000000000000000",
		48 when "11111111111111111111111111111000000000000000000000000000000000000000000000000",
		49 when "11111111111111111111111111110000000000000000000000000000000000000000000000000",
		50 when "11111111111111111111111111100000000000000000000000000000000000000000000000000",
		51 when "11111111111111111111111111000000000000000000000000000000000000000000000000000",
		52 when "11111111111111111111111110000000000000000000000000000000000000000000000000000",
		53 when "11111111111111111111111100000000000000000000000000000000000000000000000000000",
		54 when "11111111111111111111111000000000000000000000000000000000000000000000000000000",
		55 when "11111111111111111111110000000000000000000000000000000000000000000000000000000",
		56 when "11111111111111111111100000000000000000000000000000000000000000000000000000000",
		57 when "11111111111111111111000000000000000000000000000000000000000000000000000000000",
		58 when "11111111111111111110000000000000000000000000000000000000000000000000000000000",
		59 when "11111111111111111100000000000000000000000000000000000000000000000000000000000",
		60 when "11111111111111111000000000000000000000000000000000000000000000000000000000000",
		61 when "11111111111111110000000000000000000000000000000000000000000000000000000000000",
		62 when "11111111111111100000000000000000000000000000000000000000000000000000000000000",
		63 when "11111111111111000000000000000000000000000000000000000000000000000000000000000",
		64 when "11111111111110000000000000000000000000000000000000000000000000000000000000000",
		65 when "11111111111100000000000000000000000000000000000000000000000000000000000000000",
		66 when "11111111111000000000000000000000000000000000000000000000000000000000000000000",
		67 when "11111111110000000000000000000000000000000000000000000000000000000000000000000",
		68 when "11111111100000000000000000000000000000000000000000000000000000000000000000000",
		69 when "11111111000000000000000000000000000000000000000000000000000000000000000000000",
		70 when "11111110000000000000000000000000000000000000000000000000000000000000000000000",
		71 when "11111100000000000000000000000000000000000000000000000000000000000000000000000",
		72 when "11111000000000000000000000000000000000000000000000000000000000000000000000000",
		73 when "11110000000000000000000000000000000000000000000000000000000000000000000000000",
		74 when "11100000000000000000000000000000000000000000000000000000000000000000000000000",
		75 when "11000000000000000000000000000000000000000000000000000000000000000000000000000",
		76 when "10000000000000000000000000000000000000000000000000000000000000000000000000000",
		76 when others;

	valid_out <= valid(2);
	pline_proc : process(clk)
	begin
		if rising_edge(clk) then
			if reset = '1' then
				valid <= (others => '0');
			elsif en = '1' then
				valid <= valid(1 downto 0) & valid_in;
			end if;
			
			if en = '1' then
				-- Level 0
				idx0 <= idx;
				val_in0 <= val_in;
				
				-- Level 1
				idx1 <= idx0;
				add0 <= ('0' & unsigned(val_in0)) + unsigned(xi_inv(idx0));
				
				-- Level 2
				yi_q <= yi(idx1);
				if idx1 < 27 then
					df_mult_q <= df_mult(idx1);
				else
					df_mult_q <= (others => '0');
				end if;
			end if;
		end if;
	end process pline_proc;
	

	x11285 <= df_mult(14) + (df_mult(16)(7 downto 0) & "0000000000");
	x195 <= (df_mult(20)(11 downto 0) & "000000") + df_mult(20);
	x91 <= (df_mult(20)(12 downto 0) & "00000") - df_mult(19);
	x25 <= (df_mult(20)(14 downto 0) & "000") + add0;
	-- x32768
	df_mult(0) <= (add0(2 downto 0) & "000000000000000");
	-- x10922
	df_mult(1) <= df_mult(2)(16 downto 0) & '0';
	-- x5461
	df_mult(2) <= x11285 - (x91(11 downto 0) & "000000");
	-- x3276
	df_mult(3) <= (df_mult(12)(15 downto 0) & "00") + (df_mult(20)(7 downto 0) & "0000000000");
	-- x2184
	df_mult(4) <= (x25(14 downto 0) & "000") + (df_mult(13)(11 downto 0) & "000000");
	-- x1560
	df_mult(5) <= x195(14 downto 0) & "000";
	-- x1170
	df_mult(6) <= df_mult(5) - (x195(16 downto 0) & '0');
	-- x910
	df_mult(7) <= (df_mult(18)(10 downto 0) & "0000000") + (df_mult(18)(16 downto 0) & '0');
	-- x728
	df_mult(8) <= (x91(14 downto 0) & "000");
	-- x595
	df_mult(9) <= (x25(13 downto 0) & "0000") + x195;
	-- x283
	df_mult(10) <= x91 + (df_mult(20)(11 downto 0) & "000000");
	-- x100
	df_mult(11) <= x25(15 downto 0) & "00";
	-- x51
	df_mult(12) <= df_mult(20) + (df_mult(20)(13 downto 0) & "0000");
	-- x31
	df_mult(13) <= (add0(12 downto 0) & "00000") - add0;
	-- x21
	df_mult(14) <= df_mult(19) + (add0(13 downto 0) & "0000");
	-- x15
	df_mult(15) <= (add0(13 downto 0) & "0000") - add0;
	-- x11
	df_mult(16) <= df_mult(17) + df_mult(20);
	-- x8
	df_mult(17) <= (add0(14 downto 0) & "000");
	-- x7
	df_mult(18) <= df_mult(17) - add0;
	-- x5
	df_mult(19) <= (add0(15 downto 0) & "00") + add0;
	-- x3
	df_mult(20) <= (add0(15 downto 0) & "00") - add0;
	-- x2
	df_mult(21) <= (add0(16 downto 0) & '0');
	df_mult(22) <= df_mult(21);
	-- x1
	df_mult(23) <= add0; 
	df_mult(24) <= df_mult(23);
	df_mult(25) <= df_mult(23);
	df_mult(26) <= df_mult(23);
	
	add1 <= yi_q - df_mult_q;
	val_out <= std_logic_vector(add1);
end architecture rtl;