
library IEEE;

use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity sh_orient_quantizer is 
port
(
	orient_in : in std_logic_vector(15 downto 0);
	
	idx_out : out std_logic_vector(2 downto 0)
);
end entity sh_orient_quantizer;

architecture rtl of sh_orient_quantizer is
	-- 6 bit integer, 10 bit fractional fixed point
	type angles_t is array(0 to 7) of unsigned(15 downto 0);
	constant angles_tab : angles_t :=
	(
-- 		0 => to_signed(-3217,16),
-- 		1 => to_signed(-2413,16),
-- 		2 => to_signed(-1609,16),
-- 		3 => to_signed(-805,16),
-- 		4 => to_signed(0,16),
-- 		5 => to_signed(804,16),
-- 		6 => to_signed(1608,16),
-- 		7 => to_signed(2412,16)
		0 => to_unsigned(0,16),
		1 => to_unsigned(402,16),
		2 => to_unsigned(804,16),
		3 => to_unsigned(1206,16),
		4 => to_unsigned(1608,16),
		5 => to_unsigned(2011,16),
		6 => to_unsigned(2413,16),
		7 => to_unsigned(2815,16)
	);

	signal orient_cmp : std_logic_vector(7 downto 0);
	signal orient_signed : signed(15 downto 0);
	signal orient_unsigned : unsigned(15 downto 0);
	signal idx : std_logic_vector(2 downto 0);
begin
	orient_signed <= signed(orient_in);
	orient_unsigned <= unsigned(orient_signed) when orient_signed >= 0 else
	                    unsigned(-orient_signed);
	
	orient_cmp(7) <= '1' when orient_unsigned > angles_tab(7) else '0';
	
	cmp_gen : for i in 0 to 6 generate
		orient_cmp(i) <= '1' when orient_unsigned >= angles_tab(i) and orient_unsigned < angles_tab(i+1) else '0';
	end generate cmp_gen;
	
	with orient_cmp select idx <=
		"000" when "00000001",
		"001" when "00000010",
		"010" when "00000100",
		"011" when "00001000",
		"100" when "00010000",
		"101" when "00100000",
		"110" when "01000000",
		"111" when "10000000",
		"000" when others;
		
	idx_out <= idx;
	
end architecture rtl;