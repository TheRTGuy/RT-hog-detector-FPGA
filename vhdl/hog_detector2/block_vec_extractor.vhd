
library IEEE;

use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity block_vec_extractor is
generic
(
        BLOCK_WIDTH_CELLS : natural := 2;
        BLOCK_HEIGHT_CELLS : natural := 2;
        
        CELL_WIDTH_PIXELS : natural := 8;
        CELL_HEIGHT_PIXELS : natural := 8;
        
        HIST_LEN : natural := 8;
        
        IMG_WIDTH : natural := 320;
        IMG_HEIGHT : natural := 240
);
port
(
        mag_in : in std_logic_vector(8 downto 0);
        idx_in : in std_logic_vector(2 downto 0);
        
        valid_in : in std_logic;
        
        en : in std_logic;
        
        cell0_bin0_out : out std_logic_vector(14 downto 0); 
        cell0_bin1_out : out std_logic_vector(14 downto 0);
        cell0_bin2_out : out std_logic_vector(14 downto 0);
        cell0_bin3_out : out std_logic_vector(14 downto 0);
        cell0_bin4_out : out std_logic_vector(14 downto 0);
        cell0_bin5_out : out std_logic_vector(14 downto 0);
        cell0_bin6_out : out std_logic_vector(14 downto 0);
        cell0_bin7_out : out std_logic_vector(14 downto 0);
        
        cell1_bin0_out : out std_logic_vector(14 downto 0); 
        cell1_bin1_out : out std_logic_vector(14 downto 0);
        cell1_bin2_out : out std_logic_vector(14 downto 0);
        cell1_bin3_out : out std_logic_vector(14 downto 0);
        cell1_bin4_out : out std_logic_vector(14 downto 0);
        cell1_bin5_out : out std_logic_vector(14 downto 0);
        cell1_bin6_out : out std_logic_vector(14 downto 0);
        cell1_bin7_out : out std_logic_vector(14 downto 0);
        
        cell2_bin0_out : out std_logic_vector(14 downto 0); 
        cell2_bin1_out : out std_logic_vector(14 downto 0);
        cell2_bin2_out : out std_logic_vector(14 downto 0);
        cell2_bin3_out : out std_logic_vector(14 downto 0);
        cell2_bin4_out : out std_logic_vector(14 downto 0);
        cell2_bin5_out : out std_logic_vector(14 downto 0);
        cell2_bin6_out : out std_logic_vector(14 downto 0);
        cell2_bin7_out : out std_logic_vector(14 downto 0);
        
        cell3_bin0_out : out std_logic_vector(14 downto 0); 
        cell3_bin1_out : out std_logic_vector(14 downto 0);
        cell3_bin2_out : out std_logic_vector(14 downto 0);
        cell3_bin3_out : out std_logic_vector(14 downto 0);
        cell3_bin4_out : out std_logic_vector(14 downto 0);
        cell3_bin5_out : out std_logic_vector(14 downto 0);
        cell3_bin6_out : out std_logic_vector(14 downto 0);
        cell3_bin7_out : out std_logic_vector(14 downto 0);
        
        read_out : out std_logic;
        valid_out : out std_logic;
        
        clk : in std_logic;
        reset : in std_logic
);
end entity block_vec_extractor;

architecture rtl of block_vec_extractor is
        constant BLOCK_CELL_TOTAL : natural := BLOCK_WIDTH_CELLS*BLOCK_HEIGHT_CELLS;
        constant BLOCK_WIDTH_PIXELS : natural := BLOCK_WIDTH_CELLS*CELL_WIDTH_PIXELS;
        constant BLOCK_HEIGHT_PIXELS : natural := BLOCK_HEIGHT_CELLS*CELL_HEIGHT_PIXELS;

        
        
        -- Histogram bin and arithmetic array signals
        type bin_arr_t is array(HIST_LEN-1 downto 0) of unsigned(14 downto 0);
        type bin_arr_arr_t is array(BLOCK_CELL_TOTAL-1 downto 0) of bin_arr_t;
        type mux_arr_t is array(HIST_LEN-1 downto 0) of unsigned(8 downto 0);
        type mux_arr_arr_t is array(BLOCK_CELL_TOTAL-1 downto 0) of mux_arr_t;
        type decode_arr_t is array(BLOCK_CELL_TOTAL-1 downto 0) of std_logic_vector(HIST_LEN-1 downto 0);
        signal fc_decode : decode_arr_t;
        signal lc_decode : decode_arr_t;
        signal cell_bins : bin_arr_arr_t := (others => (others => (others => '0')));
        signal cbin_add : bin_arr_arr_t;
        signal cbin_sub : bin_arr_arr_t;
        signal cbin_addmux : mux_arr_arr_t;
        signal cbin_submux : mux_arr_arr_t;
        
        -- Row buffers and block register matrices
        type mag_arr_t is array(BLOCK_HEIGHT_PIXELS-1 downto 0) of unsigned(8 downto 0);
        type mag_arr_arr_t is array(BLOCK_WIDTH_PIXELS-1 downto 0) of mag_arr_t;
        signal mag_regs : mag_arr_arr_t := (others => (others => (others => '0')));
        signal mag_buff : mag_arr_t := (others => (others => '0'));
        
        type idx_arr_t is array(BLOCK_HEIGHT_PIXELS-1 downto 0) of std_logic_vector(2 downto 0);
        type idx_arr_arr_t is array(BLOCK_WIDTH_PIXELS-1 downto 0) of idx_arr_t;
        signal idx_regs : idx_arr_arr_t := (others => (others => (others => '0')));
        signal idx_buff : idx_arr_t := (others => (others => '0'));
        
        type mag_line_t is array(IMG_WIDTH-BLOCK_WIDTH_PIXELS-2 downto 0) of unsigned(8 downto 0);
        type idx_line_t is array(IMG_WIDTH-BLOCK_WIDTH_PIXELS-2 downto 0) of std_logic_vector(2 downto 0);
        type mag_line_arr_t is array(BLOCK_HEIGHT_PIXELS-2 downto 0) of mag_line_t;
        type idx_line_arr_t is array(BLOCK_HEIGHT_PIXELS-2 downto 0) of idx_line_t;
        
        signal mag_lines : mag_line_arr_t := (others => (others => (others => '0')));
        signal idx_lines : idx_line_arr_t := (others => (others => (others => '0')));
        signal buff_ctr : natural range 0 to IMG_WIDTH-BLOCK_WIDTH_PIXELS-2 := 0;
        
        
        -- Signals that connect to the frontal and last columns of pixels for each cell (makes access easier)
        type cell_col_t is array(CELL_HEIGHT_PIXELS-1 downto 0) of unsigned(8 downto 0);
        type cell_col_arr_t is array(BLOCK_CELL_TOTAL-1 downto 0) of cell_col_t;
        type cell_col_idx_t is array(CELL_HEIGHT_PIXELS-1 downto 0) of std_logic_vector(2 downto 0);
        type cell_col_idx_arr_t is array(BLOCK_CELL_TOTAL-1 downto 0) of cell_col_idx_t;
        signal fc_idx : cell_col_idx_arr_t;
        signal fc_mag : cell_col_arr_t;
        signal lc_idx : cell_col_idx_arr_t;
        signal lc_mag : cell_col_arr_t;
        
        -- Column pixel selectors (pipelined)
        type cell_sel_arr_t is array(CELL_HEIGHT_PIXELS-1 downto 0) of unsigned(8 downto 0);
        signal fc_sel : cell_sel_arr_t := (others => (others => '0'));
        signal lc_sel : cell_sel_arr_t := (others => (others => '0'));
        type cell_sel_idx_arr_t is array(CELL_HEIGHT_PIXELS-1 downto 0) of std_logic_vector(2 downto 0);
        signal fc_idx_sel : cell_sel_idx_arr_t := (others => (others => '0'));
        signal lc_idx_sel : cell_sel_idx_arr_t := (others => (others => '0'));
        
        
        -- Others
        signal valid_in0 : std_logic := '0';
        signal valid_in1 : std_logic := '0';
        signal valid_in2 : std_logic := '0';
        signal delay_inp : boolean := false;
        signal valid_out0 : boolean := false;
        signal valid_out1 : boolean;
        signal valid_out2 : boolean := false;
        signal valid_out3 : boolean := false;
        signal valid_out4 : boolean := false;
        signal col_ctr : natural range 0 to IMG_WIDTH-1 := 0;
        signal row_ctr : natural range 0 to IMG_HEIGHT-1 := 0;
        signal pix_ctr : natural range 0 to CELL_HEIGHT_PIXELS-1 := 0;
begin
        cell0_bin0_out <= std_logic_vector(cell_bins(0)(0));
        cell0_bin1_out <= std_logic_vector(cell_bins(0)(1));
        cell0_bin2_out <= std_logic_vector(cell_bins(0)(2));
        cell0_bin3_out <= std_logic_vector(cell_bins(0)(3));
        cell0_bin4_out <= std_logic_vector(cell_bins(0)(4));
        cell0_bin5_out <= std_logic_vector(cell_bins(0)(5));
        cell0_bin6_out <= std_logic_vector(cell_bins(0)(6));
        cell0_bin7_out <= std_logic_vector(cell_bins(0)(7));
        
        cell1_bin0_out <= std_logic_vector(cell_bins(1)(0));
        cell1_bin1_out <= std_logic_vector(cell_bins(1)(1));
        cell1_bin2_out <= std_logic_vector(cell_bins(1)(2));
        cell1_bin3_out <= std_logic_vector(cell_bins(1)(3));
        cell1_bin4_out <= std_logic_vector(cell_bins(1)(4));
        cell1_bin5_out <= std_logic_vector(cell_bins(1)(5));
        cell1_bin6_out <= std_logic_vector(cell_bins(1)(6));
        cell1_bin7_out <= std_logic_vector(cell_bins(1)(7));
        
        cell2_bin0_out <= std_logic_vector(cell_bins(2)(0));
        cell2_bin1_out <= std_logic_vector(cell_bins(2)(1));
        cell2_bin2_out <= std_logic_vector(cell_bins(2)(2));
        cell2_bin3_out <= std_logic_vector(cell_bins(2)(3));
        cell2_bin4_out <= std_logic_vector(cell_bins(2)(4));
        cell2_bin5_out <= std_logic_vector(cell_bins(2)(5));
        cell2_bin6_out <= std_logic_vector(cell_bins(2)(6));
        cell2_bin7_out <= std_logic_vector(cell_bins(2)(7));
        
        cell3_bin0_out <= std_logic_vector(cell_bins(3)(0)); 
        cell3_bin1_out <= std_logic_vector(cell_bins(3)(1));
        cell3_bin2_out <= std_logic_vector(cell_bins(3)(2));
        cell3_bin3_out <= std_logic_vector(cell_bins(3)(3));
        cell3_bin4_out <= std_logic_vector(cell_bins(3)(4));
        cell3_bin5_out <= std_logic_vector(cell_bins(3)(5));
        cell3_bin6_out <= std_logic_vector(cell_bins(3)(6));
        cell3_bin7_out <= std_logic_vector(cell_bins(3)(7));
        
        gen0 : for i in 0 to BLOCK_HEIGHT_CELLS-1 generate
            gen1 : for j in 0 to BLOCK_WIDTH_CELLS-1 generate
                gen_f : for k in 0 to CELL_HEIGHT_PIXELS-1 generate
                    gen2 : if j = 0 generate
                        fc_mag(i*BLOCK_WIDTH_CELLS)(k) <= mag_buff(CELL_HEIGHT_PIXELS*i + k);
                        fc_idx(i*BLOCK_WIDTH_CELLS)(k) <= idx_buff(CELL_HEIGHT_PIXELS*i + k);
                    end generate gen2;
                    gen3 : if j > 0 generate
                        fc_mag(i*BLOCK_WIDTH_CELLS + j)(k) <= mag_regs(8*j - 1)(CELL_HEIGHT_PIXELS*i + k);
                        fc_idx(i*BLOCK_WIDTH_CELLS + j)(k) <= idx_regs(8*j - 1)(CELL_HEIGHT_PIXELS*i + k);
                    end generate gen3;
                    
                    lc_mag(i*BLOCK_WIDTH_CELLS + j)(k) <= mag_regs(8*(j+1) - 1)(CELL_HEIGHT_PIXELS*i + k);
                    lc_idx(i*BLOCK_WIDTH_CELLS + j)(k) <= idx_regs(8*(j+1) - 1)(CELL_HEIGHT_PIXELS*i + k);
                end generate gen_f;
            end generate gen1;
        end generate gen0;

        
        cell_gen : for i in 0 to BLOCK_CELL_TOTAL-1 generate
            bin_gen : for j in 0 to HIST_LEN-1 generate
                cbin_add(i)(j) <= cell_bins(i)(j) + ((14 downto 9 => '0') & cbin_addmux(i)(j));
                cbin_sub(i)(j) <= cbin_add(i)(j) - ((14 downto 9 => '0') & cbin_submux(i)(j));
                
                bin_proc : process(clk)
                begin
                    if rising_edge(clk) then
                        -- Pipeline stalled? (en = '0')
                        if en = '1' then
                            -- Pipeline the muxes (probably not needed, but just in case)
                            if fc_decode(i)(j) = '1' then
                                cbin_addmux(i)(j) <= fc_sel(i);
                            else
                                cbin_addmux(i)(j) <= (others => '0');
                            end if;
                            
                            if lc_decode(i)(j) = '1' then
                                cbin_submux(i)(j) <= lc_sel(i);
                            else
                                cbin_submux(i)(j) <= (others => '0');
                            end if;
                        end if;
                        
                        -- Depending on the level of the pipeline, update the bins on a valid pixel
                        if en = '1' and valid_in2 = '1' then
                            cell_bins(i)(j) <= cbin_sub(i)(j);
                        end if;
                    end if;
                end process bin_proc;
            end generate bin_gen;
            
            -- Generate decoders
            decode_proc : process(fc_idx_sel(i),lc_idx_sel(i))
            begin
                fc_decode(i) <= (others => '0');
                fc_decode(i)(to_integer(unsigned(fc_idx_sel(i)))) <= '1';
                lc_decode(i) <= (others => '0');
                lc_decode(i)(to_integer(unsigned(lc_idx_sel(i)))) <= '1';
            end process decode_proc;
            
            -- Here, the selectors are generated and pipelined (first stage)
            cell_proc : process(clk)
            begin
                if rising_edge(clk) then
                    if en = '1' then
                        fc_sel(i) <= fc_mag(i)(pix_ctr);
                        lc_sel(i) <= lc_mag(i)(pix_ctr);
                        fc_idx_sel(i) <= fc_idx(i)(pix_ctr);
                        lc_idx_sel(i) <= lc_idx(i)(pix_ctr);
                    end if;
                end if;
            end process cell_proc;
        end generate cell_gen;
        
        read_out <= '1' when valid_out0 else '0';
        valid_out <= '1' when valid_out4 else '0';
        valid_out1 <= pix_ctr = CELL_HEIGHT_PIXELS-1;
        reg_proc : process(clk)
                variable pix_ctr_add : natural range 0 to CELL_HEIGHT_PIXELS;
        begin
                if rising_edge(clk) then
			if reset = '1' then
				valid_out0 <= false;
				delay_inp <= false;
			elsif en = '1' then
				valid_out0 <= pix_ctr = CELL_HEIGHT_PIXELS-3;
				delay_inp <= false;
			elsif valid_out0 then
				valid_out0 <= false;
				delay_inp <= true;
			end if;
			
                        if reset = '1' then
                                pix_ctr <= 0;
                                valid_in0 <= '0';
                                valid_in1 <= '0';
                                valid_in2 <= '0';
                                valid_out2 <= false;
                                valid_out3 <= false;
                                valid_out4 <= false;
                                row_ctr <= 0;
                                col_ctr <= 0;
                        -- Stalled? (en = '0')
                        elsif en = '1' then
                                -- Pre-add
                                pix_ctr_add := pix_ctr + 1;
                                -- Check if the state machine is idle
                                if pix_ctr = 0 then
                                    -- Check if pixel available
                                    if valid_in0 = '1' then
                                        -- Yes, next pixel
                                        pix_ctr <= pix_ctr_add;
                                    end if;
                                -- Last pixel?
                                elsif valid_out1 then
                                    pix_ctr <= 0;
                                else
                                    pix_ctr <= pix_ctr_add;
                                end if;
                                
                                if not delay_inp then
					valid_in0 <= valid_in;
				end if;
                                valid_in1 <= valid_in0;
                                valid_in2 <= valid_in1;
                                
                                valid_out2 <= valid_out1 and (row_ctr >= 15 and col_ctr >= 15);
                                valid_out3 <= valid_out2;
                                valid_out4 <= valid_out3;
                                
                                -- If the state machine is at its end
                                if valid_out1 then
                                        if col_ctr = IMG_WIDTH-1 then
                                                col_ctr <= 0;
                                                if row_ctr = IMG_HEIGHT-1 then
                                                        row_ctr <= 0;
                                                else
                                                        row_ctr <= row_ctr + 1;
                                                end if;
                                        else
                                                col_ctr <= col_ctr + 1;
                                        end if;
                                end if;
                        end if;
                        
                        -- Pipeline stalled?
                        if en = '1' and not delay_inp then
                                mag_buff(0) <= unsigned(mag_in);
                                idx_buff(0) <= idx_in;
                        end if;
                        if en = '1' then
                                -- Valid pixel and state machine end
                                if valid_out1 then
                                        -- Update line buffer address
                                        if buff_ctr = IMG_WIDTH-BLOCK_WIDTH_PIXELS-2 then
                                                buff_ctr <= 0;
                                        else
                                                buff_ctr <= buff_ctr + 1;
                                        end if;
                                        
                                        
                                        -- Update matrices
                                        -- For each row on the first column
                                        for i in 0 to BLOCK_HEIGHT_PIXELS-1 loop
                                                mag_regs(0)(i) <= mag_buff(i);
                                                idx_regs(0)(i) <= idx_buff(i);
                                        end loop;
                                        -- For each line and column (except the first column), shift the values from earlier columns in the kernel
                                        for j in 0 to BLOCK_WIDTH_PIXELS-1 loop
                                                for i in 0 to BLOCK_HEIGHT_PIXELS-2 loop
                                                        idx_regs(i+1)(j) <= idx_regs(i)(j);
                                                        mag_regs(i+1)(j) <= mag_regs(i)(j);
                                                end loop;
                                        end loop;
                                end if;
                        end if;
                end if;
        end process reg_proc;
        
        line_buff_mem_gen : for i in 0 to 14 generate
                mem_proc : process(clk)
                begin
                        if rising_edge(clk) then
                                if en = '1' and valid_out1 then
                                        -- For each line buffer below the first, put the value from the last column
                                        mag_lines(i)(buff_ctr) <= mag_regs(BLOCK_WIDTH_PIXELS-1)(i);
                                        idx_lines(i)(buff_ctr) <= idx_regs(BLOCK_WIDTH_PIXELS-1)(i);
                                        mag_buff(i+1) <= mag_lines(i)(buff_ctr);
                                        idx_buff(i+1) <= idx_lines(i)(buff_ctr);
                                end if;
                        end if;
                end process mem_proc;
        end generate line_buff_mem_gen;
        
        
end architecture rtl;