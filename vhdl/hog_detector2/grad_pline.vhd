library IEEE;

use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity grad_pline is
	generic
	(
		IMG_WIDTH : natural := 320;
		IMG_HEIGHT : natural := 240
	);
	port
	(
		data_in : in std_logic_vector(7 downto 0);
		valid_in : in std_logic;
		
		mag_out : out std_logic_vector(8 downto 0) := (others => '0');
		idx_out : out std_logic_vector(2 downto 0) := (others => '0');
		valid_out : out std_logic := '0';
		
		clk : in std_logic := '0';
		reset : in std_logic := '0'
	);
end entity grad_pline;

architecture rtl of grad_pline is
	component hor_edge_detect is
	generic(
		DATA_IN_WIDTH : natural := 8
		);
	port( 
		data_in : in std_logic_vector(DATA_IN_WIDTH-1 downto 0);
		data_valid : in std_logic;
		
		en : in std_logic;
		
		data_out : out std_logic_vector(DATA_IN_WIDTH downto 0);
		data_valid_o : out std_logic;
		
		clk			:	in    	std_logic := '0';
		reset			:	in    	std_logic := '0'
	);
	end component hor_edge_detect;
	component ver_edge_detect is
	generic
	(
		DATA_IN_WIDTH : natural := 8;
		IMG_WIDTH : natural := 320
	);
	port( 
		data_in : in std_logic_vector(DATA_IN_WIDTH-1 downto 0);
		data_valid : in std_logic;
		
		en : in std_logic;
		
		data_out : out std_logic_vector(DATA_IN_WIDTH downto 0);
		data_valid_o : out std_logic;
		
		clk			:	in    	std_logic := '0';
		reset			:	in    	std_logic := '0'
	);
	end component ver_edge_detect;
	component edge_mag_orient is
	generic(
		CORDIC_LENGTH : natural := 7
	);
	port( 
		data_in : in std_logic_vector(31 downto 0);
		data_valid : in std_logic;
		
		en : in std_logic;
		
		data_out : out std_logic_vector(31 downto 0);
		data_valid_o : out std_logic;
		
		clk			:	in    	std_logic := '0';
		reset			:	in    	std_logic := '0'
	);
	end component edge_mag_orient;
	component sh_orient_quantizer is 
        port
        (
                orient_in : in std_logic_vector(15 downto 0);
                idx_out : out std_logic_vector(2 downto 0)
        );
        end component sh_orient_quantizer;
	
	
	signal hed_out : std_logic_vector(8 downto 0);
	signal ved_out : std_logic_vector(8 downto 0);
	signal grad_yx : std_logic_vector(31 downto 0);
	signal ed_valid : std_logic;
	signal mag_or_out : std_logic_vector(31 downto 0);
	signal mag : std_logic_vector(8 downto 0);
        signal orient : std_logic_vector(15 downto 0);
	signal mag_or_valid : std_logic;
	signal idx : std_logic_vector(2 downto 0);
begin
	rst_distr_proc : process(clk)
	begin
		if rising_edge(clk) then
                        if reset = '1' then
                            valid_out <= '0';
                        else
                            valid_out <= mag_or_valid;
                        end if;
                        mag_out <= mag;
                        idx_out <= idx;
		end if;
	end process rst_distr_proc;
	
	hed : hor_edge_detect
	port map(
		data_in => data_in,
		data_valid => valid_in,
		en => '1',
		data_out => hed_out,
		data_valid_o => ed_valid,
		clk => clk,
		reset => reset
	);
	ved : ver_edge_detect
	generic map(
		IMG_WIDTH => IMG_WIDTH
	)
	port map(
		data_in => data_in,
		data_valid => valid_in,
		en => '1',
		data_out => ved_out,
		clk => clk,
		reset => reset
	);
	
	grad_yx <= (15 downto 9 => ved_out(8)) & ved_out & (15 downto 9 => hed_out(8)) & hed_out;
	mag_or : edge_mag_orient
	generic map(
		CORDIC_LENGTH => 9
	)
	port map(
		data_in => grad_yx,
		data_valid => ed_valid,
		en => '1',
		data_out => mag_or_out,
		data_valid_o => mag_or_valid,
		clk => clk,
		reset => reset
	);
	
	mag <= mag_or_out(8 downto 0);
	orient <= mag_or_out(31 downto 16);	
	
	shoq : sh_orient_quantizer
        port map
        (
                orient_in => orient,
                idx_out => idx
        );
end architecture rtl;