library IEEE;

use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity edge_mag_orient is
generic(
	CORDIC_LENGTH : natural := 7
);
port( 
	data_in : in std_logic_vector(31 downto 0);
	data_valid : in std_logic;
	
	en : in std_logic;
	
	data_out : out std_logic_vector(31 downto 0);
	data_valid_o : out std_logic;
	
	clk			:	in    	std_logic := '0';
	reset			:	in    	std_logic := '0'
);
end entity edge_mag_orient;

architecture rtl of edge_mag_orient is
	-- 6 bit integer, 10 bit fractional fixed point
	type atanTab_t is array(0 to 9) of signed(15 downto 0);
	constant atanTabFix10 : atanTab_t :=
		  (
			0 => to_signed(804,16),
			1 => to_signed(475,16),
			2 => to_signed(251,16),
			3 => to_signed(127,16),
			4 => to_signed(64,16),
			5 => to_signed(32,16),
			6 => to_signed(16,16),
			7 => to_signed(8,16),
			8 => to_signed(4,16),
			9 => to_signed(2,16)
		  );
		  
	constant GRAD_DW : natural := 11;
	type grad_t is array(CORDIC_LENGTH-1 downto 0) of signed(GRAD_DW-1 downto 0);
	type arg_t is array(CORDIC_LENGTH+1 downto 0) of signed(15 downto 0);
	signal gy : grad_t := (others => (others => '0'));
	signal gx : grad_t := (others => (others => '0'));
	signal arg : arg_t := (others => (others => '0'));
	alias mag : signed(GRAD_DW-1 downto 0) is gx(CORDIC_LENGTH-1) ;
	signal add0 : signed(GRAD_DW+1 downto 0);
	signal mag0 : signed(GRAD_DW+1 downto 0);
	signal add1 : signed(GRAD_DW+6 downto 0);
	signal mag1 : signed(GRAD_DW-1 downto 0);
	signal dvalid : std_logic_vector(CORDIC_LENGTH+1 downto 0) := (others => '0');
begin
	-- Adjust the magnitude by dividing by 1.6465 (due to CORDIC)
	-- This is done by multiplying by ~0.6073 in fixed point notation and using an adder/shift tree
	add0 <= ( mag&"00" ) + ( (1 downto 0 => mag(GRAD_DW-1))&mag );
	add1 <= ( mag0&"00000" ) -  ( (4 downto 0 => mag0(GRAD_DW+1))&mag0 );
	
	data_valid_o <= dvalid(CORDIC_LENGTH+1);
	data_out <= std_logic_vector(arg(CORDIC_LENGTH+1)) & (15 downto GRAD_DW => mag1(GRAD_DW-1)) & std_logic_vector(mag1);
	cordic : process(clk)
		variable i : natural range 0 to CORDIC_LENGTH-1 := 0;
		variable gxt : signed(GRAD_DW-1 downto 0);
		variable gyt : signed(GRAD_DW-1 downto 0);
		variable argt : signed(15 downto 0);
		
		variable argv : signed(15 downto 0);
		variable gx_shift : signed(GRAD_DW-1 downto 0);
		variable gy_shift : signed(GRAD_DW-1 downto 0);
		variable gy_sign : std_logic;
		
		variable gx_nxt : signed(GRAD_DW downto 0);
		variable gy_nxt : signed(GRAD_DW downto 0);
		variable arg_nxt : signed(16 downto 0);
	begin
		if rising_edge(clk) then
			if reset = '1' then
				dvalid <= (others => '0');
			elsif en = '1' then
				dvalid <= dvalid(CORDIC_LENGTH downto 0) & data_valid;
			end if;
			
			if en = '1' then
				-- Cut the adder-shift tree, to reduce combinatorial path; 2 cycles extra latency introduced
				mag0 <= add0;
				mag1 <= add1(GRAD_DW+6)&add1(GRAD_DW+6 downto 8);
				
				-- Compensate for the extra latency, by delaying the gradient orientation by 2 extra cycles
				arg(CORDIC_LENGTH+1) <= arg(CORDIC_LENGTH);
				arg(CORDIC_LENGTH) <= arg(CORDIC_LENGTH-1);
				
				-- Figure out the initial values of the CORDIC algorithm
				if data_in(GRAD_DW-1) = '1' then
					-- We're in the second or third quadrant, rotate by PI
					gx(0) <= signed(not data_in(GRAD_DW-1 downto 0)) + 1;
					gy(0) <= signed(not data_in(15+GRAD_DW downto 16)) + 1;
					
					-- To make sure the orientation is within (-pi;pi], adjust the initial argument
					if data_in(15+GRAD_DW) = '1' then
						-- -PI if we're in the third quadrant
						arg(0) <= to_signed(-3217,16);
					else
						-- PI if we're in the second quadrant
						arg(0) <= to_signed(3217,16);
					end if;
				else
					-- We're in the first or fourth quadrant, so no need to do initial rotation
					gx(0) <= signed(data_in(GRAD_DW-1 downto 0));
					gy(0) <= signed(data_in(15+GRAD_DW downto 16));
					arg(0) <= (others => '0');
				end if;
					
				-- CORDIC stages
				for i in 0 to CORDIC_LENGTH-2 loop
					gxt := gx(i);
					gyt := gy(i);
					argt := arg(i);
					gy_sign := gyt(GRAD_DW-1);
					
					-- Check sign of gy and invert appropriately
					if gy_sign = '1' then
						gx_shift := (GRAD_DW-1 downto (GRAD_DW-1-i) => gxt(GRAD_DW-1))&gxt(GRAD_DW-2 downto i);
						gy_shift := not ( (GRAD_DW-1 downto (GRAD_DW-1-i) => gyt(GRAD_DW-1))&gyt(GRAD_DW-2 downto i) );
						argv := not atanTabFix10(i);
					else
						gx_shift := not ( (GRAD_DW-1 downto (GRAD_DW-1-i) => gxt(GRAD_DW-1))&gxt(GRAD_DW-2 downto i) );
						gy_shift := (GRAD_DW-1 downto (GRAD_DW-1-i) => gyt(GRAD_DW-1))&gyt(GRAD_DW-2 downto i);
						argv := atanTabFix10(i);
					end if;
					
					-- Add
					gx_nxt := (gxt & '1') + (gy_shift & gy_sign);
					gy_nxt := (gyt & '1') + (gx_shift & not gy_sign);
					arg_nxt := (argt & '1') + (argv & gy_sign);
					
					-- Get rid of the last dummy bit
					gx(i+1) <= gx_nxt(GRAD_DW downto 1);
					gy(i+1) <= gy_nxt(GRAD_DW downto 1);
					arg(i+1) <= arg_nxt(16 downto 1);
				end loop;
			end if;
		end if;
	end process;
end architecture rtl;