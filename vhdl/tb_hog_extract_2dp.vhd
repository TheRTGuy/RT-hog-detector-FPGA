library IEEE;
library altera_mf;

use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use altera_mf.all;
use STD.textio.all;

entity tb_hog_extract_2dp is
generic(
	FIFO_ADDR_WIDTH : natural := 18;
	IMG_WIDTH_PIXELS : natural := 320;
	IMG_HEIGHT_PIXELS : natural := 240;
	WIND_WIDTH_PIXELS : natural := 64;
	WIND_HEIGHT_PIXELS : natural := 128;
-- 	COEF_FILE_NAME : string := "svm_weights_3dp.dat";
	COEF_FILE_NAME : string := "svm_weights_2dp.dat";
-- 	COEF_FILE_NAME : string := "svm_weights.dat";
	DP_AMOUNT : natural := 2
);
end entity tb_hog_extract_2dp;

architecture arch of tb_hog_extract_2dp is
	component dcfifo
	generic (
		intended_device_family		: string;
		lpm_numwords		: natural;
		lpm_showahead		: string;
		lpm_type		: string;
		lpm_width		: natural;
		lpm_widthu		: natural;
		overflow_checking		: string;
		rdsync_delaypipe		: natural;
		read_aclr_synch		: string;
		underflow_checking		: string;
		use_eab		: string;
		write_aclr_synch		: string;
		wrsync_delaypipe		: natural
	);
	port (
			aclr	: in std_logic ;
			data	: in std_logic_vector (7 downto 0);
			rdclk	: in std_logic ;
			rdreq	: in std_logic ;
			wrclk	: in std_logic ;
			wrreq	: in std_logic ;
			q	: out std_logic_vector (7 downto 0);
			rdempty	: out std_logic ;
			rdusedw	: out std_logic_vector (FIFO_ADDR_WIDTH-1 downto 0);
			wrfull	: out std_logic 
	);
	end component;
	component hog_detector is
	generic(
		USE_XIL_BRAM : boolean := FALSE;
		USE_INFERRED_BRAM : boolean := FALSE;
		IMG_WIDTH_PIXELS : natural := 320;
		IMG_HEIGHT_PIXELS : natural := 240;
		WIND_WIDTH_PIXELS : natural := 64;
		WIND_HEIGHT_PIXELS : natural := 128;
		DP_AMOUNT : natural := 2
	);
	port(
		data_in : in std_logic_vector(7 downto 0) := (others => '0');
		valid_in : in std_logic := '0';
		
		detect_out : out std_logic;
		valid_out : out std_logic;
		nxt_sample : out std_logic;
		
		reg_data_in : in std_logic_vector(31 downto 0) := (others => '0');
		reg_data_out : out std_logic_vector(31 downto 0);
		reg_addr : in std_logic_vector(1 downto 0) := (others => '0');
		reg_we : in std_logic := '0';
		reg_re : in std_logic := '0';
		
		pline_clk : in std_logic := '0';
		pline_rst : in std_logic := '0';
		
		clk : in std_logic := '0';
		reset : in std_logic := '0'
	);
	end component hog_detector;

	constant WIND_WIDTH_BLOCKS : natural := (WIND_WIDTH_PIXELS/8) - 1;
	constant WIND_HEIGHT_BLOCKS : natural := (WIND_HEIGHT_PIXELS/8) - 1;

	-- SVM coef memory quantization
	type wind_len_quantized_t is array(natural range <>) of natural;
	
	function init_wind_width_q_arr(constant wind_len_blocks : natural;
					constant dp_amt : natural;
					constant dp_idx : natural)
		return wind_len_quantized_t is
		
		variable wind_len_arr : wind_len_quantized_t(dp_amt-1 downto 0);
	begin
		for i in 0 to dp_amt-1 loop
		        if i >= dp_idx then
				wind_len_arr(i) := (wind_len_blocks + dp_amt - 1 - i + dp_idx)/dp_amt;
			else
				wind_len_arr(i) := (wind_len_blocks - 1 + dp_idx - i)/dp_amt;
			end if;
		end loop;
		return wind_len_arr;
	end function;
	
	constant WIND_WIDTH_BLOCKS_QUANTIZED : wind_len_quantized_t(DP_AMOUNT-1 downto 0) :=
		init_wind_width_q_arr(WIND_WIDTH_BLOCKS,DP_AMOUNT,0);
	
	
	signal pline_clk : std_logic := '0';
	signal clk : std_logic := '0';
	signal wrclk : std_logic := '0';
	signal rst : std_logic := '0';
	signal pix : std_logic_vector(7 downto 0) := (others => '0');
	
	
	signal wind_ctr : natural := 0;
	signal valid_o : std_logic;
	signal detected : std_logic;
	  
	signal pline_valid_in : std_logic := '0';
	signal valid_in : std_logic := '0';
	signal block_vec_nxt : std_logic;
	signal fifo_empty : std_logic;
	signal fifo_data_out : std_logic_vector(7 downto 0);
	signal fifo_amt : std_logic_vector(FIFO_ADDR_WIDTH-1 downto 0);
	
	
	signal reg_data_in : std_logic_vector(31 downto 0) := (others => '0');
	signal reg_data_out :  std_logic_vector(31 downto 0) := (others => '0');
	signal reg_addr : std_logic_vector(1 downto 0) := (others => '0');
	signal reg_we : std_logic := '0';
	signal reg_re : std_logic := '0';
	
	signal max_amt : natural := 0;
begin
	fifo0 : dcfifo
	GENERIC MAP (
		intended_device_family => "Cyclone V",
		lpm_numwords => 2**FIFO_ADDR_WIDTH,
		lpm_showahead => "OFF",
		lpm_type => "dcfifo",
		lpm_width => 8,
		lpm_widthu => FIFO_ADDR_WIDTH,
		overflow_checking => "ON",
		rdsync_delaypipe => 2,
		read_aclr_synch => "OFF",
		underflow_checking => "ON",
		use_eab => "ON",
		write_aclr_synch => "OFF",
		wrsync_delaypipe => 2
	)
	PORT MAP (
		aclr => rst,
		data => pix,
		rdclk => pline_clk,
		rdreq => block_vec_nxt,
		wrclk => wrclk,
		wrreq => valid_in,
		q => fifo_data_out,
		rdempty => fifo_empty,
		rdusedw => fifo_amt
	);
	pline_valid_in <= not fifo_empty;
		
	hog : hog_detector
	generic map
	(
		IMG_WIDTH_PIXELS => IMG_WIDTH_PIXELS,
		IMG_HEIGHT_PIXELS => IMG_HEIGHT_PIXELS,
		DP_AMOUNT => DP_AMOUNT
	)
	port map
	(
		data_in => fifo_data_out,
		valid_in => pline_valid_in,
		
		detect_out => detected,
		valid_out => valid_o,
		nxt_sample => block_vec_nxt,
		
		reg_data_in => reg_data_in,
		reg_addr => reg_addr,
		reg_we => reg_we,
		
		pline_clk => pline_clk,
		pline_rst => rst,
		
		clk => clk,
		reset => rst
	);
	
	wr_proc : process(pline_clk,rst)
		file det_file : text open write_mode is "det_out.dat";
		variable wrline : line;
	begin
		if rst = '1' then
			wind_ctr <= 0;
			max_amt <= 0;
		elsif rising_edge(pline_clk) then
			if max_amt < to_integer(unsigned(fifo_amt)) then
				max_amt <= to_integer(unsigned(fifo_amt));
			end if;
			if valid_o = '1' then
				wind_ctr <= wind_ctr + 1;
				if detected = '1' then
					write(wrline, 1);
				else
					write(wrline, 0);
				end if;
				writeline(det_file, wrline);
			end if;
		end if;
	end process wr_proc;
	
	-- 100 MHz
	clk <= not clk after 5 ns;
	-- 100 MHz
	pline_clk <= not pline_clk after  5 ns;
	-- 6.25 MHz
	-- 4.8 MHz
	wrclk <= not wrclk after 104 ns;
	
	-- Min freq. for 1 dp, 320x240 = 205 MHz, max buff. cap. = 7870
	-- Min freq. for 2 dp, 320x240 = 145 MHz, max buff. cap. = 3434
	-- Min freq. for 3 dp, 320x240 = 115 MHz, max buff. cap. = 3444
	-- Min freq. for 4 dp, 320x240 = 80 MHz, max buff. cap. = 4958
	-- Min freq. for 5 dp, 320,240 = 70 MHz, max buff. cap. = 9014
	-- Min freq. for 6 dp, 320,240 = 70 MHz, max buff. cap. = 7929
	
	test : process
		file intensity_file : text;
		file coeficients_file : text;
		variable rdline : line;
		variable pixel : integer;
	begin
		
		wait for 50 ns;
		rst <= '1';
		wait for 50 ns;
		rst <= '0';
		
		file_open(coeficients_file,COEF_FILE_NAME,READ_MODE);
		wait until falling_edge(clk);
		wait until rising_edge(clk);
		reg_we <= '1';
-- 		-- Load bias (0.9 in fixed point (32 bit frac. 25 int)
		reg_addr <= "10";
		reg_data_in <= std_logic_vector(to_unsigned(386547056,32));
		wait until falling_edge(clk);
		wait until rising_edge(clk);
		reg_addr <= "11";
		reg_data_in <= std_logic_vector(to_unsigned(0,32));
		wait until falling_edge(clk);
		wait until rising_edge(clk);
		-- Load coefficients
		for p in 0 to DP_AMOUNT-1 loop
			for k in 0 to WIND_WIDTH_BLOCKS_QUANTIZED(p)*WIND_HEIGHT_BLOCKS-1 loop
				for j in 0 to 7 loop
					reg_addr <= "00";
					for i in 0 to 3 loop
						readline(coeficients_file,rdline);
						read(rdline,pixel);
						reg_data_in <= (31 downto 20 => '0') & std_logic_vector(to_unsigned(i,2)) & std_logic_vector(to_signed(pixel,18));
						wait until falling_edge(clk);
						wait until rising_edge(clk);
					end loop;
					reg_addr <= "01";
					reg_data_in <= std_logic_vector(to_unsigned(k,28)) & 
							  std_logic_vector(to_unsigned(p,1)) & 
							      std_logic_vector(to_unsigned(j,3));
					wait until falling_edge(clk);
					wait until rising_edge(clk);
				end loop;
			end loop;
		end loop;
		reg_we <= '0';
		file_close(coeficients_file);
		wait until falling_edge(wrclk);
		wait until rising_edge(wrclk);
		assert false report "Coefficients loaded" severity failure;
		
		wait until falling_edge(wrclk);
		wait until rising_edge(wrclk);
		file_open(intensity_file,"img.dat",READ_MODE);
		valid_in <= '1';
		for i in 0 to IMG_HEIGHT_PIXELS*IMG_WIDTH_PIXELS - 1 loop
			if not endfile(intensity_file) then
				readline(intensity_file,rdline);
				read(rdline,pixel);
				pix <= std_logic_vector(to_unsigned(pixel,8));
			end if;
			
			wait until falling_edge(wrclk);
			wait until rising_edge(wrclk);
		end loop;
		valid_in <= '0';
		
		file_close(intensity_file);
		
		wait until wind_ctr = (IMG_WIDTH_PIXELS-WIND_WIDTH_PIXELS+1)*(IMG_HEIGHT_PIXELS-WIND_HEIGHT_PIXELS+1);
		wait until rising_edge(pline_clk);
		wait until rising_edge(pline_clk);
		assert false report "End of simulation" severity failure;
 
		wait;
	end process test;
end architecture arch;

